/* Lamian
   For noodles. */

function Lamian(){
    this.noodles = {};
    this.autosaveInterval = 10000; // 10 seconds.

    /* Try to load noodles from localStorage IF and only if we are the
       top-level context. */
    
    if(parent == window){
	var localKeys = Object.keys(localStorage);
	for(var ix in localKeys){
	    var key = localKeys[ix];
	    if(key.startsWith("~")){
		try{
		    var noodle = JSON.parse(localStorage.getItem(key));
		    
		} catch(e){
		    console.log("Unable to parse noodle ", key);
		    continue;
		};

		/* Make sure each noodle is up to the current spec. */
		if(!noodle.lock) noodle.lock = false;

		this.noodles[noodle.key] = noodle;
	    }
	}
	this.timer = setInterval(this.autosave,this.autosaveInterval,this);
    }
}

function Noodle(key,text,progenitors,title,lock){
    this.key = key || Date.now().toString(36).toUpperCase();
    this.text = text || "";
    this.title = title || "";
    this.lock = lock || false;
    this.progenitors = progenitors || [];
    this.mod = this.key;
}

Lamian.prototype.autosave = function(lamian){
    var noodleKeys = Object.keys(lamian.noodles);
    for(var ix in noodleKeys){
	localStorage.setItem("~" + noodleKeys[ix],
			     JSON.stringify(lamian.noodles[noodleKeys[ix]]));
    }
}

Lamian.prototype.stopAutosaving = function(){
    if(this.timer){
	clearInterval(this.timer);
    }
    this.timer = null;
}

Lamian.prototype.startAutosaving = function(){
    this.stopAutosaving();
    this.timer = setInterval(this.autosave,this.autosaveInterval,this);   
}

Lamian.prototype.newdle = function(){
    var key = Date.now().toString(36).toUpperCase();
    this.noodles[key] = new Noodle(key);
    return this.noodles[key];
}

Lamian.prototype.get = function(key){
    if(key.startsWith("~")) key = key.substr(1);
    if(this.noodles[key]){
	return this.noodles[key];
    } else {
	return false;
    }
}

Lamian.prototype.set = function(key,text){
    if(this.noodles[key]){
	if(this.noodles[key].text,length != text.length){
	    this.noodles[key].mod = Date.now().toString(36).toUpperCase();
	}
	this.noodles[key].text = text;
	return this.noodles[key];
    } else {
	return false;
    }
}

Lamian.prototype.find = function(string){
    var match = false;
    var keys = Object.keys(this.noodles);
    for(var ix in keys){
	if(this.noodles[keys[ix]].text.toLowerCase().includes(string.toLowerCase())){
	    return this.noodles[keys[ix]];
	}
    }
    return match;
}

Lamian.prototype.findAll = function(string){
    var matches = [];
    var keys = Object.keys(this.noodles);
    for(var ix in keys){
	if(this.noodles[keys[ix]].text.toLowerCase().includes(string.toLowerCase())){
	    matches.push(this.noodles[keys[ix]]);
	}
    }
    return matches;
}

Lamian.prototype.findProgeny = function(key){
    var progeny = [];
    var keys = Object.keys(this.noodles);
    for(var ix in keys){
	var noodle = this.noodles[keys[ix]];
	for(var iy in noodle.progenitors){
	    if(noodle.progenitors[iy] == key){
		progeny.push(noodle);
	    }
	}
    }
    return progeny;
}

Lamian.prototype.textDump = function(){

    var txt = "Alph.js Text Export\n";
    var keys = Object.keys(this.noodles);
    keys = keys.sort();
    
    var dato = new Date();
    txt += dato.toLocaleString() + "\n\n\n";
    
    for(var ix in keys){
	var noodle = this.noodles[keys[ix]];
	txt += Noodle.toText(noodle);
    }
    return txt;
}

Lamian.prototype.remove = function(key){
    delete this.noodles[key];
    localStorage.removeItem("~" + key);
}

Noodle.toText = function(noodle){
    var txt = "";
    var dato = new Date();
    dato.setTime(parseInt(noodle.key,36));
    
    txt += "* <<" + noodle.key + ">> " + noodle.title + "\nCreated: " + dato.toLocaleString() + "\n";
    if(noodle.mod){
	dato.setTime(parseInt(noodle.mod,36));
	txt += "Modified: " + dato.toLocaleString() + "\n";
    }
    
    if(noodle.progenitors.length > 0){
	txt += "Progenitors: ";
	for(var iy in noodle.progenitors){
	    txt += "[[" + noodle.progenitors[iy] + "]], ";
	}
    }
    
    txt += "\n\n" + noodle.text + "\n\n\n";
    return txt;
}
