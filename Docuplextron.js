/*

  ALPH.JS  -  DOCUPLEXTRON
  The Alph Project <http://alph.io/>
  ©2016,2017 Adam C. Moore (LÆMEUR) <adam@laemeur.com>

  This WebExtension is a prototype multi-document interface for
  xanalogical Web media as implemented by the Alph specifications. It
  includes support for the Xanadu Project's <http://xanadu.com>
  current xanadoc and xanalink formats.

  This is free software and is released under the terms of the current
  GNU General Public License <https://www.gnu.org/copyleft/gpl.html>

  Source code and revision history are available on GitLab:
  <https://gitlab.com/alph-project/alph.js>

*/

function Docuplextron(){
    this.lang = "en";
    this.lastX = 0;
    this.lastY = 0;
    this.movement = 0;
    this.keys = [];
    this.altKey = false;
    this.sentMsgCount = 0;
    this.activePalette = null;
    this.activeItems = [];
    this.activeElement = null;
    this.mouseBypass = false;
    this.TRANSPOINTER_MODES = 3;
    this.transpointerMode = 0;
    this.contexts = [ window ]; // Not using this now, but I need to be.
    this.edlStore = [];
    this.transpointerSpans = {};
    this.linkStore = {};
    this.linkpointerSpans = [];
    this.currentSpan = null;
    this.currentContext = window;
    this.dptronMode = false;
    this.commitTarget = "#buffer";
    this.hoveredLink = null; // HACK
    this.frameskip = 0;
    this.killring = [];
    this.stateSaves = [];
    this.loopCounter = 0;
    this.simplePointers = [];
    this.simpleNexi = [];
    this.DOMCursor = new DOMCursor();
    this.autoSaveSafe = false;
    this.credStore = {};
}

Docuplextron.urls = {
    "topbar" : 'templates/topbar',
    "halp" : 'templates/HALP',
    "hyperamaHelp" : 'templates/hyperama.help.html',
    "loadFromClipboard" : 'icons/svg/alph-icon-load_from_clipboard.svg',
    ">>" : 'icons/svg/fast-forward-double-right-arrows.svg',
    "interrobang" : 'icons/svg/Interrobang.svg',
    "cog" : 'icons/svg/cog.svg',
    "sources" : 'icons/svg/xsources.svg',
    "slideout" : 'icons/svg/slideout.svg',
    "scratchpad" : 'icons/svg/edit-document.svg',
    "buffer" : 'icons/svg/align-to-left.svg',
    'alph' : 'icons/svg/alph.svg',
    'linkmodeAll' : 'icons/svg/linkmode-all.svg',
    'linkmodeContext' : 'icons/svg/linkmode-context.svg',
    'linkmodeSpan' : 'icons/svg/linkmode-span.svg',
    'linkmodeNone' : 'icons/svg/linkmode-none.svg'
}

if(typeof(chrome) != "undefined" &&
   typeof(chrome.extension) != "undefined"){
    var urlKeys = Object.keys(Docuplextron.urls);
    for(var ix in urlKeys){
	Docuplextron.urls[urlKeys[ix]] = chrome.extension.getURL(Docuplextron.urls[urlKeys[ix]]);
    }
}


/* 





   INITIALIZATION METHODS





*/


Docuplextron.prototype.initMain = function(){

    /* Create the container DIV for the whole magilla. */
    
    this.mainDiv = document.createElement("div");
    this.mainDiv.id = "alph-main";
    document.body.appendChild(this.mainDiv);
}

/***/

Docuplextron.prototype.initFloaterDiv = function(){

    /* Create the DIV in which all floaters (windows) shall live. */
    
    this.floaterDiv = document.createElement("div");
    this.floaterDiv.id = "alph-floaters";
    this.mainDiv.appendChild(this.floaterDiv);
}

/***/

Docuplextron.prototype.initBar = function(type){

    /* Create, set-up the toolbar/topbar. */
    this.bar = document.createElement("div");
    this.bar.id = "dptron-flap";
    document.body.appendChild(this.bar);

    // Get the template HTML...
    var assetLoader = new XMLHttpRequest();
    assetLoader.open('GET',Docuplextron.urls['topbar'],false);
    assetLoader.send();

    var parser = new DOMParser();
    var tempBar = parser.parseFromString(assetLoader.responseText,"text/html");
    tempBar.querySelector("#alph-posttarget-graphic").src = Docuplextron.urls[">>"];
    tempBar.querySelector("#sideFlapTabToggle").src = Docuplextron.urls["slideout"];
 
    for (var ix = 0; ix < tempBar.body.children.length; ix++){
	this.bar.appendChild(tempBar.body.children.item(ix).cloneNode(true));
    }

    if(type != "iframe"){
	// Define functions for some buttons...

	var loadButton = this.bar.querySelector("#alph-loadFromClipboard");
	loadButton.onclick = function(e){
	    var box = this.bar.querySelector("#alph-loadbox");
	    // The "Paste" execCommand only works on a <textarea> in FF. So,
	    // create a temporary textarea and paste into it.
	    var vbox = document.createElement("textarea");
	    vbox.contentEditable = true;
	    box.parentElement.appendChild(vbox);
	    vbox.focus();
	    document.execCommand("Paste");
	    var loadAs = "";
	    if(e.ctrlKey) loadAs = "ld";
	    parent.postMessage({"op":"loadExternal","url":vbox.value,"as":loadAs},"*");
	    box.value = vbox.value;
	    vbox.remove();
	}.bind(this)
	
	var sideFlapTabToggle = this.bar.querySelector("#sideFlapTabToggle");
	sideFlapTabToggle.onclick = function(){
	    this.toggleFlap();
	}.bind(this);
	
	var helpPaneToggle = this.bar.querySelector("#helpPaneToggle");
	helpPaneToggle.onclick = function(){
	    this.showFlapTab("help");
	}.bind(this);

	var linksPaneToggle = this.bar.querySelector("#linksPaneToggle");
	linksPaneToggle.onclick = function(){
	    this.showFlapTab("links");
	}.bind(this);

	var settingsPaneToggle = this.bar.querySelector("#settingsPaneToggle");
	settingsPaneToggle.onclick = function(){
	    this.showFlapTab("settings");
	}.bind(this);

	var sourcesPaneToggle = this.bar.querySelector("#sourcesPaneToggle");
	sourcesPaneToggle.onclick = function(){
	    this.showFlapTab("sources");
	}.bind(this);

	var sessionPaneToggle = this.bar.querySelector("#sessionPaneToggle");
	sessionPaneToggle.onclick = function(){
	    this.showFlapTab("session");
	}.bind(this);

	/* And some functions for the SESSION tab button/links... */
	var sessionSaveBtn = this.bar.querySelector("#sessionExport");
	sessionSaveBtn.onclick = function(){
	    
	}.bind(this);
	var sessionImportBtn = this.bar.querySelector("#sessionImport");
	sessionImportBtn.onclick = function(evt){
	    evt.stopPropagation();
	    evt.preventDefault();
	    var i = newDialog("input","Load session from file.");
	    i.floater.bringIntoView();
	    i.input.type = "file";
	    i.okbutton.onclick = function(dialog){
		alert(dialog.input.value);
	    }.bind(this,i);
	    i.input.click();
	}.bind(this);
	var fragImportBtn = this.bar.querySelector("#textFragImport");
	fragImportBtn.onclick = function(evt){
	    evt.stopPropagation();
	    evt.preventDefault();
	    var i = newDialog("input","Import text from file.");
	    i.floater.bringIntoView();
	    i.input.type = "file";
	    i.okbutton.onclick = function(dialog){
		alert(dialog.input.value);
	    }.bind(this,i);
	    i.input.click();	
	}.bind(this);
	var fragExportTextBtn = this.bar.querySelector("#saveFragsAsText");
	fragExportTextBtn.onclick = function(){
	    
	}.bind(this);
	var fragExportJSONBtn = this.bar.querySelector("#saveFragsAsJSON");
	fragExportJSONBtn.onclick = function(){
	    
	}.bind(this);

	/* Fill sessions list... */
	this.updateSessionList();    

	var sessionNameBtn = this.bar.querySelector("#dptronSessionNameBtn");
	var sessionCreateBtn = this.bar.querySelector("#dptronSessionCreateBtn");
	var sessionNameInput = this.bar.querySelector("#dptronSessionNameBox");
	sessionNameBtn.onclick = function(input,evt){
	    evt.preventDefault();
	    evt.stopPropagation();
	    var cleanInput = input.value;
	    if(!cleanInput) return;
	    /* There is a host of things I should be doing
	       here... there must be a library or a badass regexp for
	       this. */
	    cleanInput = cleanInput.replace(" ","_");
	    window.location = window.location.toString().split("#")[0] +
		"#" + cleanInput;
	    this.sessionSave();
	    this.updateSessionList();
	}.bind(this,sessionNameInput);

	sessionCreateBtn.onclick = function(input,evt){
	    evt.preventDefault();
	    evt.stopPropagation();
	    var cleanInput = input.value;
	    if(!cleanInput) return;
	    /* There is a host of things I should be doing
	       here... there must be a library or a badass regexp for
	       this. */
	    cleanInput = cleanInput.replace(" ","_");
	    window.location = window.location.toString().split("#")[0] +
		"#" + cleanInput;
	    this.sessionRestore();
	    this.sessionSave();
	    this.updateSessionList();
	}.bind(this,sessionNameInput);

    
	if(dptron.dptronMode == "teditor"){
	    assetLoader.open('GET',Docuplextron.urls["hyperamaHelp"],false);
	} else {
	    //assetLoader.open('GET',Docuplextron.urls["halp"],false);
	    assetLoader.open('GET',Docuplextron.urls["hyperamaHelp"],false);
	}
	assetLoader.send();
	var helpDoc = parser.parseFromString(assetLoader.responseText,"text/html");
	var flap = this.bar.querySelector("#helpPane");
	flap.style.overflow = "auto";
	flap.style.width = "100%";
	flap.style.height = "100%";
	for (var ix = 0; ix < helpDoc.body.children.length; ix++){
	    flap.appendChild(helpDoc.body.children.item(ix).cloneNode(true));
	}
    }

    /* A reduced form of the bar appears in subordinate windows */
    
    if(type == "iframe"){
	var removals = ["dptron-goBarDiv","dptronSideFlap"]; 
	for(var ix in removals){
	    this.bar.querySelector("#" + removals[ix]).remove();
	}
    }
    this.sentMsgCount = 0;
    return this.bar;
}

/***/

Docuplextron.prototype.initOverlay = function(){
    this.overlay = document.createElementNS(svgns,"svg");
    this.overlay.id = "alphOverlay";
    this.overlay.setAttribute("width", window.innerWidth);
    this.overlay.setAttribute("height", window.innerHeight);
    // ↑ This is updated at regular intervals by redraw();
    document.body.appendChild(this.overlay);
}

/***/

Docuplextron.prototype.initBuffer = function(b){
    if(!b){
	this.buffer = document.createElement("x-text");
	this.buffer.id = "buffer";
	this.buffer.textContent = "ALPH buffer";
	this.buffer.setAttribute("src","#buffer");
	this.buffer.setAttribute("origin","0");
	this.buffer.setAttribute("extent","11");
	var floater = this.floaterManager.create();
	floater.appendChild(this.buffer);
	floater.style.display = "none";
    } else {
	this.buffer = document.getElementById("buffer");
    }
    this.buffer.show = function(){
	this.floaterManager.create(null,null,this.lastX,this.lastY).appendChild(this.buffer);
    }.bind(this);
}

/***/

Docuplextron.prototype.initScratchpad = function(){
    this.scratchpad = document.createElement("textarea");
    this.scratchpad.id = "alph-bufferinput";
    this.scratchpad.setAttribute("cols",40);
    this.scratchpad.setAttribute("rows",12);
    this.scratchpad.show = function(){
	var floater;
	if(Docuplextron.getParentItem(this.scratchpad)){
	    floater = Docuplextron.getParentItem(this.scratchpad);
	    this.floaterManager.pickUp(floater);
	} else {
	    floater = this.floaterManager.create(null,"Scratchpad",this.lastX,this.lastY);
	    floater.appendChild(this.scratchpad);
	    floater.style.width = "auto";
	    // Make scratchpad "sticky" by default.
	    floater.stickyToggle();
	}
	floater.style.top = this.bar.getBoundingClientRect().bottom + "px";
	floater.style.left = "0px";
	floater.style.display = "block";

    }.bind(this);
}

/***/

Docuplextron.prototype.initButtons = function(){
    var scratchpadBtn = document.createElement("img");
    scratchpadBtn.id = "dptron-scratchpad-button";
    scratchpadBtn.src = Docuplextron.urls["scratchpad"];
    scratchpadBtn.setAttribute("class","dptron-flap-icon");
    scratchpadBtn.onclick = function(){
	this.scratchpad.show();
    }.bind(this);
    
    var bufferBtn = document.createElement("img");
    bufferBtn.id = "dptron-buffer-button";
    bufferBtn.src = Docuplextron.urls["buffer"];
    bufferBtn.setAttribute("class","dptron-flap-icon");
    bufferBtn.onclick = function(){
	this.buffer.show();
    }.bind(this);

    var alphBtn = document.createElement("img");
    alphBtn.id = "dptron-alph-button";
    alphBtn.src = Docuplextron.urls["alph"];
    alphBtn.setAttribute("class","dptron-flap-icon");
    alphBtn.onclick = function(){
	this.takeover();
    }.bind(this);

    var linkViewBtn = document.createElement("img");
    linkViewBtn.id = "dptron-linkview-button";
    linkViewBtn.src = Docuplextron.urls["linkmodeAll"];
    linkViewBtn.setAttribute("class","dptron-flap-icon");
    linkViewBtn.onclick = function(){
	parent.postMessage({"op":"changeTpointerMode","dir":"up"},"*");
    };

    document.getElementById("alph-loadFromClipboard").src = Docuplextron.urls["loadFromClipboard"];

    this.bar.insertBefore(linkViewBtn,this.bar.firstElementChild);
    //this.bar.insertBefore(bufferBtn,this.bar.firstElementChild);
    this.bar.insertBefore(scratchpadBtn,this.bar.firstElementChild);
    this.bar.insertBefore(alphBtn,this.bar.firstElementChild);
}

Docuplextron.prototype.updateSessionList = function(){
    var sessionList = document.createElement("ul");
    sessionList.id = "dptronSessionList";

    this.bar.querySelector("#dptronSessionNameBox").value = window.location.hash.substr(1);

    // Get the localStorage keys for session saves, then sort 'em
    var lsKeys = [];
    var unsortedKeys = Object.keys(localStorage);
    for(var ix in unsortedKeys){
	if(unsortedKeys[ix].startsWith("dptronSession")){
	    lsKeys.push(unsortedKeys[ix]);
	}
    }
    lsKeys.sort();
    
    for(var ix in lsKeys){
	var key = lsKeys[ix];
	    if(dptron.dptronMode == "teditor"){
		if(key.substr("dptronSession-".length).split("#")[0] !=
		   window.location.toString().split("#")[0]) continue;
	    }
	    var li = document.createElement("li");
	    li.setAttribute("data-sessionKey",key);
	    var a = document.createElement("a");
	    a.href = key.substr("dptronSession-".length);
	    if(dptron.dptronMode == "teditor"){
		a.textContent = a.href.split("#")[1] || "[unnamed]";
	    } else {
		a.textContent = a.href.substr(window.location.origin.length);
	    }
	    a.onclick = function(key,evt){
		evt.preventDefault();
		evt.stopPropagation();
		
		var url = key.substr("dptronSession-".length);
		var path = url.split("#")[0];
		
		if(url == window.location.toString()){

		} else if(path == window.location.toString().split("#")[0]){
		    /* Hash change; changing window.location won't actually reload
		       the document, so do this... */
		    this.sessionSave();
		    
		    this.sessionRestore(key);
		    window.location = url;
		    this.bar.querySelector("#dptronSessionNameBox").value = window.location.hash.substr(1);
		    this.updateSessionList();
		} else {
		    //console.log(path,window.location.toString().split("#")[0]);
		    this.sessionSave();
		    window.location = url;
		}
	    }.bind(this,key);
	    a.oncontextmenu = function(li,evt){
		/* ... */
		evt.preventDefault();
		evt.stopPropagation();
		this.activeMenu = this.contextMenuOn(li,["erase session"]);	    
	    }.bind(this,li);
	    li.appendChild(a);
	    
	    if(key == "dptronSession-" + window.location.toString()){
		a.classList.add("currentSession");
		li.appendChild(document.createTextNode("(current) "));
	    }

	    var b = document.createElement("a");
	    b.textContent = " X";
	    b.style.fontSize = "smaller";
	    b.style.color = "red";
	    b.style.cursor = "pointer";
	    b.onclick = function(li,evt){
		evt.preventDefault();
		evt.stopPropagation();
		this.command("erase session",li);	
	    }.bind(this,li)
	    li.appendChild(b);
	    
	    sessionList.appendChild(li);
	    /* Make links, yadda yadda... */
    }
    this.bar.querySelector("#dptronSessionList").parentElement.replaceChild(sessionList,this.bar.querySelector("#dptronSessionList"));
    //this.bar.querySelector("#dptronSessionList").replaceWith(sessionList);
}

Docuplextron.prototype.updateLinksFlap = function(){
    var flapDiv = document.getElementById('linksPane');
    
    var linkListDiv = document.createElement("div");
    linkListDiv.id = "dptronLinkListDiv";

    var linkKeys = Object.keys(this.linkStore);
    for( var ix in linkKeys){
	var listItem = document.createElement("div");
	var editBtn = document.createElement("button");
	var deleteBtn = document.createElement("button");
	var exportBtn = document.createElement("button");

	editBtn.textContent = "edit";
	editBtn.onclick = function(link){
	    linkEdit(link);
	}.bind(this,this.linkStore[linkKeys[ix]]);
	
	deleteBtn.textContent = "delete";
	deleteBtn.onclick = function(link){
	    delete this.linkStore[link.id];
	    this.updateLinksFlap();
	}.bind(this,this.linkStore[linkKeys[ix]]);

	exportBtn.textContent = "export";
	exportBtn.onclick = function(link){
	    // Placeholder...
	    this.updateLinksFlap();
	}.bind(this,this.linkStore[linkKeys[ix]]);

	listItem.classList.add("linksFlapItem");
	listItem.appendChild(editBtn);
	listItem.appendChild(deleteBtn);
	listItem.appendChild(exportBtn);
	listItem.appendChild(
	    Alphjs.objectToHTML(
		/* ...hrm. Why do I stringify and then parse the link? */
		JSON.parse(JSON.stringify(this.linkStore[linkKeys[ix]]))
	    )
	);
	
	// Testing...
	
	listItem.onmouseover = function(listItem,link,evt){
	    // BODY
	    for(var ix in link.body.items){
		var node = link.body.items[ix];
		var rects = this.getNodeRects(node);
		for(var iy in rects){
		    flashRects(this.overlay,rects[iy].rects);
		}
	    }
	    // TARGET
	    for (var ix in link.target.items){
		var node = link.target.items[ix];
		var rects = this.getNodeRects(node);
		for(var iy in rects){
		    flashRects(this.overlay,rects[iy].rects);
		}
	    }
	}.bind(this,listItem,this.linkStore[linkKeys[ix]]);

	listItem.onmouseout = function(listItem){
	    removeSimplePointersTo(this.simplePointers,listItem);
	}.bind(this,listItem);
	
	linkListDiv.appendChild(listItem);
    }
    
    //linkListDiv.appendChild(Alphjs.objectToHTML(JSON.parse(JSON.stringify(this.linkStore))));

    document.getElementById("dptronLinkListDiv").parentElement.replaceChild(linkListDiv,document.getElementById("dptronLinkListDiv"));

}

Docuplextron.prototype.updateSourcesFlap = function(){
    var flapDiv = document.getElementById('sourcesPane');
    
    var filterBox = flapDiv.querySelector('#sourcesFilter');
    filterBox.oninput = function(){
	this.updateSourcesFlap();
    }.bind(this);
    
    var sourceListDiv = document.createElement("div");
    sourceListDiv.id = "dptronSourceListDiv";

    var sourceInfoDiv = document.createElement("div");
    sourceInfoDiv.id = "dptronSourceInfoDiv";
    sourceInfoDiv.appendChild(document.createElement("span")).textContent = "No source selected.";
    
    var sourceList = document.createElement("ul");
    sourceListDiv.appendChild(sourceList);


    //document.getElementById("dptronSourceInfoDiv").replaceWith(sourceInfoDiv);
    //document.getElementById("dptronSourceListDiv").replaceWith(sourceListDiv);

    // ! NOPE. ↑ replaceWith() not supported in some recent versions of Chrome/FF
    
    document.getElementById("dptronSourceInfoDiv").parentElement.replaceChild(sourceInfoDiv,document.getElementById("dptronSourceInfoDiv"));
    document.getElementById("dptronSourceListDiv").parentElement.replaceChild(sourceListDiv,document.getElementById("dptronSourceListDiv"));
    

    if(this.dptronMode != "teditor"){
	var sourceKeys = Object.keys(alph.sources);
	for(var ix in sourceKeys){
	    var sourceKey = sourceKeys[ix];
	    var listItem = document.createElement("li");
	    listItem.textContent = sourceKey;
	    listItem.onclick = function(alph,key,div){
		div.removeChild(div.firstElementChild);
		alph.makeSourceInfo(key,div);
	    }.bind(this,alph,sourceKey,sourceInfoDiv);
	    sourceList.appendChild(listItem);
	}
    }

    var noodles = this.noodleManager.noodles;
    var noodleKeys = Array.from(Object.keys(noodles));
    noodleKeys.sort(function(a,b) { 
	if (parseInt(a,36) < parseInt(b,36))
	    return -1;
	else if (parseInt(a,36) > parseInt(b,36))
	    return 1;
	else 
	    return 0;
    });
    for(var ix in noodleKeys){
	var noodle = noodles[noodleKeys[ix]];
	// Filter!
	if(!noodle.text.toLowerCase().includes(filterBox.value.toLowerCase())){
	    continue;
	}
	var listItem = document.createElement("li");
	listItem.setAttribute("data-noodleid",noodle.key);
	// !!! Make this a function, highlightMatching() or something.
	var matchIx = noodle.text.toLowerCase().indexOf(filterBox.value.toLowerCase());
	var matchKey = document.createElement("small");
	//matchKey.textContent = noodle.key + ":";
	
	matchKey.textContent = (noodle.title) ? noodle.title : "[" + noodle.key + "]";
	matchKey.textContent += ": ";
	var matchHead = noodle.text.substring(Math.max(matchIx - 40, 0),matchIx);
	var matchBody = document.createElement("mark");
	matchBody.textContent = noodle.text.substr(matchIx,filterBox.value.length);
	var matchTail = noodle.text.substr(matchIx + filterBox.value.length, 100);
	listItem.appendChild(matchKey);
	listItem.appendChild(document.createTextNode(matchHead));
	listItem.appendChild(matchBody);
	listItem.appendChild(document.createTextNode(matchTail));
	
	//listItem.textContent = noodle.key + ":" + noodle.title + ":" + noodle.text.substr(matchIx,160);
	listItem.onclick = function(div,noodle,filter,evt){
	    if(evt.button == 0){
		div.removeChild(div.firstElementChild);
		var noodlePretty = document.createElement("div");
		var noodleKey = document.createElement("div");
		var noodleTitle = document.createElement("div");
		var noodleText = document.createElement("pre");

		var dato = new Date();
		dato.setTime(parseInt(noodle.key,36));
		noodleKey.textContent = noodle.key + " – " + dato.toLocaleString();
		noodleTitle.textContent = noodle.title || "[no title]";

		Docuplextron.highlightMatching(noodle.text,filter,noodleText);

		noodlePretty.appendChild(noodleKey);
		noodlePretty.appendChild(noodleTitle);
		noodlePretty.appendChild(noodleText);
		
		div.appendChild(noodlePretty);
		//div.appendChild(Alphjs.objectToHTML(noodle));
	    }
	}.bind(this,sourceInfoDiv,noodle,filterBox.value);

	listItem.onmouseover = function(listItem,noodle,evt){
	    var spans = Array.from(
		document.querySelectorAll('x-text[src="~' + noodle.key + '"]')
	    );
	    for(var ix in spans){
		var target = spans[ix];
		var targetFloater = Docuplextron.getParentItem(target);
		if(targetFloater){
		    if(targetFloater.isOffScreen()) target = targetFloater.tab;
		}
		this.simplePointers.push(new SimplePointer(listItem,target,this.overlay));
	    }
	}.bind(this,listItem,noodle);

	listItem.onmouseout = function(listItem){
	    removeSimplePointersTo(this.simplePointers,listItem);
	}.bind(this,listItem)
	
	listItem.oncontextmenu = function(li,noodle,evt){
	    evt.preventDefault();
	    evt.stopPropagation();
	    this.activeMenu = this.contextMenuOn(li);	    
	}.bind(this,listItem,noodle)

	listItem.ondblclick = function(li,evt){
	    evt.preventDefault();
	    evt.stopPropagation();
	    this.actionTarget = li;
	    this.command("open/show noodle");
	}.bind(this,listItem);
	
	sourceList.appendChild(listItem);
    }
}

/***/

Docuplextron.prototype.takeover = function(autoplex,expiry){
    
    /* This "takes-over" the browser from a single-document view and loads the
       docuplextron UI/space around that document. */

    if(this.dptronMode != false) return;
    
    this.dptronMode = autoplex || true;

    // Remember the document body's width
    var oldWidth = window.getComputedStyle(document.body).width;
    var oldMinWidth = parseInt(window.getComputedStyle(document.body).minWidth);
    // Create a temporary container and move the document body into it

    var tdiv = document.createElement("div");
    while(document.body.children.length > 0){
	tdiv.appendChild(document.body.children.item(0));
    }

    // Move the Docuplextron stuff back out
    document.body.appendChild(this.mainDiv);
    document.body.appendChild(this.overlay);
    document.body.appendChild(this.bar);
    document.body.appendChild(this.DOMCursor.div);
    document.body.appendChild(this.DOMCursor.label);

    // And then put the rest of the body back into a new floater
    if(expiry != 0){
	var f = this.floaterManager.create();
	if(document.contentType == "text/plain"){
	    f.style.width = "640px";
	    f.style.padding = "1em";
	} else {
	    if(oldMinWidth < 640){
		f.style.width = "640px";
	    } else {
		f.style.width = "auto"; //oldWidth
	    }
	}
	while(tdiv.children.length > 0){
	    f.appendChild(tdiv.children.item(0));
	}
	f.id = window.location.toString();
	f.tab.text.textContent = f.id.substr(0,100);
    
	if(expiry){
	    setTimeout(function(){
		this.destroy();
	    }.bind(f),expiry);
	}

    }
    
    //var autoplex = autoplex || true;

    //if(autoplex == "teditor"){
    if(this.dptronMode == "teditor"){

    } else {
	this.bar.style.top = "0px";
	this.bar.querySelector("#dptronSideFlap").style.top = this.bar.getBoundingClientRect().height + "px";
	this.floaterManager.topBoundary = this.bar.getBoundingClientRect().height;
    }
    this.mainDiv.style.height = "100vh";
    this.mainDiv.style.backgroundColor = "transparent";
    document.body.style.margin = "auto";
    document.body.style.overflow = "hidden";
    document.body.style.maxWidth = "10000px";

    /* Now, set the new boundsCheck() function on our DOMCursor */
    this.DOMCursor.boundsCheck = function(el){
	var parentItem = Docuplextron.getParentItem(el);
	if(!parentItem) return false;
	if(parentItem.classList.contains("nonContextual")) return false;
	if(el.tagName){
	    if(el.tagName == "X-FLOATER") return false;
	}
	return true;
    }

    this.rebuildEdlStore();

    if(this.dptronMode != "teditor"){
	this.gradientMorpher(this);
    }
    this.linkStore = {};
    
    this.autoRestore();

    this.pushAllLinks();

}


/*




  SIMPLEPOINTER




*/

function OldSimplePointer(anchorA,anchorB,layer,classname){
    // SVG pointer between two elements.
    this.anchorA = anchorA;
    this.anchorB = anchorB;
    this.layer = layer || document.body;
    this.dead = false;
    this.line = document.createElementNS(svgns,"line");
    this.line.setAttribute("class",classname || "dptron-simplepointer");
    this.layer.appendChild(this.line);
    this.update = function(){
	var aRect;
	var bRect;
	try {
	    // Not a valid element?
	    aRect = anchorA.getClientRects()[0];
	    bRect = anchorB.getClientRects()[0];
	} catch(e){
	    //console.log(e);
	    this.dead = true;
	    this.line.remove();
	    return;
	}
	// Removed from the DOM?
	if(!document.body.contains(this.anchorA) ||
	   !document.body.contains(this.anchorB)){
	    this.dead = true;
	    this.line.remove();
	    return;
	}
	// Visible?
	if(window.getComputedStyle(this.anchorA).display != "none" &&
	   window.getComputedStyle(this.anchorB).display != "none"){
	    this.line.setAttribute("x1",aRect.left);
	    this.line.setAttribute("y1",aRect.top);
	    this.line.setAttribute("x2",bRect.left);
	    this.line.setAttribute("y2",bRect.top);
	}
    };
}

function SimplePointer(anchorA,anchorB,layer,classname){
    // SVG pointer between two elements.
    this.anchorA = anchorA;
    this.anchorB = anchorB;
    this.layer = layer || document.body;
    this.dead = false;
    //this.line = document.createElementNS(svgns,"polygon");
    this.line = document.createElementNS(svgns,"line");
    this.line.setAttribute("class",classname || "dptron-pointerBridge");
    this.layer.appendChild(this.line);
}

SimplePointer.prototype.update = function(){
    var aRect;
    var bRect;
    try {
	// Not a valid element?
	aRect = this.anchorA.getClientRects()[0];
	bRect = this.anchorB.getClientRects()[0];
    } catch(e){
	console.log("SimplePointer dies, because: ",e);
	this.dead = true;
	this.line.remove();
	return;
    }
    // Removed from the DOM?
    if(!document.body.contains(this.anchorA) ||
       !document.body.contains(this.anchorB)){
	console.log("SimplePointer dies, because: document body does not contain", this.anchorA, " or ", this.anchorB);
	this.dead = true;
	this.line.remove();
	return;
    }
    // Visible?
    if(window.getComputedStyle(this.anchorA).display != "none" &&
       window.getComputedStyle(this.anchorB).display != "none"){

	/* If the anchor element is a circle, we want a collapsed
	   bounding box so that our lines will point at its center. */
	
	if(this.anchorA.tagName.toLowerCase() == "circle"){
	    aRect = {};
	    aRect.left = aRect.right = parseFloat(this.anchorA.getAttribute("cx"));
	    aRect.top = aRect.bottom = parseFloat(this.anchorA.getAttribute("cy"));
	}
	if(this.anchorB.tagName.toLowerCase() == "circle"){
	    bRect = {};
	    bRect.left = bRect.right = parseFloat(this.anchorB.getAttribute("cx"));
	    bRect.top = bRect.bottom = parseFloat(this.anchorB.getAttribute("cy"));
	}
	
	if(aRect.left >= bRect.left){
	    var rRect = aRect;
	    var lRect = bRect;
	} else {
	    var rRect = bRect;
	    var lRect = aRect;
	}
	
	/*
	this.line.setAttribute("points",
			       "" + lRect.right + "," + lRect.top + " " +
			       rRect.left + "," + rRect.top + " " +
			       rRect.left + "," + rRect.bottom + " " +
			       lRect.right + "," + lRect.bottom
			      );
	*/
	this.line.setAttribute("x1",lRect.right);
	this.line.setAttribute("y1",(lRect.top + lRect.bottom)/2);
	this.line.setAttribute("x2",rRect.left);
	this.line.setAttribute("y2",(rRect.top + rRect.bottom)/2);	
    }
}


function SimpleNexus(nodes,layer,docuplextron,link,isBody){
    
    /* I am a little bug that floats near link nodes to provide a visual
       indicator of which nodes are members of a body and which are members of a 
       target, and to provide bridge points between body and target.

       I'll probably do more later.

       My "nodes" are DOM elements. 
       My "layer" is an <SVG> element that I am drawn onto. */
    
    this.nodes = nodes || [];
    this.layer = layer || document.body;
    this.docuplextron = docuplextron; // or... what? A new one?
    this.link = link || {};
    if(isBody == null || isBody == undefined) {
	this.isBody = true;
    } else {
	this.isBody = isBody;
    }
    this.simplePointers = this.docuplextron.simplePointers;
    this.dot = document.createElementNS(svgns,"circle");
    this.dot.setAttribute("class","dptron-simpleNexus");
    this.dot.setAttribute("r",5);
    this.dot.nexus = this;
    layer.appendChild(this.dot);
    this.dead = false;
    this.dot.onmouseover = function(){
	this.flashNodes();
	this.coNexus.flashNodes();
    }.bind(this);
    this.dot.onmouseout = function(){
	this.deFlashNodes();
	this.coNexus.deFlashNodes();
    }.bind(this);    
    this.dot.onclick = function(){
	this.docuplextron.activeMenu = this.docuplextron.linkContextMenu(this.link);
	//linkEdit(this.link);
    }.bind(this);
}

SimpleNexus.prototype.remove = function(){
    this.dot.remove();
    this.dead = true;
}

SimpleNexus.prototype.flashNodes = function(){
    var col = (this.isBody) ? "hot" : "cold" ;
    for(var ix in this.nodes){
	this.nodes[ix].classList.add(col);
    }
}

SimpleNexus.prototype.deFlashNodes = function(){
    var col = (this.isBody) ? "hot" : "cold" ;
    for(var ix in this.nodes){
	this.nodes[ix].classList.remove(col);
    }
}

SimpleNexus.prototype.update = function(){
    var myLeft = 0;
    var myRight = 0;
    var myTop = 0;
    var myBottom = 0;
    var zTop = 0;

    if(this.nodes.length == 0){
	
	/* If I have no nodes, I have no reason for being. Goodbye! */
	
	return this.remove();
    }

    if(!this.coNexus){
	for(var ix in this.simplePointers){
	
	    /* Find myself in simplePointers, if I can. I want to find a
               SimplePointer between myself and another SimpleNexus, so
               that I can orient myself. */

	    if(this.simplePointers[ix].anchorA == this.dot &&
	       this.simplePointers[ix].anchorB.classList.contains("dptron-simpleNexus")){
		this.coNexus = this.simplePointers[ix].anchorB;
	    } else if(this.simplePointers[ix].anchorB == this.dot &&
		      this.simplePointers[ix].anchorA.classList.contains("dptron-simpleNexus")){
		this.coNexus = this.simplePointers[ix].anchorA;
	    }

	}
    }
    var coNexusRect = this.coNexus.dot.getBoundingClientRect();
    if(!coNexusRect){
	
	/* Found a connecting nexus, but couldn't get a bounding client rect. */
		
	return this.remove();
    }
    
    for(var ix in this.nodes){
	if(!document.body.contains(this.nodes[ix])){

	    /* If the node is not present in the document body... ? */
	    
	    return this.remove();
	}
	try{
	    var nodeRect = this.nodes[ix].getBoundingClientRect();
	} catch(e) {
	    
	    /* If I can't get a bounding client rect, something's wrong. Goodbye! */
	    
	    return this.remove();
	}
	zTop = Math.max(zTop,
			parseInt(Docuplextron.getParentItem(this.nodes[ix]).style.zIndex));
	myLeft += nodeRect.left;
	myRight += nodeRect.right;
	myTop += nodeRect.top;
	myBottom += nodeRect.bottom;
    }

    // We bias these values slightly for aesthetics and clarity.
    var bias = 5 * this.nodes.length;
    myLeft = (myLeft / this.nodes.length) - bias;
    myRight = (myRight / this.nodes.length) + bias;

    var myX = 0;
    var myY = 0;

    if(coNexusRect.left < myLeft){
	myX = myLeft;
    } else {
	myX = myRight;
    }

    myY = ((myTop + myBottom)/2) / this.nodes.length;
    this.dot.setAttribute("cx",myX);
    this.dot.setAttribute("cy",myY);
    this.dot.style.zIndex = zTop + 1;
}


function removeSimplePointersTo(array,element){
    for(var ix in array){
	if(array[ix].anchorA == element || array[ix].anchorB == element){
	    array[ix].line.remove();
	    /* This is stupid. Clean-up the array, Adam. */
	    array[ix].dead = true;
	}
    }
}

function cleanUpSimplePointers(array){
    var newlen = 0;
    for(var ix in array){
	if(!array[ix].dead){
	    array[newlen] = array[ix];
	    newlen++;
	}
    }
    return array.slice(0,newlen);
}


/*





  MODALS





*/

function newDialog(type,infotext){
    
    /* Simple dialog with a textarea and two buttons. By default, OK button does
       nothing, and Cancel button removes the dialog from the DOM. */
    
    var dialog = {};
    dialog.floater = dptron.floaterManager.create(null,null,dptron.lastX,dptron.lastY);
    dialog.floater.style.zIndex = "500";
    dialog.floater.classList.add("alphModal");
    dialog.floater.classList.add("dptronUI");
    dialog.floater.tab.text.textContent = " ";
    
    dialog.infotext = document.createElement("div");
    dialog.input = document.createElement(type || "div");
    dialog.okbutton = document.createElement("input");
    dialog.cancelbutton = document.createElement("input");
    
    dialog.okbutton.type = "button";
    dialog.okbutton.value = "OK";
    dialog.infotext.textContent = infotext || "";
    
    dialog.cancelbutton.type = "button";
    dialog.cancelbutton.value = "cancel";
    dialog.cancelbutton.onclick = function(){
	this.floater.destroy();
    }.bind(dialog);

    var buttonDiv = document.createElement("div");
    
    dialog.floater.appendChild(dialog.infotext);
    dialog.floater.appendChild(dialog.input);
    dialog.floater.appendChild(buttonDiv);
    buttonDiv.appendChild(dialog.okbutton);
    buttonDiv.appendChild(dialog.cancelbutton);

    dialog.input.focus();
    return dialog;
}

/***/

function newStyleMunglor(target){
    
    /* Editor dialog for element styles.*/
    
    var munglor = newDialog("textarea","");
    dptron.simplePointers.push(new SimplePointer(munglor.floater,
						 target,
						 dptron.overlay) );
    munglor.target = target;
    munglor.floater.classList.add("styleMunglor");
    munglor.floater.classList.add("nonContextual");
    munglor.floater.stickyToggle();
    munglor.floater.tab.text.textContent = "CSS Munglor for " + target.tagName + " element";

    munglor.cancelbutton.value = "dismiss";

    if (munglor.target.style.cssText) {
	munglor.input.value = munglor.target.style.cssText;
    }
    munglor.input.setAttribute("cols",40);
    
    munglor.okbutton.value = "apply";
    munglor.okbutton.onclick = function(){
	munglor.target.style.cssText = munglor.input.value;
    };
    return munglor;
}

/***/

function attributeMunglor(target){
    
    var munglor = newDialog(null,"");
    dptron.simplePointers.push(new SimplePointer(munglor.floater,
						 target,
						 dptron.overlay) );
    munglor.target = target;
    munglor.jsonAttrs = {};
    munglor.floater.classList.add("styleMunglor");
    munglor.floater.classList.add("nonContextual");
    munglor.floater.stickyToggle();
    munglor.floater.tab.text.textContent = "Attribute Munglor for " + target.tagName + " element";

    munglor.cancelbutton.value = "dismiss";

    var attrs = target.attributes;
    for(var ix in attrs){
	var attrValue = munglor.target.getAttribute(attrs[ix].name);
	if(attrValue){
	    munglor.jsonAttrs[attrs[ix].name] = attrValue;
	}
    }

    /* Do I actually need this? */
    var attrString = JSON.stringify(munglor.jsonAttrs);    
    munglor.input.appendChild(	attributeForm( JSON.parse(attrString) ) );

    
    munglor.okbutton.value = "apply";
    munglor.okbutton.onclick = function(){
	this.apply();
    }.bind(munglor);
    munglor.apply = function(){
	var formKeys = Array.from(this.floater.querySelectorAll(".attributeKey"));
	console.log("form keys:",formKeys);
	for(var ix in formKeys){
	    var keyElement = formKeys[ix];
	    var valElement = keyElement.nextSibling;
	    this.target.setAttribute(keyElement.value, valElement.value);
	    delete this.jsonAttrs[keyElement.value];
	}
	
	attrKeys = Object.keys(this.jsonAttrs);
	for(var ix in attrKeys){
	    this.target.removeAttribute(attrKeys[ix]);
	}
    }.bind(munglor);
    return munglor;
}

/***/

function attributeForm(aObject){
    var keys = Object.keys(aObject);
    var container = document.createElement("ul");
    container.className = "attributeForm";
    for (var ix in keys){
	container.appendChild(attributePair(
	    keys[ix],
	    aObject[keys[ix]],
	    true
	));
    }
    var attrCreate = document.createElement("input");
    attrCreate.type = "button";
    attrCreate.value = "new";
    attrCreate.onclick = function(container){
	container.insertBefore(attributePair("name","value"),this);
    }.bind(attrCreate,container);
    container.appendChild(attrCreate);
    
    return container;
}

function attributePair(key,value,keylock){
    var listItem = document.createElement("li");
	
    var keyInput = document.createElement("input");
    keyInput.type = "text";
    keyInput.className = "attributeKey";
    keyInput.value = key;
    if (keylock) keyInput.disabled = true;

    var valueInput = document.createElement("input");
    valueInput.type = "text";
    valueInput.className = "attributeValue";
    valueInput.value = value;

    var attrDelete = document.createElement("input");
    attrDelete.type = "button";
    attrDelete.value = "X";
    attrDelete.onclick = function(){
	this.remove();
    }.bind(listItem);
    
    listItem.appendChild(keyInput);
    listItem.appendChild(valueInput);
    listItem.appendChild(attrDelete);
    return listItem;
}

function parseAttributeForm(form){
    
}

/***/

function newMunglor(target,value){
    
    /* General-purpose editor dialog. Stores the value of the textarea
       to the object passed as target; sets initial textarea value to value.

       Currently broken? */

    var munglor = newDialog("textarea",target.toString());

    munglor.target = target;
    munglor.floater.classList.add("styleMunglor");

    munglor.cancelbutton.value = "dismiss";

    munglor.input.value = value || "";
    
    munglor.okbutton.value = "apply";
    munglor.okbutton.onclick = function(){
	this.target = this.input.value;
    }.bind(munglor);
    return munglor;
}

/***/

Docuplextron.prototype.pukeText = function(text,delay){
    var p = document.createElement("p");
    p.style.cssText = "z-index: 99999; color: green;";
    p.textContent = text || "FOO BAR.";
    this.mainDiv.appendChild(p);
    setTimeout(function(){
	this.remove();
    }.bind(p), delay || 2000);
    
}

/*






  CONTEXT MENU COMMANDS






*/

Docuplextron.prototype.command = function(c,actionTarget){
    if(actionTarget){ this.actionTarget = actionTarget; }
    switch(c){
    case "export as plain-text":
	//var barfie = window.open("about:blank");
	var outText = "";
	var targets = (this.actionTarget != this.activeItems) ?
	    this.activeItems : [ this.actionTarget ];

	for(var ix in targets){
	    var target = targets[ix];
	    if(target.classList.contains("noodleBox")){
		var noodle = this.noodleManager.get(target.querySelector("x-text").getAttribute("src"));
		outText += Noodle.toText(noodle);
	    } else {
		var firstLine = (target.hasAttribute("title")) ?
		    target.getAttribute("title") : target.id;
		outText += firstLine + "\n" + target.textContent + "\n\n\n\n";
	    }
	}

	var dia = newDialog("a","Download noodle as plain text:");
	dia.okbutton.remove();
	dia.cancelbutton.value = "close";
	dia.input.href="data:text/plain;encoding=utf-8,"+encodeURIComponent(outText);
	dia.input.download = "noodleExport.txt";
	dia.input.textContent = "Download";

	break;
    case "foomenu...":
	this.activeMenu = this.contextMenuOn(this.actionTarget,["foo","bar","bas"]);
	break;
    case "fetch and fill":
	alph.fetchAndFill(this.actionTarget);
	break;
    case "generate EDL":
	var f = this.floaterManager.create(null,null,this.lastX,this.lastY);
	f.style.fontFamily = "monospace";
	f.style.whiteSpace = "pre-wrap";
	f.textContent = Alphjs.domToEdl(this.actionTarget);
	break;
    case "erase session":
	/* Comes from a list item in #dptronSessionList */
	localStorage.removeItem(this.actionTarget.getAttribute("data-sessionKey"));
	this.updateSessionList();
	break;
    case "export to view":
	var iframe = this.actionTarget.querySelector("iframe");
	if(iframe){
	    iframe.contentWindow.postMessage({"op":"exportHTMLToParent"},"*");
	} else {
	    var f = this.floaterManager.create();
	    f.style.width = "720px";
	    var pre = document.createElement("pre");
	    pre.style.whiteSpace = "pre-wrap";
	    pre.textContent = this.exportContextHTML(this.actionTarget);
	    f.appendChild(pre);
	}
	break;
    case "export to URL" :
	var iframe = this.actionTarget.querySelector("iframe");
	if(iframe){
	    iframe.contentWindow.postMessage({"op":"exportHTMLAndPost"},"*");
	} else {
	    this.exportContextToURL(this.actionTarget);
	}
	break;
    case "post back to server" :
	var iframe = this.actionTarget.querySelector("iframe");
	if (iframe) {
	    iframe.contentWindow.postMessage({"op":"updateOnServer"},"*");
	} else {
	    this.exportContextToURL(this.actionTarget,this.actionTarget.id);
	}
	break;
    case "respect whitespace" : 
	this.actionTarget.style.whiteSpace = "pre-wrap";
	break;
    case "select colour group" :
	var floaters = this.floaterManager.getMembers();
	var col = this.actionTarget.style.borderColor;
	this.activate(this.actionTarget);
	for(var ix in floaters){
	    if(floaters[ix].style.borderColor == col &&
	       floaters[ix] != this.actionTarget){
		this.activate(floaters[ix],true);
	    }
	}
	break;
    case "ignore whitespace" : 
	this.actionTarget.style.whiteSpace = "normal";
	break;
    case "source info" :
	if(this.actionTarget.returnAddress){
	    var baseSrc = this.actionTarget.src.split("?")[0];
	} else {
	    var baseSrc = this.actionTarget.getAttribute("src").split("?")[0];
	}
	// Fire getDescription() synchronously just in case the metadata is old.
	alph.getDescription(baseSrc,true);
	var infoFloater = this.floaterManager.create(null, null,
						     this.lastX,
						     this.lastY
						    );
	infoFloater.style.width = "500px";
	infoFloater.tab.text.textContent = "Metadata for " + baseSrc;
	alph.makeSourceInfo(baseSrc,infoFloater );
	break;
    case ">>target" : 
	this.commitTarget = this.actionTarget.getAttribute("src");
	document.getElementById("alph-posttarget").value = this.commitTarget;	
	break;
    case "edit metadata" : 
	var url = this.actionTarget.getAttribute("src").split("?")[0];
	this.loadExternal(url + "?editmeta");
	break;
    case "links to this fragment" :
	if(this.actionTarget.src){
	    var span = new AlphSpan(this.actionTarget.src,
				    this.actionTarget.origin,
				    this.actionTarget.extent);
	} else {
	    var span = new AlphSpan(this.actionTarget.getAttribute("src"),
				    this.actionTarget.getAttribute("origin"),
				    this.actionTarget.getAttribute("extent"));
	}
	alph.queryLinks(span,
		       	this.floaterManager.create(null, null,
						     this.lastX,
						     this.lastY ) );
	break;
    case "links to this resource" :
	if(this.actionTarget.src){
	    var baseSrc = this.actionTarget.src.split("?")[0];
	} else {
	    var baseSrc = this.actionTarget.getAttribute("src").split("?")[0];
	}
	var span = new AlphSpan(baseSrc);
	alph.queryLinks(span,
		   this.floaterManager.create(null, null,
						this.lastX,
						this.lastY ) );
	break;
    case "clear colours":
	this.actionTarget.style.borderColor = "";
	this.actionTarget.style.color = "";
	this.actionTarget.style.backgroundColor = "";
	break;
    case "close" : 
	// Don't actually remove the scratchpad or the #buffer from the DOM.
	if(this.actionTarget.contains(this.buffer) ||
	   this.actionTarget.contains(this.scratchpad)){
	    this.actionTarget.style.display = "none";
	} else {
	    // If we're removing a floater with an iframe, be sure to remove the
	    // EDL of its document.
	    if(this.actionTarget.querySelector("iframe")){
		this.removeEDL(this.actionTarget.querySelector("iframe"));
	    }
	    this.actionTarget.scurry();
	    this.postEDL();
	    this.rebuildTranspointers();
	    //this.actionTarget.tab.remove();
	    //this.actionTarget.remove();
	    
	}
	break;
    case "close and delete" :
	// Close a noodleBox and delete the noodle it contains from localStorage
	var noodleID = this.actionTarget.querySelector("x-text").getAttribute("src").substr(1);
	this.actionTarget.destroy();
	this.noodleManager.remove(noodleID);
	this.updateSourcesFlap();
	break;
    case "title" :
	var noodleID = (this.actionTarget.querySelector("x-text")) ?
	    this.actionTarget.querySelector("x-text").getAttribute("src") :
	    false;
	var dialog = newDialog("input","Title for this noodle:");
	if(noodleID && noodleID.startsWith("~")){
	    var noodle = this.noodleManager.get(noodleID);
	    if(noodle.title) dialog.input.value = noodle.title;
	    dialog.okbutton.onclick = function(noodle, dialog, noodleBox){
		noodle.title = dialog.input.value;
		noodleBox.tab.text.textContent = noodle.title;
		dialog.floater.splode();
	    }.bind(this, noodle, dialog ,this.actionTarget);
	} else {
	    // Just change the ID of the floater
	    dialog.input.value = this.actionTarget.id;
	    dialog.okbutton.onclick = function(dialog,floater){
		floater.id = dialog.input.value;
		floater.tab.text.textContent = dialog.input.value;
		dialog.floater.splode();
	    }.bind(this,dialog,this.actionTarget);
	}
	break;
    case "progenitors" :
	break;
    case "set as wallpaper":
	this.mainDiv.style.background = "url('" + this.actionTarget.src + "') 0px 0px/100vw 100vh";
	break;
    case "flip horizontal":
	if (this.actionTarget.style.transform &&
	    this.actionTarget.style.transform.includes("rotate")){
	    this.actionTarget.style.transform = "";
	} else {
	    this.actionTarget.style.transform = "rotateY(180deg)";
	}
	break;
    case "get context document":
	if(this.actionTarget.src){
	    var xsrc = this.actionTarget.src.split("?")[0];
	} else {
	    var xsrc = this.actionTarget.getAttribute("src").split("?")[0];
	}
	if(alph.sources[xsrc]["context"]){
	    if(typeof(alph.sources[xsrc]["context"]) == "object"){
		this.loadExternal(alph.sources[xsrc]["context"]["default"]);
	    } else {
		this.loadExternal(alph.sources[xsrc]["context"]);
	    }
	} else {
	    this.loadExternal(this.actionTarget.getAttribute("src"));
	}
	break;
    case "get source media" :
	if(this.actionTarget.src){
	    this.loadExternal(this.actionTarget.src.split("?")[0]);
	} else {
	    this.loadExternal(this.actionTarget.getAttribute("src").split("?")[0]);
	}
	break;
    case "load all sources" :
	var sources = Object.keys(alph.sources);
	for(var ix = 0; ix < sources.length; ix++){
	    if(sources[ix].startsWith("http")){
		this.loadExternal(sources[ix]);
	    }
	}
	break;
    case "widen context(raw)" :
	if(this.actionTarget.src){
	    var xsrc = this.actionTarget.src;
	    var origin = parseInt(this.actionTarget.origin);
	    var extent = parseInt(this.actionTarget.extent);
	} else {
	    var xsrc = this.actionTarget.getAttribute("src");
	    var origin = parseInt(this.actionTarget.getAttribute("origin"));
	    var extent = parseInt(this.actionTarget.getAttribute("extent"));
	}
	origin = Math.max(origin - 5000, 0);
	extent = Math.min(extent + 5000, alph.sources[xsrc]["alph:textLength"]);
	this.loadExternal(xsrc + Alphjs.TRANSCLUDE_QUERY + origin + "-" + extent);
	break;
    case "unstick" :
	this.actionTarget.stickyToggle();
	break;
    case "stick" :
	this.actionTarget.stickyToggle();
	break;
    case "(un)lock width" : 
	if(!this.actionTarget.style.width ||
	   this.actionTarget.style.width == "auto"){
	    var scale = (this.actionTarget.style.transform) ?
		parseFloat(this.actionTarget.style.transform.split("scale(")[1]) :
		1;
	    this.actionTarget.style.width = (this.actionTarget.getBoundingClientRect().width / scale) + "px";
	} else {
	    this.actionTarget.style.width = null;
	}
	break;
    case "zoom to" :
	this.floaterManager.zoomTo(this.actionTarget);
	break;
    case "open/show noodle":
	// Put some tests in here, Adam.
	var noodleID = this.actionTarget.getAttribute("data-noodleid");
	var noodleSpan = document.getElementById("~"+noodleID);
	if(!noodleSpan){
	    var noodleBox = this.newdleBox(noodleID);
	    noodleBox.style.width = "500px";
	    noodleBox.bringIntoView();
	    //this.floaterManager.pickUp(noodleBox);
	} else {
	    var noodleBox = Docuplextron.getParentItem(noodleSpan);
	    if(noodleBox){
		noodleBox.bringIntoView(this.lastX,this.lastY);	
	    }
	}
	break;
    case "delete noodle":
	this.noodleManager.remove(this.actionTarget.getAttribute("data-noodleid"));
	this.actionTarget.remove();
	break;
    }
}



/*






  CONTEXT MENUS





*/

Docuplextron.prototype.linkContextMenu = function(target){
    var menu = document.createElement("div");
    menu.setAttribute("class","alph-contextmenu");
    menu.style.top = (this.lastY - 5) + "px";
    menu.style.left = (this.lastX - 5) + "px";
    this.floaterDiv.appendChild(menu);

    var linkMenuHead = Docuplextron.menuItem( (
	(target.name) ?
	    (target.name + "(" + target.id + ")") : target.id
    ) );
    linkMenuHead.onmouseup = function(link){
	linkEdit(link);
    }.bind(this,target);
    menu.appendChild(linkMenuHead);

    var bodyItems = target.body.items;
    var targetItems = target.target.items;

    for(var ix in bodyItems){
	var i = Docuplextron.menuItem("BODY" + ix + ": " + ((bodyItems[ix].name) ? bodyItems[ix].name : bodyItems[ix].src));
	i.onmouseup = function(src){
	    this.loadExternal(src);
	}.bind(this,bodyItems[ix].src);
	menu.appendChild(i);
    }
    for(var ix in targetItems){
	var i = Docuplextron.menuItem("TARGET" + ix + ": " + ((targetItems[ix].name) ? targetItems[ix].name : targetItems[ix].src));
	i.onmouseup = function(src){
	    this.loadExternal(src);
	}.bind(this,targetItems[ix].src);
	menu.appendChild(i);
	
    }
    
    

    
    /* Get the menu on-screen if it ain't. */
    var menuRect = menu.getBoundingClientRect();
    if (menuRect.left < 0) menu.style.left = "0px";
    if (menuRect.top < 0) menu.style.top = "0px";
    if (menuRect.right > window.innerWidth) menu.style.left = (window.innerWidth - menuRect.width) + "px";
    if (menuRect.bottom > window.innerHeight) menu.style.top = (window.innerHeight - menuRect.height) + "px";
    
    return menu;
    
}


Docuplextron.prototype.contextMenuOn = function(target,items){
    
    //if(!target.returnAddress){
	this.actionTarget = target;
    //}

    var menu = document.createElement("div");
    menu.setAttribute("class","alph-contextmenu");
    menu.style.top = (this.lastY - 5) + "px";
    menu.style.left = (this.lastX - 5) + "px";
    this.floaterDiv.appendChild(menu);

    /* Put a title or heading on the menu */
    if(target.tagName == "X-FLOATER"){
	var title = document.createElement("div");
	title.setAttribute("class","alph-contextmenutitle");
	if(target.classList.contains("noodleBox")){
	    var noodleId = target.querySelector("x-text").getAttribute("src");
	    title.textContent = "Noodle " + noodleId;
	} else if(target.id){
	    title.textContent = Docuplextron.titleTrim(target.id);
	}
	menu.appendChild(title);

	var borderPicker = Docuplextron.colorSwatches(null,null,"BORDER/GROUP COLOR");
	borderPicker.action = function(c1,c2){
	    this.style.borderColor = c1;
	}.bind(target);
	menu.appendChild(borderPicker);
    
	var colorPicker = Docuplextron.colorSwatches(null,null,"BODY COLOR");
	colorPicker.action = function(c1,c2){
	    this.style.backgroundColor = c1;
	    this.style.color = c2;
	}.bind(target);
	menu.appendChild(colorPicker);

    } else {
	if(target.id){
	    var title = document.createElement("div");
	    title.setAttribute("class","alph-contextmenutitle");
	    title.textContent = target.id;
	    menu.appendChild(title);
	}
    }    
    
    
    /* Create the menu with the passed items if any were passed */

    if(items){
	for(var ix in items){
	    menu.appendChild(Docuplextron.menuItem(items[ix]));
	}
    } else {
	
	// Some defaults for various menu targets...
	if(target.tagName == "X-FLOATER"){
	    menu.appendChild( (target.getAttribute("data-sticky") == true) ?
			      Docuplextron.menuItem("unstick") : Docuplextron.menuItem("stick") );
	    menu.appendChild(Docuplextron.menuItem("title"));
	    menu.appendChild(Docuplextron.menuItem("zoom to"));
	    //menu.appendChild(Docuplextron.menuItem("foomenu..."));
	    menu.appendChild(Docuplextron.menuItem("clear colours"));
	    menu.appendChild(Docuplextron.menuItem("select colour group"));
	    menu.appendChild(Docuplextron.menuItem("fetch and fill"));
	    menu.appendChild(Docuplextron.menuItem("export as plain-text"));
	}
	
	if(target.returnAddress || target.hasAttribute("src")){
	    menu.appendChild(Docuplextron.menuItem("source info"));
	    menu.appendChild(Docuplextron.menuItem("widen context(raw)"));
	    menu.appendChild(Docuplextron.menuItem("get context document"));
	    menu.appendChild(Docuplextron.menuItem("get source media"));
	    menu.appendChild(Docuplextron.menuItem("links to this fragment"));
	    menu.appendChild(Docuplextron.menuItem("links to this resource"));
	    menu.appendChild(Docuplextron.menuItem(">>target"));
	    menu.appendChild(Docuplextron.menuItem("edit source metadata"));
	    if(target.tagName == "IMG"){
		menu.appendChild(Docuplextron.menuItem("set as wallpaper"));
		menu.appendChild(Docuplextron.menuItem("flip horizontal"));
	    }
	} else if(target.tagName == "X-FLOATER"){
	    if(target.style.whiteSpace == "normal"){
		menu.appendChild(Docuplextron.menuItem("respect whitespace"));
	    } else {
		menu.appendChild(Docuplextron.menuItem("ignore whitespace"));
	    }
	    menu.appendChild(Docuplextron.menuItem("(un)lock width"));
	    menu.appendChild(Docuplextron.menuItem("generate EDL"));
	    menu.appendChild(Docuplextron.menuItem("load all sources"));
	    menu.appendChild(Docuplextron.menuItem("export to view"));
	    menu.appendChild(Docuplextron.menuItem("export to URL"));
	    if(target.id.startsWith("http://") ||
	       target.id.startsWith("https://")){
		menu.appendChild(Docuplextron.menuItem("post back to server"));
	    }
	    menu.appendChild(Docuplextron.menuItem("close"));
	} else if(target.tagName == "LI" &&
		  target.hasAttribute("data-noodleid")){
	    menu.appendChild(Docuplextron.menuItem("open/show noodle"));
	    menu.appendChild(Docuplextron.menuItem("delete noodle"));
	}
    }

    /* Get the menu on-screen if it ain't. */
    var menuRect = menu.getBoundingClientRect();
    if (menuRect.left < 0) menu.style.left = "0px";
    if (menuRect.top < 0) menu.style.top = "0px";
    if (menuRect.right > window.innerWidth) menu.style.left = (window.innerWidth - menuRect.width) + "px";
    if (menuRect.bottom > window.innerHeight) menu.style.top = (window.innerHeight - menuRect.height) + "px";
    
    return menu;
}

Docuplextron.menuItem = function(text){
    var item = document.createElement("div");
    item.setAttribute("class","alph-contextmenuitem");
    item.textContent = text;
    return item;
}

/*






  LINK EDITOR






*/
function NodeForm(alphspan){
    this.span = alphspan || new AlphSpan();
    this.form = document.createElement("form");
    this.src = document.createElement("input");
    this.origin = document.createElement("input");
    this.extent = document.createElement("input");
    this.name = document.createElement("input");
    
    this.form.nodeForm = this;
    
    this.form.appendChild(this.src);
    this.form.appendChild(this.origin);
    this.form.appendChild(this.extent);
    this.form.appendChild(this.name);
    
    this.src.setAttribute("placeholder","Media URI");
    this.src.value = this.span.src;
    this.origin.placeholder = "Origin";
    this.origin.value = this.span.origin || "";
    this.extent.placeholder = "Extent";
    this.extent.value = this.span.extent || "";
    this.name.placeholder = "Name";
    this.name.value = this.span.name || "";
    
    this.src.onchange = function(){
	this.span.src = this.src.value;
    }.bind(this);

    this.origin.onchange = function(){
	this.span.origin = this.origin.value;
    }.bind(this);
    
    this.extent.onchange = function(){
	this.span.extent = this.extent.value;
    }.bind(this);

}

function linkEdit(link){
    /* 
       A link editor!
       
    */
    
    var floater = dptron.floaterManager.create(
	"linkEditor","Link Editor");
    floater.classList.add("nonContextual");
    floater.classList.add("dptronUI");

    var formContainer = document.createElement("div");
    floater.appendChild(formContainer);

    var bodyNodes = document.createElement("div");
    var targetNodes = document.createElement("div");
    var props = document.createElement("div");
    
    var btnAddBodyNode = document.createElement("button");
    var btnAddTargetNode = document.createElement("button");
    var btnTargetFromSelection = document.createElement("button");
    var btnCreate = document.createElement("button");

    formContainer.appendChild(props);
    formContainer.appendChild(bodyNodes);
    formContainer.appendChild(targetNodes);
    formContainer.appendChild(btnCreate);

    if(!link){

	/* If no link has been passed, we'll just create a blank one.
	   If the link editor was summoned while there was an active selection,
	   we'll make that selection the body. */
	
	var link = { "id" : "~" + Date.now().toString(36).toUpperCase() ,
		     "type" : "",
		     "rel" : "" }	
	
	if(alph.selection){
	    for(var ix in alph.selection.spans){
		bodyNodes.appendChild(new NodeForm(alph.selection.spans[ix]).form);
	    }
	} else {
	    // If there's no selection, just a completely blank body.
	    bodyNodes.appendChild(new NodeForm().form);
	}
	
	targetNodes.appendChild(new NodeForm().form);
    } else {
	
	/* Make the body/target editors from the passed link. */
	for (var ix in link.body.items){
	    bodyNodes.appendChild(
		new NodeForm(link.body.items[ix]).form
	    );
	}

	for (var ix in link.target.items){
	    targetNodes.appendChild(
		new NodeForm(link.target.items[ix]).form
	    );
	}
    }

    /* Fill-out the properties for the form. */
    var dismemberedLink = {};
    Object.assign(dismemberedLink,link);
    delete dismemberedLink.body;
    delete dismemberedLink.target;
    props.appendChild(attributeForm(dismemberedLink));

    // Put the buttons on
    bodyNodes.appendChild(btnAddBodyNode);
    targetNodes.appendChild(btnAddTargetNode);
    targetNodes.appendChild(btnTargetFromSelection);

    // And define some button behavior...
    btnCreate.textContent = "OK";
    btnCreate.onclick = function(props,bodyNodes,targetNodes,floater){
	var newLink = {};
	var newBody = {"type":"List","items":[]};
	var newTarget = {"type":"List","items":[]};
	newLink.body = newBody;
	newLink.target = newTarget;
	
	// Create link object properties
	var propKeys = Array.from(props.querySelectorAll(".attributeKey"));
	for(var ix in propKeys){
	    var key = propKeys[ix].value;
	    var val = propKeys[ix].nextSibling.value;
	    if(val.toLowerCase() == "null" || val == "NaN" || val == ""){
		val = null;
	    }
	    newLink[key] = val;
	}
	
	// Create link body
	var bodyNodeForms = Array.from(bodyNodes.querySelectorAll("form"));
	for(var ix in bodyNodeForms){
	    var newSpan = new AlphSpan(
		bodyNodeForms[ix].children[0].value,
		bodyNodeForms[ix].children[1].value,
		bodyNodeForms[ix].children[2].value,
		bodyNodeForms[ix].children[3].value,
	    );
	    
	    newBody.items.push(newSpan);
	}
	// Create link target
	var targetNodeForms = Array.from(targetNodes.querySelectorAll("form"));
	for(var ix in targetNodeForms){
	    newTarget.items.push(
		new AlphSpan(
		    targetNodeForms[ix].children[0].value,
		    targetNodeForms[ix].children[1].value,
		    targetNodeForms[ix].children[2].value,
		    targetNodeForms[ix].children[3].value
		)
	    );
	}
	
	// Add to the linkStore
	this.linkStore[newLink.id] = newLink;
	this.updateLinksFlap();
	this.paintLinkTerminals();
	floater.destroy();
    }.bind(dptron,props,bodyNodes,targetNodes,floater);
    
    btnTargetFromSelection.textContent = "From Selection";
    btnTargetFromSelection.onclick = function(){
	// Remove all old NodeForms
	var oldForms = Array.from(this.querySelectorAll("form"));
	for(var ix in oldForms){
	    oldForms[ix].remove();
	}
	var btns = this.firstElementChild;
	for(var ix in alph.selection.spans){
	    this.insertBefore(new NodeForm(alph.selection.spans[ix]).form,btns);
	}
    }.bind(targetNodes);
  
    btnAddBodyNode.textContent = "+";
    btnAddBodyNode.onclick = function(){
	this.parentElement.insertBefore(new NodeForm().form,this);
    }.bind(btnAddBodyNode);
    
    btnAddTargetNode.textContent = "+";
    btnAddTargetNode.onclick = function(){
	this.parentElement.insertBefore(new NodeForm().form,this);
    }.bind(btnAddTargetNode);

}

function oldLinkEdit(){
    var anchor = Alphjs.surround("a");
    anchor.setAttribute("href","alphLinkEditor");
    var floater = dptron.floaterManager.create(null,"alphLinkEditor",dptron.lastX,dptron.lastY);
    floater.style.width = "auto";

    var foo = SimplePointer(anchor, floater);
    
    var relations = document.createElement("select");
    relations.style.display = "block";
    floater.appendChild(relations);

    var relation = document.createElement("input");
    floater.appendChild(relation);
    relation.style.display = "block";
    relation.setAttribute("disabled",true);

    // https://www.iana.org/assignments/link-relations/link-relations.xml
    var rels = ["","[custom]","about", "alternate", "appendix", "archives", "author",
		"blocked-by", "bookmark", "canonical", "chapter", "collection",
		"contents", "copyright",
    "create-form", "current", "derivedfrom", "describedby", "describes", "disclosure",
    "dns-prefetch", "duplicate", "edit", "edit-form", "edit-media", "enclosure",
    "first", "glossary", "help", "hosts", "hub", "icon", "index", "item", "last",
    "latest-version", "license", "lrdd", "memento", "monitor", "monitor-group",
    "next", "next-archive", "nofollow", "noreferrer", "original", "payment",
    "pingback", "preconnect", "predecessor-version", "prefetch", "preload", "prerender",
    "prev", "preview", "previous", "prev-archive", "privacy-policy", "profile",
    "related", "replies", "search", "section", "self", "service", "start",
    "stylesheet", "subsection", "successor-version", "tag", "terms-of-service",
    "timegate", "timemap", "type", "up", "version-history", "via", "webmention",
    "working-copy", "working-copy-of"];
    for (var ix in rels){
	var opt = document.createElement("option");
	opt.value = rels[ix];
	opt.text = rels[ix];
	relations.add(opt);
    }
    relations.onchange = function(){
	relation.value = this.value;
	if(this.value == "[custom]"){
	    relation.removeAttribute("disabled");
	} else {
	    relation.setAttribute("disabled",true);
	}
    }.bind(relations,relation);    
    
    var title = document.createElement("input");
    title.setAttribute("placeholder","Title");
    title.style.display = "block";
    floater.appendChild(title);

    var target = document.createElement("input");
    target.id = "linkEditorTarget";
    target.setAttribute("placeholder","URL");
    target.style.display = "block";
    floater.appendChild(target);

    var btn = document.createElement("input");
    btn.setAttribute("type","button");
    btn.value = "link";
    btn.onclick = function(relations,title,object,subject,floater){
	if(title.value != ""){
	    object.setAttribute("title",title.value);
	}
	object.setAttribute("rel",relations.value);
	object.setAttribute("href",subject.value);
	//object.setAttribute("href",alph.selection.asString());
	floater.destroy();
	dptron.pushAllLinks();
    }.bind(btn,relations,title,anchor,target,floater);
    floater.appendChild(btn);

    var cancel = document.createElement("input");
    cancel.setAttribute("type","button");
    cancel.value = "cancel";
    cancel.onclick = function(relations,title,object,subject,floater){
	Docuplextron.deParent(object);
	Alphjs.joinContiguousSpans();
	floater.remove();
	floater = null;
    }.bind(cancel,relations,title,anchor,target,floater);
    floater.appendChild(cancel);
    /*
    var help = document.createElement("p");
    help.innerHTML = '<small>Select a <a href="https://www.iana.org/assignments/link-relations/link-relations.xml" target="new">link relation</a> from the drop-down, and give the link a title if it needs one; then, you can either type (or paste) a URL into the target field, or make a selection of the target media you wish to link to and the target field will be filled automatically.</small>';
    floater.appendChild(help);
    */
}

/*





  IMPORT / EXPORT





*/

Docuplextron.prototype.loadExternal = function(url,as){

    if (parent != window){
	parent.postMessage({"op":"loadExternal",
			    "url":url, "as":as},"*");
	return;
    }

    if(url == ""){
	/* Don't try to load an empty URL. Really, there should be some 
	   validation going on here. */
	return;
    } else if(url.startsWith("ld+")){
	/* These are an ugly way to change how we request a document. If the
	   Load: bar is given a URL that starts with "ld+", then we will send
	   an "Accept: application/ld+json" header in the request... */
	url = url.substr(3);
	as = "ld";
    }else if(url.startsWith("as+")){
	/* ...and if we prefix the URL with "as+", we send 
	   "Accept: application/activity+json". */
	url = url.substr(3);
	as = "as";
    }

    /* See if we've got this resource open in a floater already. If
       so, re-load the resource into its existing floater; if not,
       create a new floater. */
    
    var floater = document.getElementById(url);

    if(floater){
	
	/* If the resource being loaded is the parent context of the
	   workspace... I don't know. Do nothing? */
	
	if (url == window.location.toString()){
	    
	    return floater;

	}
	
    } else {
	
	floater = this.floaterManager.create(null,url,this.lastX,this.lastY);
    }


    /* Set the floater's tab text, and fill it with a simple
       "loading..." message. Call bringIntoView() on it, just in case
       it's off in the wilds somewhere. */
    
    floater.tab.text.textContent = url.substr(url.lastIndexOf("/"));
    
    floater.appendChild(document.createElement("div")).textContent = "Loading " + url + " ...\n";
    floater.querySelector("div").style.whiteSpace = "pre-wrap";
    floater.bringIntoView();


    // Try to get metadata (synchronously) before loading the whole document
    
    var sourceName = url.split("?")[0].split("#")[0];
    if(!alph.sources[sourceName]){
	floater.querySelector("div").textContent += "Requesting metadata...\n";
	alph.getDescription(sourceName,true);
    }

    // Now that we have metadata, does this source support the
    // ?fragment interface? For the moment, we're only interested in
    // plain-text sources that support it.
    var sourceMeta = alph.sources[sourceName];
    if (sourceMeta["alph:interface"] &&
	sourceMeta["alph:interface"].includes("http://alph.io/interfaces/fragment") &&
	sourceMeta.mediaType.startsWith("text/plain")){
	console.log("fragment interface available on plain-text source.");
	// Send a ?fragment query instead of requesting the whole document?
	if(!url.includes("?")){
	    if(url.includes("#")){
		url = url.split("#");
		url = url[0] + Alphjs.TRANSCLUDE_QUERY + url[1];
	    }
	}
    }
    
    
    // Now make the request for the document
    
    var xhr = new XMLHttpRequest();
    xhr.onload = function(xhr,f,url){
	if(xhr.status == 200){
	    var baseUrl = url.split("?")[0].split("#")[0];
	    
	    // Keep a reference to our SVG layer
	    var foverlay = f.overlay;
	    
	    if(f.querySelector("iframe")){
		// We'll be replacing the iframe in the floater, so remove that
		// iframe's EDL.
		this.removeEDL(f.querySelector("iframe"));
	    }

	    // Empty the floater.
	    var deadkids = Array.from(f.childNodes);
	    for(var ix in deadkids){
		deadkids[ix].remove();
	    }
	    

	    /* Here's a fun thing: some sites (like textfiles.com)
	       don't send a Content-Type header at all for some files!
	       Ugh. So, set the mediatype to text/plain by default,
	       and then check to see if the server sent one. */
	    
	    var mediatype = "text/plain";

	    if(xhr.getResponseHeader("Content-Type")){
		mediatype = xhr.getResponseHeader("Content-Type");
	    }

	    // Different actions depending on rec'd media type...
	    
	    if(mediatype.startsWith("text/html")){
		// Is there a fragment identifier? If so, grab the fragment out of
		// the response document and load it into a floater.
		if(url.includes("#")){
		    var fragID = url.split("#")[1];
		    var parser = new DOMParser();
		    var responseDoc = parser.parseFromString(xhr.responseText,"text/html");
		    if(fragID.startsWith("xpath:")){
			fragID = fragID.substr(6);
			var i = responseDoc.evaluate(
			    fragID,
			    responseDoc.documentElement,
			    null,
			    XPathResult.ANY_TYPE,
			    null);
		    } else if(fragID.startsWith("element(")){
			var i = Alphjs.resolvePoint(
			    fragID.slice(8,-1),
			    responseDoc.documentElement)[0];
		    } else if(fragID.startsWith("range(")){
			
		    } else {
			var i = responseDoc.getElementById(fragID);
			if(!i){
			    i = document.createTextNode("Could not locate fragment '" + fragID + "'.");
			}
		    }
		    if(!i){
			console.log("!",i);
			f.appendChild(document.createTextNode("Well, that didn't work."));
			return;
		    }

  		    i.setAttribute("resource",url);
		    f.id = "[" + url + "]";
		    f.setAttribute("content-source",url.split("#")[0]);
		    
		    /* Now... there must be a scrap of free code somewhere for this, but
		       we should fix all of the relative URLs in these document fragments,
		       because having a document fragment is great, but not REALLY great 
		       if none of the links work. 
		    */

		    // Fix all of the "href" and "src" properties.
		    var srcs = Array.from(i.querySelectorAll("[href],[src]"));
		    var urlDomain = url.substr(0,url.indexOf("/"));
		    var urlDir = url.substr(0,url.lastIndexOf("/") + 1);
		    
		    for (var ix in srcs){
			var attr = (srcs[ix].hasAttribute("src")) ? "src" : "href";
			var attrVal = srcs[ix].getAttribute(attr);
			
			if(attrVal.startsWith("#")){
			    srcs[ix].setAttribute(attr, url.split("#")[0] + attrVal);
			} else if(attrVal.startsWith("/")){
			    srcs[ix].setAttribute(attr, urlDomain + attrVal); 
			} else if(!attrVal.includes("://")){
			    srcs[ix].setAttribute(attr, urlDir + attrVal);
			}
		    }

		} else {
		    var i=document.createElement("iframe");
		    // This is just an initial size, which should be changed once the
		    // <iframe> is loaded and sends us a "sizeReport" message.
		    i.style.width = "720px";
		    i.style.height = "480px";
		    i.style.backgroundColor = "white";
		    i.style.border = "none";
		    i.style.overflow = "hidden";
		    i.setAttribute("scrolling","no");
		    i.setAttribute("sandbox","allow-same-origin allow-scripts allow-top-navigation");
		    i.src=url;
		}
		f.appendChild(i);
	    } else if (mediatype.startsWith("text/")){
		
		/* Get a handle on the actual text content. We may have cached it
		   already, and the server may just have sent us a 'content not changed'
		   response with no text. */
		
		var media = xhr.responseText;
		
		if(alph.sources[baseUrl].mediaCache){
		    if (alph.sources[baseUrl].mediaCache.length > media.length){
			media = alph.sources[baseUrl].mediaCache;
		    }
		}

		if(media.match(/,start=/)){
		    // Is this a xanadoc EDL?
		    f.appendChild(Docuplextron.edlToAlpHTML(media));
		    alph.getXSources();
		} else {
		    // Otherwise, it's just plain text...
		    var docDiv = document.createElement("article");
		    docDiv.setAttribute("class","plaintext");
		    f.appendChild(docDiv);

		    if(url.includes(Alphjs.TRANSCLUDE_QUERY)){
			// This splits a concatenated fragment string into
			// a series of X-TEXT elements and inserts them into
			// docDiv. 
			Alphjs.splitHref(url,docDiv);
			alph.fetchAndFill(docDiv);
		    } else {
			var span = AlphSpan.fromURL(url);
			var doc = span.toDOMFragment();
			//var doc = document.createElement("x-text");
			docDiv.appendChild(doc);
			//doc.textContent = xhr.responseText;
			doc.textContent = media;
			//doc.setAttribute("src", url);
			//doc.setAttribute("origin", 0);
			//doc.setAttribute("extent", media.length);
			if(!span.origin){
			    doc.setAttribute("origin", 0);
			    doc.setAttribute("extent", media.length);
			} else if(!span.extent){
			    doc.setAttribute("extent", media.length);
			}
			alph.fetchAndFill(docDiv);
		    }
		}
		
		/* Finally, set an initial size for the floater, as
		   plain-text documents without hard line-wrapping
		   will stretch as wide as (or wider than) the whole
		   screen if you don't limit 'em. */
		
		/* Make the <article> support columns. If this is a 
		   hard-wrapped text file, see how wide it is on its own
		   and set the column-width to something a little wider than
		   that. */
		
		//var textRect = doc.getBoundingClientRect();

		/* This tests to see if the x-text is butted right up against the
		   right side of the floater. Probably not a hard-wrapped file,
		   so set an arbitrary column width. */
		//var newlines = doc.textContent.match(/\n/g).length;
		//var newlineRatio = doc.textContent.length / newlines;
		//console.log("Loaded text averages a newline every " + newlineRatio + " chars.");
		
		//if(textRect.width > (f.getBoundingClientRect().width - 20) &&
		//   textRect.width > 600){
		//    f.style.width = "640px";
		//    docDiv.style.columnWidth = "630px";
		//} else {
		//    docDiv.style.columnWidth = (textRect.width + 10) + "px";
		//}
		
	    } else if (mediatype.startsWith("image/")){
		var i = document.createElement("img");
		i.src = url;
		f.appendChild(i);
		f.style.width = "auto";
		f.style.backgroundColor = "transparent";
	    } else if (mediatype.startsWith("audio/")){
		var i = document.createElement("audio");
		i.setAttribute("controls",true);
		i.setAttribute("preload","metadata");
		i.src = url;
		f.style.width = "auto";
		f.appendChild(i);
	    } else if (mediatype.startsWith("video/")){
		var i = document.createElement("video");
		i.setAttribute("controls",true);
		i.setAttribute("preload","metadata");
		i.src = url;
		f.style.width = "auto";
		f.appendChild(i);
	    } else if (mediatype.startsWith("application/") && mediatype.includes("json")){
		//console.log("Response data:",xhr.response);
		f.appendChild(Alphjs.objectToHTML(xhr.response));
		//f.appendChild(Alphjs.objectToHTML(JSON.parse(xhr.response)));
		f.style.width = "512px";
		f.id = "ld+" + url;
	    } else {
		f.textContent = "I don't know what to do with" + xhr.getResponseHeader("Content-Type") + "right now.";
	    }

	    // Put the SVG overlay back onto the floater.
	    f.appendChild(foverlay);

	    // Make the parent document aware of the new content.
	    this.updateContexts();
	    this.postEDL();
	    //this.pushAllLinks();

	    // If we received an HTML file, fulfil any text transclusions
	    if(mediatype.startsWith("text/html")){
		alph.fetchAndFill();
	    }
	} else {
	    //console.log("Bad news?");
	    f.textContent = "HTTP " + xhr.status + "; media unavailable.";
	}
	// Last thing -- update dptron.transpointerSpans
	this.findSomeMatchingContent(f.querySelector("iframe") || window);
	this.pushAllLinks();
    }.bind(this, xhr, floater, url);

    xhr.open('GET',url);
    if(as){
	if(as == "ld"){
	    xhr.setRequestHeader("Accept","application/ld+json");
	    xhr.responseType = "json";
	} else if(as == "as"){
	    xhr.setRequestHeader("Accept","application/activity+json");
	    xhr.responseType = "json";
	}
    }
    floater.firstElementChild.textContent += "Requesting media...\n";
    xhr.send();
        
    return floater;
}

/* * */

Docuplextron.prototype.commitSpan = function(){
    
    /* Store the orphan text currently being edited, either to the in-browser
       #buffer, or to a URL.

       TODO:
       This is pretty long. Break this up into a few more functions, Adam. 
    */

    // The <x-text> currently being edited:

    var anchorNode = window.getSelection().anchorNode;
    console.log("commit anchor node: ", anchorNode);
    
    if(anchorNode.nodeType == 3) { // A text node
	var span = anchorNode.parentElement;
	if(!span || !span.tagName || span.tagName != "X-TEXT"){
	    console.log(span, " is not valid commit source.");
	    return;
	}
    } else if (anchorNode.tagName && anchorNode.tagName == "TEXTAREA"){
	var span = anchorNode;
	if(span.id != "alph-bufferinput"){
	    console.log("commit source is a TEXTAREA, but not the scratchpad: ",span);
	    return;
	}
    } else {
	console.log("Not a valid commit source?");
	return;
    }

    this.commitTarget = document.getElementById("alph-posttarget").value;

    var commitTarget = this.commitTarget;

    var timestamp = "\n\n* @" + (new Date).toISOString() + "\n";
    //var timestamp = "\n\n* " + YMDMins() + "\n";
    
    console.log("trying to commit ", span, this.commitTarget);
    
    if (span.id == "alph-bufferinput"){
	
	/* Committing an edit from the scratchpad:
	   Write the text to the buffer/URL target, then
	   open the target in a composition view and clear the scratchpad. */
	
	if(commitTarget.startsWith("#")){
	    var buf = document.getElementById(commitTarget.substr(1));
	    buf.textContent += timestamp + span.value;
	    buf.setAttribute("extent",buf.textContent.length);
	    alph.sources[commitTarget]["alph:textLength"] = buf.textContent.length;
	    span.value = "";
	    this.postEDL();
	    return;
	} else if(commitTarget.startsWith("http")){
	    // This could/should be broken-out into its own function, probably.
	    var xhr = new XMLHttpRequest();
	    var form = new FormData();
	    form.append("media",span.value);
	    //form.append("ts",timestamp);
	    xhr.onload = function(xhr,span,target){
		if(xhr.status == 200){
		    //Console.log(this.responseText)
		    var r = JSON.parse(xhr.responseText);
		    this.loadExternal(target,"alph-floater");
		    alph.getDescription(target);
		    span.value = "";
		    this.postEDL();
		}
	    }.bind(this,xhr,span,commitTarget);
	    xhr.onerror = function(){
		alert(JSON.stringify(this));
	    }.bind(xhr);
	    xhr.open('POST',commitTarget);
	    xhr.send(form);
	}
    } else if(span.getAttribute("src").startsWith("~")){
	
	/* This is an <x-text> being edited inside of a composition
	   pane. The idea here is that we want to retain the HTML
	   structure of the edited span, while wrapping all Text nodes
	   inside <x-text> tags so they become Xanalogical.

	   Tricky.

	   -----
	   This ~works~ for noodles, but it doesn't work well. Noodle
	   titles are discarded, the resulting <x-text> is bare,
	   instead of wrapped in an <article> or something. */

	var noodle = this.noodleManager.get(span.getAttribute("src"));
	timestamp += "<<" + noodle.key + ">> " + noodle.title;
	if(noodle.mod){
	    timestamp += " (" + noodle.mod + ")\n";
	} else {
	    timestamp += "\n";
	}
	if(noodle.progenitors.length > 0){
	    for(var pp in noodle.progenitors){
		timestamp += "[[" + noodle.progenitors[pp] + "]] ";
	    }
	    timestamp += "\n";
	}
	timestamp += "\n";
	
	// Length/terminal-index of the buffer we're committing to.
	var bufend = 0;
	
	if(commitTarget.startsWith("#")){
	    bufend = document.getElementById(commitTarget.substr(1)).textContent.length;
	} else {
	    this.pukeText("COMMIT: fetching target metadata...")
	    var m = alph.getDescription(commitTarget,true,true); // Force async meta fetch
	    if(m["httpError"] &&
	       m["httpError"] == 404){
		var meth = "PUT";
	    } else {
		var meth = "POST";
	    }
	    //console.log("Got this back from getDescription():",m);
	    if(m["alph:textLength"]){
		bufend = m["alph:textLength"];
	    }
	}

	/* If we're going to post a timestamp, extend bufend to
	   include it. */
	
	/* NOTE: We have to be careful about newlines,
	   here. Firefox/Chrome both use single-byte newlines (LF) in
	   String objects (thus, edited text in contentEditable
	   elements), but they convert these to CR,LF when POSTing to
	   a Web server. So for our origins/offsets we have to
	   increment each index for every newline in a string. */
	
	var viaHttp = commitTarget.startsWith("http");
	
	bufend += (viaHttp) ?
	    timestamp.length + (timestamp.split("\n").length - 1) :
	    timestamp.length;

	this.pukeText("COMMIT: appending text to " + bufend + " codepoint permascroll.");
	//console.log("Buffer end: " + bufend + "?");
	
	/* This NodeIterator will give us all of the Text nodes in the
	   span in document order. We want to:
	   1) cat all of these Text nodes into a string, and
	   2) wrap all of the text nodes in <x-text> elements.*/
	
	var iterator = document.createNodeIterator(span,4);
	var catspan = "";

	/* This is wonky:
	
	   I'm plugging these text nodes into an Array because the NodeIterator
	   was just ... I don't know what the hell was going on when I tried to
	   run this loop by iterating with the NodeIterator directly. This works,
	   but I'd prefer if I knew what I was doing wrong using the NodeIterator.*/
	
	var tnodes = [];
	var tnode = iterator.nextNode();
	while(tnode){
	    tnodes.push(tnode);
	    tnode = iterator.nextNode();
	}
	
	while(tnode = tnodes.shift()){
	    catspan += tnode.textContent;

	    var newBufend = (viaHttp) ?
		bufend + tnode.textContent.length + tnode.textContent.split("\n").length -1 :
		bufend + tnode.textContent.length;
	    
	    var subspan = Alphjs.newSpan(commitTarget, bufend, newBufend);
	    tnode.parentElement.insertBefore(subspan,tnode);
	    subspan.appendChild(tnode);
	    bufend = newBufend;
	};
	
	/* Now, we've created all of these new spans inside of a contentEditable
	   span, but we want to break them out of there because we never want to
	   nest transclusion spans. However, we want to keep a reference to the
	   span's parent so that we can refresh the spans from source later, so...*/
	
	var spanParent = span.parentElement;
	Docuplextron.deParent(span);

	/* If the text we're posting was in a noodleBox, remove that class
	   from its floater, otherwise the floater will fail to load on get an error on
	   session restore. */
	
	var floater = Docuplextron.getParentItem(spanParent);
	if(floater) floater.classList.remove("noodleBox");

	if(commitTarget.startsWith("#")){ 
	    var buf = document.getElementById(commitTarget.substr(1));
	    buf.textContent += timestamp + catspan;
	    buf.setAttribute("extent",buf.textContent.length);
	    alph.sources[commitTarget]["alph:textLength"] = buf.textContent.length;
	} else if (commitTarget.startsWith("http")){

	    var form = new FormData();
	    //form.append("media",catspan);
	    form.append("media",timestamp + catspan);
	    //form.append("ts",timestamp);

	    this.postText(commitTarget,form,spanParent);
	}
    }
}

/* * */
Docuplextron.prototype.postText = function(target,form,spanParent,creds){
    var xhr = new XMLHttpRequest();
    xhr.onload = function(xhr,spanParent,target,form){
	var targetBase = target.split("/")[2];
	if(xhr.status == 200){
	    this.loadExternal(target);
	    // Update the media length from the server
	    alph.getDescription(target,false,true);
	    alph.fetchAndFill(spanParent);
	    this.postEDL();
	} else if(xhr.status == 401){
	    // We need to authenticate this request;
	    if(this.credStore[targetBase]){
		var creds = this.credStore[targetBase];
	    } else {
		// Prompt for credentials
		var creds = prompt("u:p").split(":");
		this.credStore[targetBase] = creds;
	    }
	    this.postText(target,form,spanParent,creds);
	} else {
	    console.log("Error with this: ",
			xhr.status, xhr.response, xhr.getAllResponseHeaders());
	}
    }.bind(this,xhr,spanParent,target,form);
    if(creds){
	xhr.open("POST",target,true,creds[0],creds[1]);
    } else {
	xhr.open("POST",target);
    }
    xhr.send(form);
}
/* * */

Docuplextron.edlToAlpHTML = function(edl){

    /* Read a Xanadu EDL document <http://xanadu.com/xuEDL.html>, and
       convert it to an Alph-flavored HTML document.*/
    
    var lines = edl.split("\n");
    var spans = [];
    for (var ix in lines){
	if(lines[ix].startsWith("span:")){
	    var splitline = lines[ix].replace("span:","");
	    splitline = splitline.trim();
	    splitline = splitline.split(",start=");
	    splitline = [splitline[0]].concat(splitline[1].split(",length="));
	    spans.push(new AlphSpan(splitline[0],
				    parseInt(splitline[1]),
				    parseInt(splitline[1]) + parseInt(splitline[2])
				   )
		      );
	} else if (lines[ix].startsWith("xanalink:")){
	    // Need to get Alph links working again first...
	} else {
	    /* Do nothing? */
	}
    }
    var article = document.createElement("article");
    article.classList.add("xanadoc");
    for(var ix in spans){
	article.appendChild(spans[ix].toDOMFragment());
    }
    return article;
}

/* * */

Docuplextron.prototype.exportContextHTML = function(context){
    /* In brief: make a copy of the DOM, rip all of the Docuplextron
       stuff out of it, then return the HTML of the clean DOM. */


    // Circumstances are a bit different if this is a multi-document view, or a
    // single document (or subordinate iframe) view.

    if(context == document.body){
	// Single-document mode -- maybe we're an iframe

	var ex = document.createDocumentFragment();
	var body = document.body.cloneNode(true);
	var head = document.head.cloneNode(true);
	ex.appendChild(head);
	ex.appendChild(body);

	// Now remove all of the Alph crap.
	try{ex.getElementById("alph-main").remove();} catch(e){console.log(e)};
	try{ex.getElementById("alph-footer").remove();} catch(e){console.log(e)};	
	try{ex.getElementById("alphOverlay").remove();} catch(e){console.log(e)};
	try{ex.getElementById("dptron-flap").remove();} catch(e){console.log(e)};
	try{ex.getElementById("alphDOMCursor").remove();} catch(e){console.log(e)};
	try{ex.getElementById("alphDOMCursorInfotext").remove();} catch(e){console.log(e)};
	
    } else if(context.tagName == "X-FLOATER"){
	// Multi-document view -- we're exporting the contents of a floater div, OR,
	// we are are exporting the contents of a subordinate IFRAME.

	var iframe = context.querySelector("iframe");
	if(iframe){
	    /* If this request is for the contents of an IFRAME, post a message
	       down to the IFRAME and get the ball rolling from there. */
	    iframe.contentWindow.postMessage({"op":"exportHTMLToParent"},"*");
	    return;
	}

	// Here's our container:

	var ex = document.createDocumentFragment();
	var head = document.createElement("head");
	var body = document.createElement("body");
	ex.appendChild(head);
	ex.appendChild(body);

	// First, clone the contents of the context node into our export document body

	var kids = Array.from(context.childNodes);
	for(var ix in kids){
	    body.appendChild(kids[ix].cloneNode(true));
	}
	
	/* ***
	   If we had some <body> style or other attributes , they've been lost, here.
	   Come up with a way to retain those.
	   ***

	   Next, figure out what to do for a <head>. If we're exporting the host document,
	   just copy the <head> of the window. If it's some kind of ephemeral view
	   (imported plain-text, new compositions, etc.), then use an empty <head>. */
	
	if(context.id == window.location.toString() ||
	   (context.id == (window.location.toString() + "(modified)")) ){
	    head = document.head.cloneNode(true);
	} else {
	    /* ...? */
	}
    }
    
    /* Next, look for items classed as "do-not-export", and remove them.
       This will include the Alph <script> and stylesheet <link> in the <head>.
       Also, if this was a slider/floater, get rid of the SVG overlay. */
    
    var killables = Array.from(ex.querySelectorAll(".do-not-export, .alph-floater-overlay"));
    while(killables.length > 0){
	var killme = killables.pop();
	killme.remove();
    }
    
    return "<!DOCTYPE html>\n<html>" + head.outerHTML + body.outerHTML + "</html>";

}

/* * */
Docuplextron.prototype.postMedia = function(method,data,bin,url){
    method = method || "PUT";
    bin = bin || true;    
    if(!data)return;
    console.log("Trying to ", method," this: ", data);
    if (!url){
	url = prompt("URL to publish to:");
	if(!url) return;
    } else {
	if( url.endsWith("(modified)")) {
	    url = url.split("(modified)")[0];
	}
	var verify = false;
	if(parent != window) {
	    verify = true;
	} else {
	    var verify = confirm("PUT to " + url + "?\n(This may overwrite/destroy data!)");
	}
	if(!verify) return;
    }
	
    var xhr = new XMLHttpRequest();
    var form = new FormData();
    form.append("media",data);
    if (bin) form.append("binary","t");
    xhr.onload = function(xhr,url){
	if (xhr.status == 200){
	    if(window.location != url){
		this.loadExternal(url);		
	    } else {
		//window.open(url);
	    }
	} else {
	    console.log("Post failed? Came back with this: ", xhr);
	}
    }.bind(this,xhr,url);
    xhr.open(method,url);
    xhr.send(form);    
}

/* * */

Docuplextron.prototype.exportContextToURL = function(context,url){
    if (!url){
	url = prompt("URL to publish to:");
	if(!url) return;
    } else {
	if( url.endsWith("(modified)")) {
	    url = url.split("(modified)")[0];
	}
	var verify = false;
	if(parent != window) {
	    verify = true;
	} else {
	    var verify = confirm("PUT to " + url + "?\n(This may overwrite/destroy data!)");
	}
	if(!verify) return;
    }
	
    var xhr = new XMLHttpRequest();
    var form = new FormData();
    form.append("media",this.exportContextHTML(context));
    form.append("binary","t");
    xhr.onload = function(xhr,url,context){
	if (xhr.status == 200){
	    
	    context.id = url;
	    if(window.location != url){
		this.loadExternal(url);		
	    } else {
		//window.open(url);
	    }
	}
    }.bind(this,xhr,url,context);
    xhr.open("PUT",url);
    xhr.send(form);
}

/* 




 EDITING




*/

Docuplextron.prototype.newComposition = function(){

    /* */
    
    var f = this.floaterManager.create(null,
				       "[" + Date.now().toString(36).toUpperCase() + "]"
				       ,this.lastX,this.lastY);
    var a = document.createElement("article");
    var frag = Alphjs.newSpan("~"+this.noodleManager.newdle().key,"0","9",true);
    frag.textContent = "-edit me-";
    a.appendChild(frag);
    f.appendChild(a);
    window.getSelection().removeAllRanges();
    window.getSelection().selectAllChildren(frag);
    frag.focus();
    return f;
}

/* * */

Docuplextron.prototype.newdleBox = function(noodleID,text,editable,atX,atY){
    var atX = atX || this.lastX;
    var atY = atY || this.lastY;
    var editable = editable || false;
    if(noodleID){
	var noodle = this.noodleManager.get(noodleID);
	if(noodle){
	    var text = noodle.text;
	} else {
	    var floater = this.floaterManager.create(null,null,atX,atY);
	    floater.appendChild(document.createTextNode("Fragment " + noodleID +
							" no longer exists."));
	    return floater;
	}
    } else {
	var noodle = this.noodleManager.newdle();
	var text = text || "\n";
	noodle.text = text;
    }
    var floater = this.floaterManager.create("noodleBox",
					 noodle.title || null,
					 atX,atY);
    var span = Alphjs.newSpan("~"+noodle.key,"0",text.length,editable);
    if(!noodle.title) floater.tab.text.textContent = " ";
    span.textContent = text;
    floater.style.width = "500px";
    floater.appendChild(span);
    window.getSelection().removeAllRanges();

    var r = new Range();
    r.selectNodeContents(span);
    r.collapse(true);
    window.getSelection().addRange(r);
    span.focus();
    floater.toTop();
    return floater;
}

/***/

Docuplextron.prototype.insertEditable = function(p){
    
    /* This thing's a mess. Needs to be designed properly. */
    
    var frag = Alphjs.newSpan("~" + this.noodleManager.newdle().key,"0","10",true);
    frag.textContent = "[editable]";
    if(!p){
	Alphjs.paste(frag);
    } else if (p == "sibling"){
	var ae = this.activeElement;
	if(ae.tagName.toLowerCase() != "x-text"){
	    var newSibling = document.createElement(ae.tagName.toLowerCase());
	    newSibling.appendChild(frag);
	} else {
	    var newSibling = frag;
	}
	ae.parentNode.insertBefore(newSibling,ae);
	ae.parentNode.insertBefore(ae,newSibling);
	this.DOMCursor.select(newSibling);
    } else if (p == "child"){
	this.activeElement.appendChild(frag);
	this.DOMCursor.select(frag);
    }
    window.getSelection().removeAllRanges();
    window.getSelection().selectAllChildren(frag);
    frag.focus();
}

/* * */

Docuplextron.prototype.domPopout = function(){

    /* DOM pop-out. Copies the currently selected DOM node into a new floater. */
    
    if(this.activeElement instanceof HTMLElement){
	var f = this.floaterManager.create(null,null,this.lastX,this.lastY);
	f.appendChild(this.activeElement.cloneNode(true));
    }
    this.postEDL();
    parent.postMessage({"op":"findMyMatchingContent"},"*");
}

/* * */

Docuplextron.prototype.popout = function(excise){

    var anchorItem = Docuplextron.getParentItem(window.getSelection().anchorNode.parentElement);
    if(anchorItem.classList.contains("noodleBox")){
	/* Create a new noodle with a new limbic span. */

	/* Selection.toString() would theoretically be easier, HOWEVER,
	   it destroys whitespace. So, this instead: */
	var newdleText = window.getSelection().getRangeAt(0).cloneContents().textContent;
	
	if(excise){
	    window.getSelection().deleteFromDocument();
	}
	
	var newdleBox = this.newdleBox(null,newdleText,true);
	newdleBox.toTop();
	
    } else {
	/* Create a new composition pane with whatever's in alph.selection. */
    
	if(parent != window){
	    // Escalate to parent. The selection should already be up there.
	    parent.postMessage({"op":"popout"},"*");
	    return;
	}
	
	var art = document.createElement("article");
	art.appendChild(alph.selection.toFragment());
	alph.fetchAndFill(art);
	
	if(excise){
	    alph.cut();
	}
	
	var floater = this.floaterManager.create(null,null,this.lastX,this.lastY);
	floater.appendChild(art);
	floater.style.width = "640px";
	this.postEDL();
	parent.postMessage({"op":"findMyMatchingContent"},"*");
    }
}

/***/

Docuplextron.prototype.merge = function(){

    /* Combine the currently active noodles into a new noodle. */

    var noodleBoxen = [];
    var noodleIDs = [];
    
    for (var ix in dptron.activeItems){
	if(dptron.activeItems[ix].classList.contains("noodleBox")){
	    noodleBoxen.push(dptron.activeItems[ix]);
	    noodleIDs.push(
		dptron.activeItems[ix].querySelector("x-text").getAttribute("src").substr(1)
	    );
	}
    }

    var catText = "";

    noodleBoxen.sort(function(a,b){
	if ( parseFloat(a.style.top) < parseFloat(b.style.top) ){
	    return -1;
	} else {
	    return 1;
	}
    });
    
    var topStyle = noodleBoxen[0].style.cssText;
    var initialNoodle = this.noodleManager.get(noodleBoxen[0].querySelector("x-text").getAttribute("src"));
    var initialX = parseFloat(noodleBoxen[0].style.left);
    var initialY = parseFloat(noodleBoxen[0].style.top);
    var initialWidth = noodleBoxen[0].style.width;
    
    for (var ix in noodleBoxen){
	if(!catText.endsWith("\n")){
	    catText += "  ";
	}
	catText += noodleBoxen[ix].querySelector("x-text").textContent;
	noodleBoxen[ix].splode();
    }

    var noodleOne = this.noodleManager.newdle();
    noodleOne.progenitors = noodleIDs;
    noodleOne.text = catText;
    noodleOne.title = initialNoodle.title;
    var xtextOne = new Alphjs.newSpan("~" + noodleOne.key,
				      0, noodleOne.text.length, true);
    xtextOne.textContent = noodleOne.text;
    var floaterOne = this.floaterManager.create("noodleBox",
						null,
						initialX,initialY);
    floaterOne.appendChild(xtextOne);
    floaterOne.tab.text.textContent = noodleOne.title || " ";
    //floaterOne.style.width = initialWidth;
    floaterOne.style.cssText = topStyle;
    
    return floaterOne;

}

/***/

Docuplextron.prototype.split = function(shifted){
    
    /* Do a noodle split or Alph split?  */

    var returnSpans = [];

    var sel = window.getSelection();
    var range = sel.getRangeAt(0);
    var anchor = sel.anchorNode;

    /* We want to be inside of a text node, which should be inside of an
       X-TEXT, which should have a limbic source, and should be inside of
       a noodleBox. */
    
    if(anchor.nodeType == 3 &&
       anchor.parentElement.tagName == "X-TEXT" &&
       anchor.parentElement.getAttribute("src").startsWith("~") &&
       Docuplextron.getParentItem(anchor.parentElement).classList.contains("noodleBox")){

	var sourceXtext = anchor.parentElement;
	var sourceText = sourceXtext.textContent;
	var sourceNoodle = this.noodleManager.get(sourceXtext.getAttribute("src").substr(1));
	var sourceFloater = Docuplextron.getParentItem(sourceXtext);

	/* Make absolutely sure that Lamian has the current version of
	   the noodle before we destroy its container later. */

	sourceNoodle.text = sourceText;

	/* Now, make the new noodles, spans, and floaters, and replace the 
	   previous one. */

	var r1 = sourceFloater.getBoundingClientRect();
	
	var noodleOne = this.noodleManager.newdle();
	noodleOne.progenitors.push(sourceNoodle.key);
	noodleOne.text = sourceText.substr(0,range.startOffset);
	var floaterOne = this.newdleBox(noodleOne.key,null,true,r1.left,r1.top);
	floaterOne.style.width = sourceFloater.style.width;


	var r2 = floaterOne.getBoundingClientRect();
	

	var noodleTwo = this.noodleManager.newdle();
	noodleTwo.progenitors.push(sourceNoodle.key);
	noodleTwo.text = sourceText.substr(range.startOffset);
	var floaterTwo = this.newdleBox(noodleTwo.key,null,true,r2.left,r2.bottom);
	floaterTwo.style.width = sourceFloater.style.width;
	
	sourceFloater.splode();

	//return [xtextOne,xtextTwo];
	return [floaterOne.querySelector("x-text"), floaterTwo.querySelector("x-text")];
	
    } else {
	return Alphjs.split(shifted);
    }
}


/* 






 MISCELLANY






*/

Docuplextron.titleTrim = function(txt,len){
    
    /* This is really stupid right now, but it's a placeholder. */

    if(!txt) return;

    var len = len || 80;

    if(txt.length > 80){
	return txt.substr(0,77) + "...";
    } else {
	return txt;
    }

}

Docuplextron.getParentById = function(el,id){

    /* Hunt back up the DOM tree for a parent element with the passed id. */
    
    if (!id) return false;

    try{
	if(!el.nodeType){
	    return false;
	} else if(el.nodeType == Node.TEXT_NODE){
	    var p = el.parentElement;
	} else if(el.nodeType == Node.ELEMENT_NODE){
	    var p = el;
	} else {
	    return false;
	}
    } catch(e){
	debugger;
    }

    while(p && p.id != id){
	p = p.parentElement;
    }
    
    if(!p){
	return false
    } else {
	return p;
    }
    
}


Docuplextron.getParentItem = function(el,tag){

    /* Hunt back up the DOM tree for a parent element of the passed type. */
    
    var tag = tag || "X-FLOATER";

    try{
    if(!el.nodeType){
	return false;
    } else if(el.nodeType == Node.TEXT_NODE){
	var p = el.parentElement;
    } else if(el.nodeType == Node.ELEMENT_NODE){
	var p = el;
    } else {
	return false;
    }
    } catch(e){ debugger; }

    while(p && p.tagName != tag){
	p = p.parentElement;
    }
    
    if(!p){
	return false
    } else {
	return p;
    }
}

/***/

Docuplextron.frameOf = function(win){
    // Because .frameElement of cross-domain iframes is restricted, we
    // sometimes have to iterate over the <iframe>s in the document to
    // find the container of an incoming message.
    if(win == window){ return window };
    try{
	var f = win.frameElement;
    } catch(e) {
	var iframes = Array.from(document.getElementsByTagName("IFRAME"));
	for(var ix in iframes){
	    if (iframes[ix].contentWindow == win){
		return iframes[ix];
	    }
	}
	//console.log("Couldn't find iframe.");
	return false;
    }
    return f;
}

/***/

Docuplextron.prototype.reportSizeToParent = function(){
    if(parent != window){
	var docStyle = window.getComputedStyle(document.body);
	var scrollHeight = document.body.scrollHeight;
	
	var w = parseFloat(docStyle["margin-left"]) +
	    parseFloat(docStyle["margin-right"]) +
	    parseFloat(docStyle["width"]) +
	    parseFloat(docStyle["padding-left"]) +
	    parseFloat(docStyle["padding-right"]);
	var h = parseFloat(docStyle["margin-top"]) +
	    parseFloat(docStyle["margin-bottom"]) +
	    parseFloat(docStyle["height"]) +
	    parseFloat(docStyle["padding-right"]) +
	    parseFloat(docStyle["padding-left"]);
	
	parent.postMessage({"op":"sizeReport",
			    "width":w+"px",
			    "height":Math.max(h,scrollHeight)+"px"},
			   "*");
    }
}

/***/

Docuplextron.prototype.saveSelectionRanges = function(){

    /* When we manipulate DOM elements that contain the current selection
       (or caret), this and restoreSelectionRanges() are used to restore the selection.*/
    
    var sel = window.getSelection();
    var ranges = [];

    for (var ix = 0; ix < sel.rangeCount; ix++){
	ranges.push(sel.getRangeAt(ix));
    }
    this.savedRanges = ranges;
}

/***/

Docuplextron.prototype.restoreSelectionRanges = function(){

        /* When we manipulate DOM elements that contain the current selection
       (or caret), this and saveSelectionRanges() are used to restore the selection.*/

    if (!this.savedRanges) return;
    var sel = window.getSelection();
    sel.removeAllRanges();
    while(this.savedRanges.length > 0){
	sel.addRange(this.savedRanges.shift());
    }
}

/***/

Docuplextron.prototype.gotoHelp = function(){
    var flap = this.bar.querySelector("#dptronSideFlap");
    if(flap.style.right != "0px") this.toggleFlap();
    this.showFlapTab("help");
}

/***/

Docuplextron.prototype.toggleFlap = function(){

    var flap = this.bar.querySelector("#dptronSideFlap");
    var toggle = flap.querySelector("#sideFlapTabToggle");
    if (flap.style.right == "0px"){
	toggle.style.transform = "rotate(0deg)";
	toggle.style.left = "-32px";
	flap.style.right = "-500px";
	flap.style.overflow = "visible";
    } else {
	this.updateSourcesFlap();
	this.updateLinksFlap();
	toggle.style.left = "0px";
	toggle.style.transform = "rotate(180deg)";
	flap.style.right = "0px";
	flap.style.overflowX = "hidden";
	flap.style.overflowY = "hidden";
    }
}

/***/

Docuplextron.prototype.showFlapTab = function(tab){
    var flap = this.bar.querySelector("#dptronSideFlap");
    var tabs = {"help":{"toggle":"#helpPaneToggle",
			"pane":"#helpPane"},
		"sources":{"toggle":"#sourcesPaneToggle",
			   "pane":"#sourcesPane"},
		"settings":{"toggle":"#settingsPaneToggle",
			    "pane":"#settingsPane"},
		"session":{"toggle":"#sessionPaneToggle",
			   "pane":"#sessionPane"},
		"links":{"toggle":"#linksPaneToggle",
			 "pane":"#linksPane"}
	       };
    
    var tabKeys = Object.keys(tabs);
    for(var ix in tabKeys){
	var tabKey = tabKeys[ix];
	var thisTab = tabs[tabKey];
	var thisToggle = this.bar.querySelector(thisTab.toggle);
	var thisPane = this.bar.querySelector(thisTab.pane);
	if(tabKey == tab){   
	    thisToggle.classList.add("dptronActiveTab");
	    thisPane.style.display = "block";
	} else {
	    thisToggle.classList.remove("dptronActiveTab");
	    thisPane.style.display = "none";
	}
    }

    if(tab == "sources"){
	this.updateSourcesFlap();
    } else if(tab == "session"){
	this.updateSessionList();
    }
}

/***/

Docuplextron.taintId = function(el){

    /* Simply append "(modified)" to the id of a context view that has
       been altered. */
    
   if(Docuplextron.getParentItem(el).id){
	if(!Docuplextron.getParentItem(el).id.endsWith("(modified)")){
	    Docuplextron.getParentItem(el).id = Docuplextron.getParentItem(el).id + "(modified)";
	}
    }
}

/***/

Docuplextron.deParent = function(node){
    
    /* Pass me an element, and I will replace that element with its children. */
    
    var frag = document.createDocumentFragment();
    while(node.firstChild){
	frag.appendChild(node.firstChild);
    }
    node.parentElement.replaceChild(frag,node);
    return frag.firstElementChild;
}

/***/

Docuplextron.broadcast = function(msg){

    /* Use the postMessage() interface to send a message down to all subordinate
       contexts (iframes) */
    
    var contexts = Array.from(document.getElementsByTagName("IFRAME"));
    if(contexts.length > 0){
	for(var ix in contexts){
	    contexts[ix].contentWindow.postMessage(msg,"*");
	}
    }
}

/***/

Docuplextron.prototype.undo = function(){
    
    /* Disabled. Crudely implemented at the very beginning; not improved since. */
    
    if(this.stateSaves.length > 0){
	//this.mainDiv.innerHTML = this.stateSaves.pop();
    }
}

/***/

Docuplextron.prototype.activate = function(el,multi){
    
    /* Set our activeItem and do any required representational modifications on it.*/

    if(el.classList.contains("nonContextual")) {
	if(!multi){
	    el.toTop();
	}
	return;
    }
    
    if(!multi){
	for(var ix in this.activeItems){
	    if(this.activeItems[ix] != el){
		this.deactivate(this.activeItems[ix]);
	    }
	}
	this.activeItems = [el];
	el.classList.add("alphActiveItem");
	if(el != this.mainDiv){
	    el.tab.classList.add("alphActiveItem");
	    el.toTop();
	}
    } else{
	var itemIx = this.activeItems.indexOf(el);
	if(itemIx >= 0){
	    // Floater is already in the array. Remove it.
	    this.deactivate(el);
	    this.activeItems.splice(itemIx,1);
	} else {
	    el.classList.add("alphActiveItem");
	    el.tab.classList.add("alphActiveItem");
	    this.activeItems.push(el);
	}
    }
    //console.log(this.activeItems);
}

/***/

Docuplextron.prototype.deactivate = function(floater){
    
    /* Do things to the no-longer active item... */
    
    floater.classList.remove("alphActiveItem");
    if(floater != this.mainDiv){
	floater.tab.classList.remove("alphActiveItem");
    }
}

/***/

Docuplextron.prototype.gradientMorpher = function(tron){
    
    /* Totally unnecessary, but a pleasant effect. Mutates the
       background gradient of the workspace. */

    if(document.getElementById("dptronGradMorphToggle").checked){
    
	if(tron.mainDiv.style.background.includes("linear-gradient")){
	    g = tron.mainDiv.style.background;
	} else if(window.getComputedStyle(tron.mainDiv).backgroundImage.includes("linear-grad")){
	    g = window.getComputedStyle(tron.mainDiv).backgroundImage;
	} else return;
	var deg = g.split("deg")[0];
	deg = deg.split("(");
	deg = parseInt(deg[deg.length-1]);
	g = g.split("rgb(");
	var gb = g[2].trim('"').split(')')[0].split(',');
	g = g[1].trim('"').split(")")[0].split(',');
	
	var g_ix = parseInt(Math.random()*4);
	var gb_ix = parseInt(Math.random()*4);
	var r1 = 1 - parseInt(Math.random()*3);
	var r2 = 1 - parseInt(Math.random()*3);
	var r3 = 1 - parseInt(Math.random()*3);
	
	g[g_ix] = Math.max(Math.min(parseInt(g[g_ix]) + r1, 255),0);
	gb[gb_ix] = Math.max(Math.min(parseInt(gb[gb_ix]) + r2, 256),0);
	deg = (parseInt(deg) + r3) % 360;
    
	tron.mainDiv.style.background = "linear-gradient(" + deg + "deg, rgb(" + g[0] + ", " + g[1] + ", " + g[2] + "), rgb(" + gb[0] + ", " + gb[1] + ", " + gb[2] + "))";
    }
    tron.morpherTimeout = setTimeout(tron.gradientMorpher,300,tron);
}

Docuplextron.prototype.sessionSave = function(dryrun){
    
    /* Save sessions to local storage. */
    
    var restore = [];
    var floaters = this.floaterManager.getMembers();
    for (var ix in floaters){
	var floater = floaters[ix];
	var fSave = {};
	if(floater.classList.contains("noodleBox")){
	    fSave.type = "noodle";
	    fSave.source = floater.querySelector("x-text").getAttribute("src");
	} else if(floater.style.display != "none"){
	    fSave.type = "composition";
	    if(floater.id.startsWith("http")){
		fSave.source = floater.id;
	    } else {
		var floaterClone = floater.cloneNode(true);
		floaterClone.removeChild(floaterClone.querySelector("svg.alph-floater-overlay"));
		fSave.source = encodeURIComponent(floaterClone.outerHTML);
	    }
	} else {
	    continue;
	}
	
	/* There is almost certainly a better way to do this. Save all attributes to an 
	   attributes property or something. */
	
	fSave.z = parseInt(floater.getAttribute("data-z")) || parseInt(floater.style.zIndex) || 100;
	fSave.scale = floater.getAttribute("data-scale") || parseFloat(floater.style.transform.match(/scale\((.*)\)/)[1]);
	fSave.sticky = floater.sticky;
	fSave.style = floater.style.cssText;

	restore.push(fSave);
    }

    // Save links...
    var linkKeys = Object.keys(this.linkStore);
    for (var ix in linkKeys){
	var l = this.linkStore[linkKeys[ix]];
	if(l.id.startsWith("~") || l.retain ){
	    restore.push({"type":"link",
			  "link": l });
	}
    }
    
    // Save some other settings...

    restore.push(
	{
	    "type":"dptronState",
	    "mainDivStyle":this.mainDiv.style.cssText
	}
    );
    
    if(!dryrun){
	localStorage.setItem("dptronSession-"+window.location.toString(),
			     JSON.stringify(restore));
    }
    //console.log(restore);
    return restore;
}

Docuplextron.prototype.exportSession = function(){
    /* Hrm. Let's just try this... */
    window.open("data:application/json;encoding=utf-8," +
		encodeURIComponent(JSON.stringify(this.sessionSave(true)))
	       );
}

Docuplextron.prototype.autoRestore = function(){
    if(localStorage["dptronSession-"+window.location.toString()]){
	this.sessionRestore("dptronSession-"+window.location.toString());
    }
}
	
Docuplextron.prototype.sessionRestore = function(session,merge){   
    var restore;

    if(!merge){
	this.floaterManager.destroyAll();
    }

    /* Turn OFF session auto-save. In the event that this restore fucks-up,
       we don't want to automaticall over-write the old session, do we? */
    dptron.autoSaveSafe = false;
    
    if(session){
	try{
	    restore = JSON.parse(localStorage.getItem(session));
	} catch(e){
	    console.log("DPTRON SESSION RESTORE FAILED: ",e);
	    return false;
	}
    } else {
	restore = {};
	delete this.mainDiv.backgroundColor;
	delete this.mainDiv.backgroundImage;
    }

    console.log("Trying to restore session from: ",restore);
    
    for(var ix in restore){
	var floater = restore[ix];
	if(floater.type == "dptronState"){
	    if(floater.mainDivStyle){
		this.mainDiv.style.cssText = floater.mainDivStyle;
	    }
	} else if(floater.type == "link"){
	    try{
		var l = floater.link;
		// We want body/target nodes to be AlphSpan objects, I think!
		for(var n in l.body.items){
		    l.body.items[n] = new AlphSpan(l.body.items[n].src,
						   l.body.items[n].origin,
						   l.body.items[n].extent,
						  l.body.items[n].name);
		}
		for(var n in l.target.items){
		    l.target.items[n] = new AlphSpan(l.target.items[n].src,
						     l.target.items[n].origin,
						     l.target.items[n].extent,
						    l.target.items[n].name);
		}
		this.linkStore[l.id] = l;

	    } catch(e){
		console.log(e);
	    }
	} else {
	    if(floater.type == "composition"){
		if(floater.source.startsWith("http")){
		    var f = this.loadExternal(floater.source);
		    if(!f) continue;
		    //console.log("Got ", f, " back from loadExternal()");
		} else {
		    
		    /* Make a DOM from the stored HTML fragment */
		    
		    var parser = new DOMParser();
		    var t = parser.parseFromString(decodeURIComponent(floater.source),"text/html").querySelector("x-floater");

		    /* Because t is an <x-floater>, but NOT a Floater
		       object, we have to copy over its children, and
		       reassign its attributes to a newly-created
		       floater to make it work properly. */
		    
		    var f = this.floaterManager.create(null,t.id);
		    for (var iy in t.children){
			try{
			    f.appendChild(t.children[iy]);
			} catch(e){
			    //console.log(t.children,iy);
			}
		    }
		    
		    for (var iy in Object.keys(t.attributes)){
			f.setAttribute(
			    t.attributes[iy].name,
			    t.attributes[iy].value);
		    }
		}
	    } else if(floater.type == "noodle"){
		var f = this.newdleBox(floater.source,null,true);
	    }

	    /* Put this whole mess in a try{} block... ugh. */

	    try{
		f.style.cssText = floater.style;

		if(f.style.transform){
		    f.setAttribute("data-scale", parseFloat(f.style.transform.match(/scale\((.*)\)/)[1]));
		} else {
		    f.setAttribute("data-scale", floater.scale);
		    f.style.transform = "scale(" + floater.scale + ");";
		}

		if(f.style.zIndex){
		    f.setAttribute("data-z",f.style.zIndex);
		} else {
		    f.setAttribute("data-z", floater.z);
		    f.style.zIndex = floater.z;
		}

		if(floater.sticky){
		    f.sticky = floater.sticky;
		    f.setAttribute("data-sticky",f.sticky);
		}
	    } catch(e){
		console.log("Floater styling error: ", e);
	    }

	}
    }
    alph.getXSources();
    alph.fetchAndFill();
    dptron.autoSaveSafe = true;

    return true;
}

Docuplextron.highlightMatching = function(text,matchtext,container){
    var matchIx = 0;
    while(matchIx >= 0){
	var matchIx = text.toLowerCase().indexOf(matchtext.toLowerCase());
	if(!matchIx || matchIx < 0) break;
	
	var matchHead = text.substring(0,matchIx);
	var matchBody = document.createElement("mark");
	matchBody.textContent = text.substr(matchIx,matchtext.length);
	container.appendChild(document.createTextNode(matchHead));
	container.appendChild(matchBody);
	text = text.substring(matchIx + matchtext.length);
    }
    container.appendChild(document.createTextNode(text));
}

Docuplextron.makePaletteItems = function(items,container){
    for(var ix in items){
	var item = items[ix].split("|");
	var span = document.createElement("span");
	span.className = "dptronPaletteItem";
	var marked = document.createElement("mark");
	marked.textContent = item[1];
	span.appendChild(document.createTextNode(item[0]));
	span.appendChild(marked);
	span.appendChild(document.createTextNode(item[2]));
	container.appendChild(span);
    }
}

Docuplextron.colorIxFromStyle = function(color){
    if(!color || color == "transparent") return 0;
    color = color.split("(")[1];
    color = color.split(")")[0];
    color = color.split(",");
    for(var ix in color){
	color[ix] = parseInt(color[ix]);
    }
    
    return (color[0] * 65536) + (color[1] * 256) + color[2];
}

Docuplextron.hexColorFromIx = function(index){
    var r = parseInt(index / 64);
    var g = parseInt((index - (r * 64)) / 8);
    var b = index % 8;
    console.log(r,g,b);
    return "#" + parseInt(2 * r,16) + parseInt(2 * g,16) + parseInt(2 * b,16);
}

Docuplextron.pad = function(str,len){
    if(str.length >= len) return str;
    var padString = "";
    for(var ix = str.length; ix < len; ix++){
	padString += "0";
    }
    return "" + padString.toString() + str;
}

Docuplextron.colorSpin = function(color,dir){
    //var colorIX = ((dir * (524538 + 2048 + 8)) +
    //var colorIX = ((dir * (459002 + 1792 + 7)) +
    var colorIX = ((dir * (1016293 + 3968 + 15)) +
		   Docuplextron.colorIxFromStyle(color)) % 16777216;
    if (colorIX < 0){
	colorIX = 16777216 + colorIX;
    }
    return Docuplextron.pad(colorIX.toString(16),6);
}
/* 




   ARRANGEMENT




*/

Docuplextron.prototype.showActiveElement = function(){
    
    /*  If the activeElement is not inside of the visible workspace,
	move its container into view. */
    
    if(this.activeElement.nodeType != Node.ELEMENT_NODE){
	var r = this.activeElement.parentElement.getBoundingClientRect();
    } else {
	var r = this.activeElement.getBoundingClientRect();
    }
    
    if(r.top > window.innerHeight){
	var pe = Docuplextron.getParentItem(this.activeElement);
	if(pe){
	    var oldTop = parseInt(pe.style.top);
	    pe.style.top = oldTop - (r.height * 2) + "px";
	}
    } else if(r.top < 0){
	var pe = Docuplextron.getParentItem(this.activeElement);
	if(pe){
	    pe.style.top = "0px";
	}
    }
    this.DOMCursor.select(this.activeElement);
}

/***/

Docuplextron.prototype.lineEmUp = function(){
    
    /* Find all the floaters and put 'em in a row. Panic move. */
    
    var floaters = Array.from(document.querySelectorAll(".alph-floater"));
    for(var ix in floaters){
	var f = floaters[ix];
	f.setAttribute("data-z",50);
	f.setAttribute("data-scale",0.5);
	f.style.transform = "scale(0.5)";
	f.style.zIndex = "50";
	f.style.top = this.bar.getBoundingClientRect().bottom + "px";
	if(ix > 0){
	    f.style.left = (parseInt(floaters[ix-1].style.left) + floaters[ix-1].getBoundingClientRect().width) + "px";
	} else {
	    f.style.left = "0px";
	}
    }
}

/*





  LINKS 
  (a hot mess)




*/

/* Some of this functionality should be over in Alph.js as it is core
   Alph stuff, but it's so embryonic at the moment, I'm gestating it
   in here. */

function storeLink(link){

}

/***/

function parseXanalink(linktext){
    return false;
}

/***/

Docuplextron.derefNodes = function(nodes){

    /* Dereference the URL strings in the "nodes" property of an ALD 
       into AlphSpan objects. 

       This  relies on AlphSpan.fromURL(), which is quite primitive at the
       moment and only works on integer-range: fragment specifiers.
*/
    
    var returnArray = [];
    for(var ix in nodes){
	var nodeObject = {"spans":[]};
	returnArray.push(nodeObject);
	var nodeEdl = nodes[ix];
	for(var iy in nodeEdl){
	    nodeObject.spans.push({"span":AlphSpan.fromURL(nodeEdl[iy]),
				   "keys":[]});
	}
    }
    return returnArray;
}

/***/

Docuplextron.prototype.pushAllLinks = function(){
    
    /* Find all of the <link> and <a> links in the document
       (workspace?), convert them to an Alph representation, then post
       them out to the parent context.*/
    
    var headlinks = Array.from(document.head.querySelectorAll('link'));
    
    if(this.dptronMode){	
	var anchors = Array.from(document.getElementById("alph-floaters").querySelectorAll('a'));
	console.log("found anchors: ",anchors);
    } else {
	var anchors = Array.from(document.body.querySelectorAll('a'));

	/* If we're not in Docuplextron mode, we still don't want to get any
	   anchors that are part of the Alph crap that's been loaded into the page,
	   so...
	
	   Ugh. This is so ugly. */
	
	var nonalph = [];
	for(var ix in anchors){
	    if (!Docuplextron.getParentById(anchors[ix],"alph-main") &&
		!Docuplextron.getParentById(anchors[ix],"dptron-flap") &&
		!Docuplextron.getParentById(anchors[ix],"alphOverlay") &&
		!Docuplextron.getParentById(anchors[ix],"alph-footer") &&
		!Docuplextron.getParentById(anchors[ix],"alph-DOMCursor") &&
		!Docuplextron.getParentById(anchors[ix],"alph-DOMCursorInfoText")
	       ){
		nonalph.push(anchors[ix]);	
	    }
	}
	anchors = nonalph;
    }
    
    for(var ix in headlinks){
	
	/* Convert <LINK> elements in the <HEAD> into our internal link
	   representation. */
	
	var hlink = headlinks[ix];
	var alink = Alink.fromLink(hlink);
	this.linkStore[alink.id] = alink;
	//this.linkStore.push(Alink.fromLink(hlink));	
    }
    
    for(var iy in anchors){
	var anchor = anchors[iy];
	if(!anchor.href) continue;

	if (this.dptronMode){
	    var anchorFloater = Docuplextron.getParentItem(anchor);
	    var link = new Alink(
		anchorFloater.id + (
		    (anchor.id != "") ?
			("#" + anchor.id) :
			("#xpath:" + Alphjs.getXPath(anchor,anchorFloater))
		),
		"Jumplink",
		Alinks.bodyFromAnchor(anchor),
		Alinks.targetFromAnchor(anchor));
	} else {
	    var link = new Alink(
		document.location.toString() + (
		    (anchor.id != "") ?
			("#" + anchor.id) :
			("#xpath:" + Alphjs.getXPath(anchor))
		),
		"Jumplink",
		Alinks.bodyFromAnchor(anchor),
		Alinks.targetFromAnchor(anchor));

	}
	
	if(anchor.rel != "") link.rel = anchor.rel;
	if(anchor.title != "") link.title = anchor.title;
	
	// Replace this with a postMessage() eventually
	this.linkStore[link.id] = link;
	//this.linkStore.push(link);
    }
    this.buildLinkpointerSpans();
}

/***/

Docuplextron.prototype.buildLinkpointerSpans = function(){

    /* Go through every link, and every span, and search for matches
       against all the EDLs in edlStore. When matches are found, post
       "requestLinkRects" 

       Note:

       The three functions in this cascade (buildLinkpointerSpans(),
       buildLinkpointerSpansOf(), and findLinkTerminals() ) need to be
       smarter about when to match and when not to.  We want partial
       content matches to look different than complete content
       matches, and we need to match against ~collapsed~ EDLs, not the
       DOM-fragmented EDLs that we have in edlStore.
*/

    this.linkpointerSpans = [];
    for(var ix in this.linkStore){
	var link = this.linkStore[ix];
	this.buildLinkpointerSpansOf(link);
    }
}

/***/

Docuplextron.prototype.buildLinkpointerSpansOf = function(link){

    /* See Note in buildLinkpointerSpans() */

    /* So, this can get a bit beastly. We have to be able to handle
       links in which the body/target may be any of the following:

       - "url"
       - {source:"",selector:""}
       - {items:[ ... ]} – where each item is a full body/target itself

       And, of course, a URL could be to another linkage structure, so there
       are a couple of ways in which this could recurse.

       
    */
    
    for (var iy in link.nodes){
	var node = link.nodes[iy];
	for (var iz in node.spans){
	    var span = node.spans[iz].span;
	    node.spans[iz]["keys"] = this.findLinkTerminals(span);
	}
    }
}

/***/

Docuplextron.prototype.getNodeRects = function(node){
    /* 
       Return DOMRects highlighting the media in the span.

       node should be an AlphSpan. 

       Because we may be returning multiple DOMRect arrays, we return an
       array of objects with a reference to the containing floater, an 
       array of DOMRects, and - because we may be returning rects for
       partial matches, a flag indicating the completeness of match:

           { 
	     floater: reference_to_containing_floater,
             rects: [DOMRects...],
	     clip: "none" || "origin" || "extent" || "both"
	   } 

       -----

       Right now this only works for media in the docuplextron,
       not for <IFRAME> contents.

       This function is becoming humongous. Needs to be modularized. A lot.

    */
    
    var groupArrays = [];

    if(!node.origin && node != 0){

	/* No origin/extent means the target is a document or
	   DOM element. That's easy! */
	
	var hashIx = node.src.indexOf("#");
	
	if(node.src.includes("xpath:")){

	    /* Element specified with XPath */

	    /* We need to find the contextNode for the XPath. If we
	       imported any HTML fragments from Web sources with
	       loadExternal(), we will have set the content-source
	       attribute on the floater with the source document's
	       URL. */
	    
	    var splitSrc = node.src.split("#xpath:");
	    var floaters = Array.from(document.body.querySelectorAll(
		"[content-source='" + splitSrc[0] + "']"));
	    for(var iy in floaters){
		var anch = document.evaluate(
		    splitSrc[1],
		    floaters[iy],
		    null,
		    XPathResult.ANY_TYPE,
		    null);
		var el;
		while(el = anch.interateNext()){
		    /* Every el SHOULD be a match for the passed AlphSpan. 
		       Get its rectangles and store them ...somewhere. */
		    
		    groupArrays.push(
			{
			    "floater": floaters[iy],
			    "rects": Array.from(el.getClientRects())
			}
		    );

		}
	    }   
	} else if(node.src.includes("#")) {
	    
	    /* Named fragment */
	    
	} else {
	    
	    /* Whole document */

	    if(node.src.startsWith("~")){
		var floaters = Array.from(document.body.querySelectorAll(
		    "x-text[src='" + node.src + "']"));
	    } else {
		var floaters = Array.from(document.body.querySelectorAll(
		    "[content-source='" + node.src + "']"));
	    }
	    for(var iy in floaters){
		groupArrays.push(
		    {
			"floater" : Docuplextron.getParentItem(floaters[iy]),
			"rects" : Array.from(floaters[iy].getClientRects())
		    }
		);
	    }
	    var f = document.getElementById(node.src);
	    if(f){
		groupArrays.push(
		    {
			"floater" : f,
			"rects" : Array.from(f.getClientRects())
		    }
		);
	    }
	}
    } else {
	/* So, the span has an origin/offset pair. That means this is
	   a range of some kind. We already have a function that does
	   this for text (getMatchingContentRects()), but I'm going to
	   re-write/-factor that here, in hopes of making a better
	   version. This also -- perhaps stupidly -- does some of the
	   work of the findMatching...() functions. */

	if (node.origin.toString().includes("xpath:")){
	    
	    // *** DEPRECATED ***

	    /* This is a DOM Range

	       We do DOM Ranges with two XPaths for the start and end node,
	       and each node is optionally suffixed with an XPath 2.0 comment
	       indicating the offset, like so:

	       xpath://p[1]/a/text()[0](:12:)

	    */
	    var floaters = Array.from(document.body.querySelectorAll(
		"[content-source='" + node.src + "']"));
	    
	    // * WHERE'S THE REST OF THIS, ADAM? *
	    
	} else if(node.origin.toString().startsWith("point(")){
	    
	    /* This is a point() XPointer (without an offset, it's the
	       same as an element() XPointer). Two of these give us an
	       easy-peasy DOM Range. */
	    
	    var floaters = Array.from(document.body.querySelectorAll(
		"[content-source='" + node.src + "'],[content-source^='" + node.src + "#']"));

	    for(var ix in floaters){
		var clip;
		var rootNode = floaters[ix]; //floaters[ix].parentElement;
		var anchor = Alphjs.resolvePoint(node.origin,rootNode);
		var focus = Alphjs.resolvePoint(node.extent,rootNode);

		/* If we have neither anchor nor focus nodes, we're 
		   fucked, so we quit. However, if we have one or the 
		   other, this is at least a PARTIAL match. So, we return
		   a range that goes either to the start or end of the 
		   rootNode. */
		
		if(!anchor && !focus) continue;
		var rng = new Range();

		if(!anchor){
		    rng.setStartBefore(floaters[ix]);
		    rng.setEnd(focus[0],focus[1]);
		    clip = "origin";
		} else if(!focus){
		    rng.setStart(anchor[0],anchor[1]);
		    rng.setEndAfter(floaters[ix]);
		    clip = "extent";
		} else {
		    rng.setStart(anchor[0],anchor[1]);
		    rng.setEnd(focus[0],focus[1]);
		    clip = "none";
		}

		groupArrays.push(
		    { "floater" : Docuplextron.getParentItem(floaters[ix]),
		      "rects" : Array.from(rng.getClientRects()),
		      "clip" : clip
		    }
		);		
	    }
	} else {
	    /* Are there any elements in the workspace that have this
	       source as an src value?  We look for either the source
	       exactly, or a source with a fragment, which we
	       initially ignore. This is because audiovisual media
	       carry their fragment specifier in the 'src' attribute,
	       while <X-TEXT> has origin/extent attrs. */

	    var sources = Array.from(document.body.querySelectorAll(
		"[src='" + node.src + "'],[src^='" + node.src + "#']"));
	    for(var iy in sources){
		switch(sources[iy].tagName.toLowerCase()){
		case "x-text":
		    var xel = AlphSpan.fromXtext(sources[iy]);
		    
		    // Test for a content intersection

		    var xsect = node.intersection(xel);
		    if(!xsect) continue;

		    // If we have an intersection, use a Range object
		    // to get client rects for the relevant portion of
		    // the Text node

		    var clip;
		    if(xsect.origin == node.origin){
			clip = (xsect.extent == node.extent) ?
			    "none" : "extent";
		    }else{
			clip = (xsect.extent == xsect.extent) ?
			    "origin" : "both";
		    }
		
		    var rng = document.createRange();
		    var rngStart = xsect.origin - xel.origin;
		    rng.setStart(sources[iy].childNodes[0],rngStart);
		    rng.setEnd(sources[iy].childNodes[0],
			       (xsect.extent - xsect.origin) + rngStart);

		    groupArrays.push(
			{
			    "floater": Docuplextron.getParentItem(sources[iy]),
			    "rects" : Array.from(rng.getClientRects()),
			    "clip" : clip
			}
		    );
		    break;
		case "img":
		    break;
		case "video":
		    break;
		case "audio":
		    break;
		case "source":
		    break;
		}
	    }

	}
    }
    return groupArrays;
    
}

/***/

Docuplextron.prototype.paintLinkTerminals = function(){

    /* This draws ALL of the link terminals, nexûs. and sets-up SimplePointers
       between them. */
    
    // Remove old terminals

    var oldTerms = Array.from(document.body.querySelectorAll(".linkTerminalGroup"));
    for(var ix in oldTerms){
	oldTerms[ix].remove();
    }

    // Remove old nexûs
    
    for(var ix in this.simpleNexi){
	try{
	    this.simpleNexi.dot.remove();
	} catch(e){
	}
    }
    simpleNexi = [];

    /* Iterate over all links in the linkStore and try to create link
       terminals for the nodes that are visible in the workspace. */
    
    var linkIDs = Object.keys(this.linkStore);
    for(var ix in linkIDs){
	var link = this.linkStore[linkIDs[ix]];

	// Bodies first...
	
	var bodyNexi = []; 
	for(var iy in link.body.items){
	    var node = link.body.items[iy];
	    var rects = this.getNodeRects(node);
	    var lastTerm = false;
	    for(var iz in rects){
		var term = linkTerminal(
		    rects[iz].rects,
		    rects[iz].floater
		);
		if(!lastTerm){
		    bodyNexi.push(term);
		}
		term.setAttribute("linkid",link.id);
		term.setAttribute("linkpart","body");
		term.setAttribute("nodeix",iy);
		rects[iz].floater.querySelector("svg").appendChild(term);
		if(lastTerm){
		    this.simplePointers.push(
			new SimplePointer(term,lastTerm,this.overlay,"dptron-equivSP")
		    );
		}
		lastTerm = term;
	    }
	}
	var bodyNexus = new SimpleNexus(bodyNexi,this.overlay,this,link,true);
	for(var iy in bodyNexi){
	    this.simplePointers.push(
		new SimplePointer(bodyNexi[iy],bodyNexus.dot,this.overlay)
	    );
	}

	
	// Now targets ...
	
	var targetNexi = [];
	for(var iy in link.target.items){
	    var node = link.target.items[iy];
	    var rects = this.getNodeRects(node);
	    var lastTerm = false;

	    for (var iz in rects){
		var term = linkTerminal(
		    rects[iz].rects,
		    rects[iz].floater
		);
		targetNexi.push(term);
		term.setAttribute("linkid",link.id);
		term.setAttribute("linkpart","target");
		term.setAttribute("nodeix",iy);
		rects[iz].floater.querySelector("svg").appendChild(term);
		if(lastTerm){
		    this.simplePointers.push(
			new SimplePointer(term,lastTerm,this.overlay,"dptron-equivSP")
		    );
		}
		lastTerm = term;
	    }
	}
	var targetNexus = new SimpleNexus(targetNexi,this.overlay,this,link,false);
	for(var iy in targetNexi){
	    this.simplePointers.push(
		new SimplePointer(targetNexi[iy],targetNexus.dot,this.overlay)
	    );
	}

	bodyNexus.coNexus = targetNexus;
	targetNexus.coNexus = bodyNexus;
	
	this.simpleNexi.push(bodyNexus);
	this.simpleNexi.push(targetNexus);

	this.simplePointers.push(
	    new SimplePointer(bodyNexus.dot,targetNexus.dot,this.overlay,"dptron-linkBridge")
	)
    }
}


/***/
function linkTerminal(rects,offset){
    var g = svgGroup();
    if(offset){
	var fRect = offset.getBoundingClientRect();
	var tScale = 1 / parseFloat(offset.getAttribute("data-scale"));
	g.setAttribute("transform",
		       "translate(" + (tScale * (0 - fRect.left)) + "," +
		       (tScale * (0 - fRect.top)) + ")" +
		       " scale(" + tScale + ")"
		      );
    }
    g.setAttribute("class","linkTerminalGroup");
    g.onmouseover = function(evt){
	this.style.fill = "rgba(192,192,192,0.66)";
	console.log(this);
    }.bind(g);
    g.onmouseout = function(evt){
	this.style.fill = "";
    }.bind(g);
    for (var ix in rects){
	var domRect = rects[ix];
	var r = svgRect(domRect.left,
			domRect.top,
			domRect.width,
			domRect.height,
			"linkTerminal"
		       );
	g.appendChild(r);
    }
    return g;
}

function flashRects(layer,rects){
    /* Testing function. 

       rects is an Array of DOMRect objects.
       layer is an <SVG> element
       
       Draw some SVG rectangles onto the passed layer and have 
       those rectangles remove themselves after a moment. 
    */
    
    for (var iz in rects){
	var dr = rects[iz];
	var r = svgRect(dr.left,
			dr.top,
			dr.width,
			dr.height,
			"linkTerminal"
		       );
	layer.appendChild(r);
	window.setTimeout(function(){
	    this.remove();
	}.bind(r), 2000);
    }
}

/***/

Docuplextron.prototype.findLinkTerminals = function(span){
    
    /* Compare the passed content span against the EDLs of all open
       documents, and store matches to .... 
    
       This is a modified version of findMatchingContentFor(). Unify them someday? 

       See Note in buildLinkpointerSpans() */

    if(typeof(span) == "string"){
	
	/* Span links to a document, not a media span. Figure out what
	   to do here, Adam. */
	
	return;
    }
    
    // ??? Why do I use an array here instead of an AlphSpan?
    
    var sourceSpan = [span.src, span.origin, span.extent];
    var matchStrings = [];
    
    // Iterate over the EDLs of all other documents...
    
    for(var targetEdlIndex = 0;
	    targetEdlIndex < this.edlStore.length;
	    targetEdlIndex++)
    {
	var targetEdl = this.edlStore[targetEdlIndex];
	    
	for(var targetIx = 1; targetIx < targetEdl.length; targetIx++){

	    /* Check the note in findMatchingContentFor() for a the bug that warrants this chunk:
	    
	    if(sourceSpan[0] == targetEdl[targetIx][0] &&
	       parseInt(sourceSpan[1]) == parseInt(targetEdl[targetIx][1]) &&
	       parseInt(sourceSpan[2]) == parseInt(targetEdl[targetIx][2])){
		continue;
	    }	
	    */
	    spanMatch = Alphjs.compareSpans(sourceSpan, targetEdl[targetIx]);
	    
	    if(spanMatch){
		var srcString = spanMatch.join("~");
		matchStrings.push(srcString);

		/* Create the transpointerSpans entry if we need to. */
		
		if(!this.transpointerSpans[srcString]){
		    this.transpointerSpans[srcString] = [
			[this.currentContext],
			[targetEdl[0]]
		    ];
		} else {
		    
		    /* If the transpointerSpans entry exists but does
		   not yet contain an array for one of our
		   targets, push it on. */
			
		    if(!this.transpointerSpans[srcString].find(function(s){
			return s[0] == this;
		    },targetEdl[0])){
			this.transpointerSpans[srcString].push(
			    [targetEdl[0]]
			);
		    }
		    if(!this.transpointerSpans[srcString].find(function(s){
			return s[0] == this;
		    },this.currentContext)){
			this.transpointerSpans[srcString].push(
			    [this.currentContext]
			);
		    }
		}
	    }
	}
    }
    return matchStrings;
}

/***/

Docuplextron.prototype.buildLinkBridges = function(){
    for(var ix in this.linkStore){
	drawLink(this.linkStore[ix]);
    }
}

/***/

Docuplextron.prototype.drawLink = function(link){
    
    /* TODO ===
       This nests too deeply. Break it up, Adam. */
    
    var pointString = "";
    var rectStack = [];
    for(var ix in link.nodes){
	var node = link.nodes[ix];
	for(var iy in node.spans){
	    var span = node.spans[iy];
	    for(var iz in span.keys){
		var polyGroup = this.transpointerSpans[span.keys[iz]];
		//console.log(polyGroup);
		if(!polyGroup) continue;
		for(var contextIx = 0; contextIx < polyGroup.length; contextIx++){
		    if(polyGroup[contextIx][1]){
			//pointString += polyGroup[contextIx][1].getClientRects()[0].left + "," + polyGroup[contextIx][1].getClientRects()[0].top + " ";
			rectStack.push(polyGroup[contextIx][1]);
		    }
		}
	    }
	}
    }
    if(rectStack.length > 1){
	for(var ia = 1; ia < rectStack.length; ia++){
	    this.simplePointers.push(new SimplePointer(rectStack[ia-1],
							 rectStack[ia],
							 this.overlay));
	}
    }
}

/*





  OVERLAY LOOP 





 */

Docuplextron.prototype.redraw = function(){
    
    // Update dptron.transpointerSpans.  This doesn't fire if we're
    // still waiting on responses from the last time we called it.
    
    //updateAllRects();

    /* ↑ Seriously fucking sub-optimal. Here's the deal: For as long
       as I'm dealing with the root Window object as a single context
       instead of using the floater divs as contexts and making use of
       their individual svg overlays, this is a wholly undesirable but
       necessary call to be making. */
    
    this.killSvgEphemerons();
    this.buildBridges();
    //buildLinkBridges();

    if(this.dptronMode == "teditor"){
	// Make sure the DOMCursor is disabled.
	if(this.DOMCursor.enabled){
	    this.DOMCursor.toggle();
	}
    } else {
	this.DOMCursor.updatePosition();
    }

    // TESTING AREA =====
    for(var ix in this.simplePointers){
	if (!this.simplePointers[ix].dead){
	    this.simplePointers[ix].update();
	}
    }
    for(var ix in this.simpleNexi){
	if(!this.simpleNexi[ix].dead){
	    this.simpleNexi[ix].update();
	}
    }
    // AERA GNITSET =====

    
    // CURSOR! I can't believe I forgot that caret browsing is not the
    // default for most folks. So, here's a quick SVG cursor hack.

    /*
    if(window.getSelection()){
	var selRect = window.getSelection().getRangeAt(0).getClientRects()[0];
	var cursorPline = svgPolyline(
	    selRect.left + "," + selRect.top + " " + selRect.left + "," + selRect.bottom,
	    "cursorHack");
	cursorPline.classList.add("killme");
	this.overlay.appendChild(cursorPline);
    }
    */
    
    /* Fix the size of the overlay layer if the window has changed dimensions. */
    
    if (this.overlay.getAttribute("width") != window.innerWidth){
	this.overlay.setAttribute("width", window.innerWidth);
    }
    
    if (this.overlay.getAttribute("height") != window.innerHeight){
	this.overlay.setAttribute("height", window.innerHeight);
	this.floaterManager.bottomBoundary = window.innerHeight;
    }

    /* Should this be at the bottom? Or just after the frameskip portion? */
    //requestAnimationFrame(this.overlayLoop);
}

/* 





   TRANSPOINTERS





*/

Docuplextron.prototype.updateContexts = function(){
    
    /* Get an array of all the contexts/documents in the workspace.

       These contexts will be used as the identifying elements in the
       dptron.edlStore arrays. This gives us a list of contexts to
       interrogate about their EDLs.

       NOT BEING USED, H27.1158
    */
    
    if(this.dptronMode){
	this.contexts = this.floaterManager.getMembers();
    } else {
	this.contexts = [ window ];
    }
    
    var iframes = Array.from(document.getElementsByTagName("IFRAME"));
    for(var ix in iframes){
	try{
	    this.contexts.push(iframes[ix].contentWindow);
	} catch(e) {
	    //console.log(e);
	}
    }
}

/***/

Docuplextron.prototype.findMatchingContentFor = function(span){

    /* Compare the passed content span against the EDLs of all open
       documents, and store matches to dptron.transpointerSpans. */
    
    // ??? Why do I use an array here instead of an object?
    var sourceSpan = [span.src, span.origin, span.extent];
    
    // Iterate over the EDLs of all other documents
    
    for(var targetEdlIndex = 0;
	    targetEdlIndex < this.edlStore.length;
	    targetEdlIndex++)
    {
	var targetEdl = this.edlStore[targetEdlIndex];
	    
	/* Slightly different than the other findMatching...()
	   functions: no spanOffset */
	
	for(var targetIx = 1; targetIx < targetEdl.length; targetIx++){

	    /* HOWEVER, there is a problem, here. We can't have a span
	    matching against itself, otherwise we get screwed-up
	    transpointers, but how do we know which precise span we're
	    comparing against? We don't.

	    Here's a dumb fix, but it's dumb. The side-effect is that
	    separate instances of the same span in the parent context
	    workspace will not be transpointed.

	    One better fix will be to support contexts in edlStore and
	    dptron.currentContext and elsewhere as floater DIV
	    references in addition to Window/IFRAME objects (which has
	    always been part of the plan, but is not yet implemented).

	    Best approach is to pass a live X-TEXT to this function
	    instead of an AlphSpan. */
	    
	    if(sourceSpan[0] == targetEdl[targetIx][0] &&
	       parseInt(sourceSpan[1]) == parseInt(targetEdl[targetIx][1]) &&
	       parseInt(sourceSpan[2]) == parseInt(targetEdl[targetIx][2])){
		continue;
	    }	
	    spanMatch = Alphjs.compareSpans(sourceSpan, targetEdl[targetIx]);

	    
	    if(spanMatch){
		this.addTranspointerSpan(
		    spanMatch.join("~"),
		    this.currentContext,
		    targetEdl[0]);
	    }
	}
    }
    this.updateAllRects();
}

/***/

Docuplextron.prototype.findSomeMatchingContent = function(context,edlIndex){
    if(!edlIndex){
	var sourceEdl = this.edlStore.find(function(c){
	    return c[0] == this;
	},context);
    } else {
	var sourceEdl = this.edlStore[edlIndex];
    }
    
    if(!sourceEdl){
	console.log("No edl for ", context);
	//debugger;
	return;
    }

    // Iterate over each media span (notice our index starts a 1, not 0)
    for(var spanIx = 1; spanIx < sourceEdl.length; spanIx++){
	// Then iterate over the EDLs of all other documents
	for(var targetEdlIndex = 0;
	    targetEdlIndex < this.edlStore.length;
	    targetEdlIndex++)
	{
	    var targetEdl = this.edlStore[targetEdlIndex];
	    
	    // If we're searching for matching spans within the same
	    // document, we start this loop with an offset
	    var spanOffset = (targetEdl == sourceEdl) ? spanIx : 0;
		
	    for(var targetIx = 1 + spanOffset;
		targetIx < targetEdl.length;
		targetIx++){

		spanMatch = Alphjs.compareSpans(sourceEdl[spanIx],
					 targetEdl[targetIx]);
		   
		if(spanMatch){
		    this.addTranspointerSpan(
			spanMatch.join("~"),
			targetEdl[0],
			sourceEdl[0]);
		}
	    }
	}
    }
    //this.rebuildTranspointers();
    //this.updateRects(context);
    this.updateAllRects();
}

/***/

Docuplextron.prototype.findAllMatchingContent = function(){
    
    /* Compare ALL CONTENT SPANS in the EDLs of all open documents and
       look for matching content. Store matches to dptron.transpointerSpans. */

    if(!this.allMessagesBack()) return;
    
    // *** This nests too deeply and should be broken into a few functions
        
    // Iterate over each EDL in the edlStore
    for(var sourceEdlIndex = 0; sourceEdlIndex < this.edlStore.length; sourceEdlIndex++){
	var sourceEdl = this.edlStore[sourceEdlIndex];
	/*
	  Entries in the EDL store look like this:

	  [ (context),
	    ["http://server/path/resource","origin","offset"],
	    ["http://server/path/resource","origin","offset"],
	    ...
	  ]

	  So there's a reference to the window/iframe/div which contains
	  the document at the top, then a series of content spans in the
	  form of 3-element arrays.
	*/

	// Iterate over each media span (notice our index starts a 1, not 0)
	for(var spanIx = 1; spanIx < sourceEdl.length; spanIx++){

	    // We want to compare this media span with all of the media spans
	    // in this and all remaining documents, so ...
	    for(var targetEdlIndex = sourceEdlIndex;
		targetEdlIndex < this.edlStore.length;
		targetEdlIndex++)
	    {
		var targetEdl = this.edlStore[targetEdlIndex];

	  	// If we're searching for matching spans within the same
		// document...
		var spanOffset = (targetEdlIndex == sourceEdlIndex) ? spanIx : 0;
		
		for(var targetIx = 1 + spanOffset;
		    targetIx < targetEdl.length;
		    targetIx++){

		    spanMatch = Alphjs.compareSpans(sourceEdl[spanIx],
					     targetEdl[targetIx]);
		    
		    // compareSpans returns a simple src,origin,extent array
		    
		    // dptron.transpointerSpans is an object which uses
		    // content span strings as property keys, and the
		    // value for each property is an array of arrays,
		    // the first item in each being a reference to the
		    // window which contains the matching content, the
		    // subsequent items being SVGPolygon elements. The
		    // "points" attribute of the polygon will contain
		    // an outline of the matching content in the
		    // window.
		    
		    if(spanMatch){
			this.addTranspointerSpan(
			    spanMatch.join("~"),
			    targetEdl[0],
			    sourceEdl[0]);
			
		    }
		}
	    }
	}
    }

    // When we're all done, get SVG polygons for all the matching
    // content.
    this.updateAllRects();
}

/***/

Docuplextron.prototype.addTranspointerSpan = function(index, target, source){

    // Create the transpointerSpans entry for this span if we need to.
    if(!this.transpointerSpans[index]){
	this.transpointerSpans[index] = [
	    [target], // [ target, SVGRect, SVGRect, ...]
	    [source]
	];
    } else {
	
	// If we have an entry for this span, but NOT for one of the
	// contexts in this match, push one on.
	
	if(!this.transpointerSpans[index].find(function(s){
	    return s[0] == this;
	},target)){
	    this.transpointerSpans[index].push(
		[target]
	    );
	}
	if(!this.transpointerSpans[index].find(function(s){
	    return s[0] == this;
	},source)){
	    this.transpointerSpans[index].push(
		[source]
	    );
	}
    }
}

/***/

Docuplextron.prototype.allMessagesBack = function(){
   if(this.sentMsgCount > 0){
	return false;
    } else if ( this.sentMsgCount < 0) {

	// This shouldn't be happening, but it does sometimes,
	// so... that needs to be sorted out.

	this.sentMsgCount = 0;
	return false;
    } else {
	return true;
    }
}

/***/

Docuplextron.prototype.updateAllRects = function(){
    
    /* Populate dptron.transpointerSpans by sending "requestRects"
       messages to IFRAMEs and by calling getMatchingContentRects() on
       top-level contexts. */

    if(!this.allMessagesBack()) return;
 
    var spanKeys = Object.keys(this.transpointerSpans);
    for(var ix in spanKeys){
	var spanObj = spanKeys[ix].split("~");
	var rectSet = this.transpointerSpans[spanKeys[ix]];
	for(var iy in rectSet){
	    var msgTarget = rectSet[iy][0];
	    var id = false;
	    if(typeof(msgTarget) == "string"){
		id = msgTarget;
		msgTarget = window;
	    } else if (msgTarget != window){
		if(msgTarget.contentWindow){
		    msgTarget = msgTarget.contentWindow;
		} else {
		    //console.log("Bad entry " + iy + " in transpointerSpans?",rectSet[iy]);
		    rectSet.splice(iy,1);
		    iy--;
		    continue;
		}
	    }
	    msgTarget.postMessage({"op":"requestRects",
				   "type":"X-TEXT",
				   "src":spanObj[0],
				   "origin":spanObj[1],
				   "extent":spanObj[2],
				   "id":id},
				  "*");
	    this.sentMsgCount++;	    
	}		
    }
}

/***/

Docuplextron.prototype.updateRects = function(target){
    
    /* Request span highlights for all media spans in a particular context.
       'target' will either be a Window reference, or an id string */

    if(!this.allMessagesBack()) return;

    var spanKeys = Object.keys(this.transpointerSpans);
    for(var ix in spanKeys){
	var spanObj = spanKeys[ix].split("~");
	var rectSet = this.transpointerSpans[spanKeys[ix]];
	for(var iy in rectSet){
	    var msgTarget = rectSet[iy][0];
	    var id = false;
	    if (typeof(msgTarget) == "string"){
		if(msgTarget != target){
		    continue;
		}
		id = msgTarget;
		msgTarget = window;
	    } else if (msgTarget != window){
		if(msgTarget.contentWindow){
		    msgTarget = msgTarget.contentWindow;
		} else {
		    //console.log("Bad entry " + iy + " in transpointerSpans?",rectSet[iy]);
		    rectSet.splice(iy,1);
		    iy--;
		    continue;
		}
		if(msgTarget != target){
		    continue;
		}
	    }
	    msgTarget.postMessage({"op":"requestRects",
				   "type":"X-TEXT",
				   "src":spanObj[0],
				   "origin":spanObj[1],
				   "extent":spanObj[2],
				   "id":id},
				  "*");
	    this.sentMsgCount++;
	}
    }		
}

/***/

Docuplextron.prototype.receiveRects = function(msg,source){
    
    /* Handle some rects that we requested.

       [G9A.1009 - Not actually rects, but SVG polygon point strings.]
       msg["matches"] will contain the point strings for each matching
       content area. */
    
    var issuingFrame = (msg["id"]) ? msg["id"] : Docuplextron.frameOf(source);
    this.sentMsgCount--;
    
    if(msg["matches"].length > 0){
	var matches = msg["matches"];
	var matchingSpanArray = this.transpointerSpans[msg["span"]];

	// Find the array containing a reference to the window that sent this
	// message in the dptron.transpointerSpans object.
	var targetArray;
	for(var ix in matchingSpanArray){
	    if(matchingSpanArray[ix][0] == issuingFrame){
		targetArray = matchingSpanArray[ix];
	    }
	}
	    
	if(!targetArray){
	    // dptron.currentContext or dptron.currentSpan has changed since the
	    // message was sent. We don't need these rects anymore.
	    return;
	}
	    
	var lastPolyIx = 0;
	for(var ix = 1; ix <= matches.length; ix++){
	    // Set the "points" attributes of the SVGPolyons in the array to the
	    // strings in matches, or create the SVGPolygon if it doesn't exist.
	    if(targetArray[ix]){
		targetArray[ix].setAttribute("points", matches[ix-1]);
	    } else {
		targetArray[ix] = svgPolygon(matches[ix-1],"coincidence");
		if(issuingFrame == window){
		    // Append SVG to global overlay
		    this.overlay.appendChild(targetArray[ix]);
		} else if( typeof(issuingFrame) == "string"){
		    var floater = document.getElementById(issuingFrame);
		    if(floater){
			floater.overlay.appendChild(targetArray[ix]);
		    }
		} else {
		    // Append SVG to floater's local overlay
		    Docuplextron.getParentItem(issuingFrame).overlay.appendChild(targetArray[ix]);		
		}
	    }
	    lastPolyIx = ix;
	}
	// In case the number of matches has been reduced...
	if(targetArray.length > (1+lastPolyIx)){
	    targetArray = targetArray.slice(0,lastPolyIx);
	}
    }
}

/***/

Docuplextron.prototype.killSvgEphemerons = function(){
    var deletableSVG = Array.from(this.overlay.querySelectorAll(".killme"));    
    for(var ix in deletableSVG){
	deletableSVG[ix].remove();
    }
}

/***/


Docuplextron.prototype.buildBridges = function(){
    
    /* Draw all of the SVG rectangles linking the segments in transpointerSpans */

    /* TODO
       Let's make this more efficient, so that it re-uses existing SVG polys
       in the way that DOMCursor.updatePosition() re-uses divs.
    */
    
    //var dsvgix = 0;
    var keys = Object.keys(this.transpointerSpans);
    for(var ix in keys){
	var contentSpan = this.transpointerSpans[keys[ix]];
	
	/* contentSpan refers to an array of arrays containing SVG
	   polygons that enclose matching content spans in different
	   contexts (windows/documents).  The sub-arrays look like
	   this: [ window_ref, SVGPolygon, SVGpolygon... ] */

	var polygons = [];
	for(var contextIx = 0; contextIx < contentSpan.length; contextIx++){
	    var context = contentSpan[contextIx];
	    for(var polyIx = 1; polyIx < context.length; polyIx++){
		polygons.push(context[polyIx]);
	    }
	}
	// Now we have all of the SVG Polygons of this content portion in an array.
	// Let's draw a line between them.
	for(var polyIx = 1; polyIx < polygons.length; polyIx++){
	    if(polygons[polyIx-1].getBoundingClientRect().left <
	       polygons[polyIx].getBoundingClientRect().left){
		var lBox = polygons[polyIx-1].getBoundingClientRect();
		var rBox = polygons[polyIx].getBoundingClientRect();
	    } else {
		var rBox = polygons[polyIx-1].getBoundingClientRect();
		var lBox = polygons[polyIx].getBoundingClientRect();
	    }
	    var lPoly = svgPolygon("" +
				   lBox.right + "," + lBox.top + " " +
				   rBox.left  + "," + rBox.top  + " " +
				   rBox.left  + "," + rBox.bottom  + " " +
				   lBox.right  + "," + lBox.bottom, 'coincidence');
	    lPoly.classList.add("killme");
	    this.overlay.appendChild(lPoly);
	}
    }
}

Docuplextron.prototype.rebuildTranspointers = function(){
    
    /* This is a brute-force approach, and needs to be improved. I
       really need a scheme for maintenance of the transpointerSpans
       object; findMatchingContent() updates and adds spans, but
       there's nothing going in and removing invalid ones. */
    
    this.transpointerSpans = {};
    
    //var svgElements = Array.from(document.getElementsByTagName("polygon"));
    var svgElements = Array.from(document.querySelectorAll("polygon.coincidence"));
    
    for(var ix in svgElements){

	svgElements[ix].remove();
    }
    
    this.sentMsgCount = 0;

    if(document.getElementById("vlinksEnable").checked){
	this.buildLinkpointerSpans();
    }
    
    this.findMatchingContent();
}

Docuplextron.prototype.findMatchingContent = function(){
    
    // Dispatcher for the various find*Matching*() functions.
    
    if(this.transpointerMode == 0){
	this.findAllMatchingContent();
    } else if(this.transpointerMode == 1){
	this.findSomeMatchingContent(this.currentContext);
    } else if(this.transpointerMode == 2){
	this.findMatchingContentFor(this.currentSpan);
    } else {
	// Do nothing if transpointerMode is 3
    }
}

Docuplextron.prototype.getMatchingContentRects = function(type,src,origin,extent,offsetSrc){

    /* Called upon receipt of the "requestRects" message.
    
       We're passed a content span (src/origin/extent)
       We search for matching content in our document
       We return matching spans with selection rectangles for text/images
       For audio/video...??? */

    var xoff = 0;
    var yoff = 0;
    var contextRect;
    var context = document;
    if(offsetSrc){
	try{
	    context = document.getElementById(offsetSrc);
	    var contextRect = context.getBoundingClientRect();
	} catch(e){
	    // id passed as offsetSrc refers to a dead element. 
	    this.rebuildEdlStore();
	    return [];
	}
	xoff = contextRect.left;
	yoff = contextRect.top;	
    }
    
    switch(type){
    case "X-TEXT":
	//var spans = Array.from(document.querySelectorAll("x-text[src='"+src+"']"));
	var spans = Array.from(context.querySelectorAll("x-text[src='"+src+"']"));
	if(!spans) return;
	var targetRange = document.createRange();
	var matches = [];
	for (var ix in spans){
	    // Sort 'em
	    if (parseInt(spans[ix].getAttribute("origin")) < origin ){
		if(parseInt(spans[ix].getAttribute("extent")) < origin ||
		   parseInt(spans[ix].getAttribute("origin")) > extent ) continue;
		var leftOrigin = parseInt(spans[ix].getAttribute("origin"));
		var leftExtent = parseInt(spans[ix].getAttribute("extent"));
		var rightOrigin = origin;
		var rightExtent = extent;
		var targetSide = "l";
	    } else {
		var targetSide = "r";
		var rightOrigin = parseInt(spans[ix].getAttribute("origin"));
		var rightExtent = parseInt(spans[ix].getAttribute("extent"));
		var leftOrigin = origin;
		var leftExtent = extent;
	    }

	    // Look for matches
	    if(leftExtent <= rightOrigin) {
		// Left span lies ouside of right span
		continue;
	    } else if (leftExtent < rightExtent){
		// Left span partially overlaps right span
		match = true;		
		var matchLength = leftExtent - rightOrigin;
		var spanRangeIndex = rightOrigin - leftOrigin;
		var matchOrigin = origin;
		var textNode = spans[ix].childNodes[0];
		if(targetSide == "l"){
		    targetRange.setStart(textNode, spanRangeIndex);
		    targetRange.setEnd(textNode, spanRangeIndex + matchLength);
		    matchOrigin += spanRangeIndex;
		} else {
		    targetRange.setStart(textNode, 0);
		    try{
			targetRange.setEnd(textNode, matchLength);
		    } catch(e) {
			console.log("bad index: ", matchLength);
		    }
		}
		var matchExtent = matchOrigin + matchLength;
	    } else if(leftExtent >= rightExtent){
		// Left span encloses right span
		match = true;
		var matchLength = rightExtent - rightOrigin;
		var spanRangeIndex = rightOrigin - leftOrigin;
		var matchOrigin = origin;
		var textNode = spans[ix].childNodes[0];
		if(targetSide == "l"){
		    targetRange.setStart(textNode, spanRangeIndex);
		    targetRange.setEnd(textNode,spanRangeIndex + matchLength);
		    matchOrigin += spanRangeIndex;
		} else {
		    try{
			targetRange.setStart(textNode, 0);
			targetRange.setEnd(textNode, matchLength);
		    } catch(e) {
			// Usually, if there's a mismatch in here,
			// it's because whitespace has been altered by
			// the browser. Really, we should call
			// alph.fillSpan() or something to make sure
			// that text span contents are true-to-source.
			
			continue;
		    }
		}
		var matchExtent = matchOrigin + matchLength;
	    } else {
		// This shouldn't happen.
		//console.log("Span matching weirdness in getMatchingContentRects()");
	    }

	    if(matchOrigin){
		// Make a polygon point string and push it onto matches.
		var divisor = (context.nodeType == Node.ELEMENT_NODE) ?
		    parseFloat(context.getAttribute("data-scale")) : 1;
		var rects = Array.from(targetRange.getClientRects());
		var pointStringL = "";
		var pointStringR = "";
		for(var rectIx = 0; rectIx < rects.length; rectIx++){
		    var l = rects[rectIx]; // Down the left side...
		    var r = rects[(rects.length - 1) - rectIx]; // ...and up the right.
		    pointStringL += (l.left - xoff)/divisor + "," + (l.top - yoff)/divisor + " " +
			(l.left - xoff)/divisor + "," + (l.bottom - yoff)/divisor + " ";
		    pointStringR += (r.right - xoff)/divisor + "," + (r.bottom - yoff)/divisor + " " +
			(r.right - xoff)/divisor + "," + (r.top - yoff)/divisor + " ";
		}
		matches.push(pointStringL + pointStringR);
	    }
	}
	return matches;
	break;
    case "IMG":
	break;
    case "AUDIO":
	break;
    case "VIDEO":
	break;
    }
}

/*





   EDL MANAGEMENT __________________________________________________





*/

Docuplextron.prototype.postEDL = function(context){
    
    /* Generate an EDL from all of the content spans in the passed
       context, and post it up to the parent context. A "context" here
       may be any DOM node; this will usually be either the entire document, 
       or a div.alph-floater/div.alph-slider

       Any function that manupulates the document should call
       postEDL() once the damage is done. postEDL() sends the
       "storeEDL" message, which triggers the storeEDL() function in
       the parent context, which calls findMatchingContent() which
       maintains dptron.transpointerSpans.*/
    
    var context = context || document;
    
    if(context == document && parent == window && this.dptronMode){
	// Don't post the full-document EDL in this case; instead,
	// post EDLs for each floating context
	var floaterContexts = this.floaterManager.getMembers();
	for (var ix in floaterContexts){
	    var narrowContext = floaterContexts[ix];
	    if(!narrowContext.querySelector("iframe")){
		this.postEDL(narrowContext);		
	    }
	}
    } else {
    
	var edl = [];
	var spans = Array.from(context.querySelectorAll("[src]"));
	for (var ix in spans){
	    var span = spans[ix];
	    // Just doing text right now. Images and A/V later.
	    if(spans[ix].tagName == "X-TEXT"){
		edl.push([span.getAttribute("src"),
			  span.getAttribute("origin"),
			  span.getAttribute("extent")]);
	    }
	}
	if(context.id == ""){
	    
	    /* The element needs an 'id' attribute so that we can
	       reference it in our edlStore. This will be ignored if
	       we're posting up from an IFRAME, but if this is for a
	       floater in the parent context, it's essential. */
	    
	    context.id = "context_" + Date.now();
	}
	parent.postMessage({"op":"storeEDL",
			    "edl":edl,
			    "id":context.id},"*");
    }
}

/***/

Docuplextron.prototype.storeEDL = function(w,edl,id){
    // 'w' is a the source window of a "storeEDL" message. It might be
    // the contentWindow of an iframe. If it is, get a handle on that
    // iframe instead.
    var contextTag;
    
    if(w != window){
	try{
	    var iframe = Docuplextron.frameOf(w);
	} catch(e){
	    this.findMatchingContent();
	    return;
	}
	contextTag = iframe;
    } else {
	// If this *is* coming from the top-level window, we want to
	// use the passed 'id' as the first edl element if one was passed.
	//
	if(id){
	    contextTag = id;
	} else {
	    contextTag = w;
	}
    }
    // Then put the window/iframe at the front of the array.
    edl.unshift(contextTag);
    
    // Store it to dptron.edlStore. If one is there already, replace it.
    var stored = false;
    for(var ix in this.edlStore){
	if(this.edlStore[ix][0] == contextTag){
	    this.edlStore[ix] = edl;
	    stored = true;
	}
    }
    if(!stored){
	this.edlStore.push(edl);
    }
    
    this.rebuildTranspointers();
}

/***/

Docuplextron.prototype.removeEDL = function(target){
    // 'target' is an iframe
    this.edlStore = this.edlStore.filter( function (el){
	el[0] == this;
    },target);
    
    this.rebuildTranspointers();
}

/***/

Docuplextron.prototype.rebuildEdlStore = function(){

    this.edlStore = [];
    this.postEDL();
    
    // Docuplextron.broadcast only posts messages to IFRAMEs, so this won't recurse
    Docuplextron.broadcast({"op":"postEDL"});
}

Docuplextron.colorspaceCanvas = function(){
    var canvas = document.createElement("canvas");
    canvas.width = 256;
    canvas.height = 256;
    var context = canvas.getContext("2d");
    var grad1 = context.createLinearGradient(0,0,0,256);
    var grad2 = context.createLinearGradient(0,0,256,0);
    grad1.addColorStop(0,"rgb(0,0,0)");
    grad1.addColorStop(1,"rgb(255,255,255)");
    
    grad2.addColorStop(0,"rgb(255,0,0)");
    grad2.addColorStop(0.333,"rgb(0,255,0)");
    grad2.addColorStop(0.666,"rgb(0,0,255)");
    grad2.addColorStop(1,"rgb(255,0,0)");
    context.fillStyle = grad1;
    context.fillRect(0,0,256,256);
    context.globalCompositeOperation = "multiply";
    context.fillStyle = grad2;
    context.fillRect(0,0,256,256);
    return canvas;
}

Docuplextron.colorSwatches = function(backgrounds,foregrounds,caption){

    /* I return a table of color swatches, each of which call my
       action() method on mouseup, passing the background/foreground
       color in my swatch. To use me, call with arrays of background
       and foreground colors and a caption, then define my action()
       method to do whatever you want. */
    
    var tab = document.createElement("table");
    tab.className = "dptronColorPicker";

    tab.cap = document.createElement("caption");
    tab.appendChild(tab.cap);
    tab.cap.textContent = caption || "";
    
    var row = document.createElement("tr");
    tab.appendChild(row);

    /* Default to 16 pastel shades if no colors are passed. */
    
    var swatchBGs = backgrounds || ["rgb(255,114,126)","rgb(255,146,110)",
				    "rgb(227,215,138)","rgb(199,255,150)",
				    "rgb(154,179,255)","rgb(130,150,255)",
				    "rgb(183,158,255)","rgb(207,203,179)",
				    "rgb(196,55,67)","rgb(220,111,75)",
				    "rgb(152,140,63)","rgb(124,180,75)",
				    "rgb(80,105,181)","rgb(80,100,205)",
				    "rgb(108,83,180)","rgb(124,120,96)"];
    
    var swatchFGs = foregrounds || ["black","black",
				    "black","black",
				    "black","black",
				    "black","black",
				    "black","black",
				    "black","black",
				    "black","black",
				    "black","black"];
    
    tab.action = function(color1,color2){
	/* Define this method when you use me! */
    };
    
    for(var ix in swatchBGs){
	var swatch = document.createElement("td");
	swatch.style.backgroundColor = swatchBGs[ix];
	swatch.style.color = swatchFGs[ix];
	swatch.color1 = swatchBGs[ix];
	swatch.color2 = swatchFGs[ix];
	swatch.textContent = ix;
	row.appendChild(swatch);
	swatch.onmouseup = function(swatches){
	    swatches.action(this.color1,this.color2);
	}.bind(swatch,tab);
    }
    return tab;
}

/*





   SVG _____________________________________________________





*/

var svgns = "http://www.w3.org/2000/svg";

function svgGroup(){
    var g = document.createElementNS(svgns,"g");
    return g;
}

function svgLine(x1,y1,x2,y2,c){
    var l = document.createElementNS(svgns,"line");
    l.setAttribute("x1", x1);
    l.setAttribute("y1", y1);
    l.setAttribute("x2", x2);
    l.setAttribute("y2", y2);
    l.setAttribute("class", c || "");
    return l;
}
function svgRect(x,y,w,h,c){
    var r = document.createElementNS(svgns,"rect");
    r.setAttribute("x", x);
    r.setAttribute("y", y);
    r.setAttribute("width", w);
    r.setAttribute("height", h);
    r.setAttribute("class", c || "");
    return r;
}
function svgPolyline(p,c){
    var r = document.createElementNS(svgns,"polyline");
    //r.setAttribute("stroke","black");
    //r.setAttribute("stroke-width","6");
    r.setAttribute("points", p);
    r.setAttribute("class", c || "");
    return r;
}
function svgPolygon(p,c){
    var r = document.createElementNS(svgns,"polygon");
    r.setAttribute("points", p);
    r.setAttribute("class", c || "");
    return r;
}
function svgText(string,x,y){
    var t = document.createElementNS(svgns,"text");
    t.textContent = string;
    t.setAttribute("x",x);
    t.setAttribute("y",y);
    return t;
}


/* 
   RGB <-> HSV converters, from:
   https://stackoverflow.com/questions/17242144/javascript-convert-hsb-hsv-color-to-rgb-accurately

*/

/* accepts parameters
 * h  Object = {h:x, s:y, v:z}
 * OR 
 * h, s, v
 */

function HSVtoRGB(h, s, v) {
    var r, g, b, i, f, p, q, t;
    if (arguments.length === 1) {
        s = h.s, v = h.v, h = h.h;
    }
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
    return {
        r: Math.round(r * 255),
        g: Math.round(g * 255),
        b: Math.round(b * 255)
    };
}

function RGBtoHSV(r, g, b) {
    if (arguments.length === 1) {
        g = r.g, b = r.b, r = r.r;
    }
    var max = Math.max(r, g, b), min = Math.min(r, g, b),
        d = max - min,
        h,
        s = (max === 0 ? 0 : d / max),
        v = max / 255;

    switch (max) {
        case min: h = 0; break;
        case r: h = (g - b) + d * (g < b ? 6: 0); h /= 6 * d; break;
        case g: h = (b - r) + d * 2; h /= 6 * d; break;
        case b: h = (r - g) + d * 4; h /= 6 * d; break;
    }

    return {
        h: h,
        s: s,
        v: v
    };
}

function YMDMins(){
    var d = new Date();
    var y = (d.getFullYear() - 2000).toString(36);
    var m = d.getMonth().toString(36);
    var day = d.getDate().toString(36);
    var mins = (d.getHours() * 60) + d.getMinutes();
    return y + m + day + "." + mins.toString();
}
