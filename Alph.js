/*

  ALPH.JS
  The Alph Project <http://alph.io/>
  ©2016,2017 Adam C. Moore (LÆMEUR) <adam@laemeur.com>

  Client tools for xanalogical hypertext. 
  
  This is free software and is released under the terms of the current
  GNU General Public License <https://www.gnu.org/copyleft/gpl.html>

  Source code and revision history are available on GitLab:
  <https://gitlab.com/alph-project/alph.js>

*/

function Alphjs(){
    this.lang = "en-US";
    this.sources = {}; // Metadata store for source media
    this.lamian = null;
    this.selection = new AlphSelection();
    this.box = new AlphBox();
}

Alphjs.TRANSCLUDE_QUERY = "?fragment=";
Alphjs.DESCRIBE_QUERY = "?describe";
Alphjs.LINK_QUERY = "?link";
Alphjs.logoURL = (typeof(chrome) == "undefined" ||
		  typeof(chrome.extension) == "undefined") ?
    "icons/svg/alph.svg" : chrome.extension.getURL("icons/svg/alph.svg");

Alphjs.domToEdl = function(el){
    var edlString = "";
    var xtexts = Array.from(el.getElementsByTagName("X-TEXT"));
    for(var ix in xtexts){	    
	var fragURL = xtexts[ix].getAttribute("src") + ",start=" +
	    xtexts[ix].getAttribute("origin") + ",length=" +
	    (parseInt(xtexts[ix].getAttribute("extent")) -
	     parseInt(xtexts[ix].getAttribute("origin"))) + "\n";
	edlString += 'span: ' + fragURL;

	// DO LINKS HERE.
    }
    return edlString;
}


Alphjs.restoreCaretPosition = function(element,offset){
    var r = document.createRange();
    r.setStart(element.childNodes[0],offset);
    r.setEnd(element.childNodes[0],offset);
    window.getSelection().removeAllRanges();
    window.getSelection().addRange(r);
}

AlphSpan.prototype.intersection = function(span){
    /* Find intersection of two AlphSpans with numeric fragment specifiers. */

    /* This needs to be upgraded to support n-dimensional coordinate pairs */
    
    try{
	this.origin = parseFloat(this.origin);
	this.extent = parseFloat(this.extent);
	span.origin = parseFloat(span.origin);
	span.extent = parseFloat(span.extent);
    } catch(e){
	// Fail if our origin/extent are non-numeric.
	return false;
    }
    
    // Sort 'em.
    
    if(span.origin < this.origin){
	// Out-of-bounders, here.
	if(span.extent < this.origin || span.origin > this.extent){
	    return false;
	} else {
	    var leftOrigin = span.origin;
	    var leftExtent = span.extent;
	    var rightOrigin = this.origin;
	    var rightExtent = this.extent;
	    var targetSide = "l";
	}
    } else {
	var targetSide = "r";
	var leftOrigin = this.origin;
	var leftExtent = this.extent;
	var rightOrigin = span.origin;
	var rightExtent = span.extent;
    }

    if(leftExtent <= rightOrigin){
	// Left span lies outside of right span
	return false;
    } else if(leftExtent < rightExtent){
	// Left span partially overlaps right span
	return new AlphSpan(this.src,rightOrigin,leftExtent);
    } else if(leftExtent >= rightExtent){
	// Left span encloses right span
	return new AlphSpan(this.src,rightOrigin,rightExtent);
    } else {
	// This shouldn't happen.
	return false;
    }
    
}

Alphjs.splitHref = function(url,container){

    /* Pass me a URL in the form of:

            http://some.server/doc?fragment=x-y`z-a`...

       and a container element, and I will append <x-text>s to the
       container */

    var el = container;
    var xref = url;
    var xsrc = xref.split("?")[0];
        
    // Get the transclusion spec string
    var qstring = xref.split(Alphjs.TRANSCLUDE_QUERY)[1];

    // Discard any additional query parameters
    qstring = qstring.split("&")[0];

    var spans = qstring.split("`");
    for(var iy = 0; iy < spans.length; iy++){
	var span = spans[iy].split("-");
	var origin = Math.min(parseInt(span[0]),parseInt(span[1]));
	var extent = Math.max(parseInt(span[0]),parseInt(span[1]));
	var newspan = Alphjs.newSpan(xsrc,origin,extent);
	el.appendChild(newspan);
    }
}

Alphjs.newSpan = function(src,origin,extent,editable){
    
    /* Return a new X-TEXT element. */
    
    var span = document.createElement("x-text");
    
    // Do I need this(↓) now that I'm doing noodles/Lamian?
    /*
    try{
    if(src.startsWith("~")){
	span.id = src;
    }} catch(e){ debugger; }
    */
    span.setAttribute("src",src);
    span.setAttribute("origin",origin);
    span.setAttribute("extent",extent);
    if(editable){
	span.contentEditable = editable;
    }
    return span;
}

Alphjs.insertNewline = function(evt){

    /* contentEditable implementations have an irritating
       feature: carriage-returns are inserted as <br>
       elements, not newlines. We're doing plain-text stuff
       here, and we don't want any god-damned <br> elements,
       so we have to change that behaviour. 

       This whole mess is further complicated by differences
       in the way that Chrome and Firefox handle newlines 
       in contentEditable elements.*/
	    
    var sel = window.getSelection();
    if(sel.anchorNode.parentElement.tagName == "X-TEXT" &&
       sel.anchorNode.parentElement.getAttribute("contenteditable")){
	var pe = sel.anchorNode.parentElement;
	evt.preventDefault();
	var r = sel.getRangeAt(0);
	var oldOffset = r.startOffset;
	var newText = r.startContainer.textContent;

	if(evt.shiftKey){
	    newText = newText.substring(0,oldOffset) +
		"\n\r" + newText.substring(oldOffset);
	} else {
	    newText = newText.substring(0,oldOffset) +
		"\n" + newText.substring(oldOffset);
	}
	
	r.startContainer.textContent = newText;
	r.setStart(r.startContainer,oldOffset+1);
	if(sel.getRangeAt(0) != r){
	    sel.removeAllRanges();
	    sel.addRange(r);
	}
    }
}

Alphjs.getXPath = function(el,rootNode){

    // Return an XPath for the passed element.

    rootNode = rootNode || document;
    var path = ""
    while(el.parentElement && (el.parentElement != rootNode)){
	/* See if the element has an ID. If it does, double-check
	   and make sure it doesn't have a space in it. Most browsers
	   won't fuss about it, but an XPath parser will.
	*/
	if(el.id &&
	   !el.id.includes(" ")){
	    path = 'id("' + el.id + '")' + path;
	    return path;
	} else {
	    var myIx = 0;
	    var matchingTags = 0;
	    var prefix = "/" + el.tagName.toLowerCase();
	    var sibs = Array.from(el.parentElement.children);
	    for(var ix in sibs){
		if(sibs[ix].tagName == el.tagName){
		    matchingTags++;
		}
		if (sibs[ix] == el){
		    myIx = matchingTags;
		}
	    }
	    if(matchingTags > 1){
		prefix += "[" + myIx + "]";
	    }
	    path = prefix + path;
	}
	el = el.parentElement;
    }
    path = "/" + path;
    return path;
}

Alphjs.resolvePoint = function(xptr,rootNode){
    
    /* Return an array containing the node and offset specified by the
       passed point() XPointer, or return false. */
    
    var node;    
    var rootNode = rootNode || document.documentElement;
    
    // Strip the 'point(' and ')' if present.
    if(xptr.startsWith("point(")) xptr = xptr.slice(6,-1);

    xptr = xptr.split(".");
    var offset = xptr[1];
    var sequence = xptr[0].split("/");

    /* The child sequence will just be an array of integer strings, 
       BUT, it may begin with a barename pointer, so test for that. */

    var child = sequence.shift();
    if(!child) return false; // Empty pointer!
    
    if(parseInt(child) != child){
	// Trim quotation marks if present
	if(child.startsWith("'") ||
	   child.startsWith('"')){
	    child = child.slice(1,-1);
	}
	// Find the named node under the rootNode
	node = rootNode.querySelector("#" + child);
	if(!node) return false;
    } else {
	// In XPointer child sequences, nodes are indexed from 1. So...
	node = rootNode.childNodes[parseInt(child) - 1];
    }

    while(child = sequence.shift()){
	    node = node.childNodes[parseInt(child) - 1];
    }
    
    return [node, offset];
}

Alphjs.getElementPointer = function(node,rootNode){
    // Return an element() scheme XPointer for the passed node.

    rootNode = rootNode || document.documentElement;
    var path = ""
    
    while(node.parentElement){
	if(node.id && (node.id != "")){
	    path = 'element("' + node.id + '"' + path + ")";
	    return path;
	} else {
	    var sibs = Array.from(node.parentElement.childNodes);
	    for(var ix in sibs){
		if(sibs[ix] == node){
		    var sibix = parseInt(ix) + 1;
		    /* Nodes are indexed from 1 in XPointer/XPath */
		    path = "/" + sibix  + path;
		    break;
		}
	    }
	}
	
	if(node.parentElement != rootNode){
	    node = node.parentElement;
	} else {
	    break;
	}
    }

    /* Does our terminal node have a "resource" attribute? If so, 
       we might have been climbing up a document fragment. See if 
       the "resource" attribute has an element() child sequence of 
       its own -- if so, this should replace the first member of 
       our child sequence. */
    
    if(node.hasAttribute("resource")){
	var rurl = node.getAttribute("resource");
	if(rurl.includes("#element(")){
	    rurl = rurl.split("#")[1];
	    rurl = rurl.replace("element(","/");
	    rurl = rurl.replace(")","");
	    var newPath = path.substr(1);
	    newPath = newPath.substr(newPath.indexOf("/"));
	    path = rurl + newPath;
	}
    }
    
    return "element(" + path.substr(1) + ")";
}

Alphjs.getPoint = function(node,rootNode,focus){

    // Return a point() scheme XPointer for the passed node.

    rootNode = rootNode || document;
    if(!node){
	if(focus){
	    node = window.getSelection().focusNode;
	} else {
	    node = window.getSelection().anchorNode;
	}
    }
    var path = ""
    if(node.nodeType == 3){
	if(focus){
	    path = "." + window.getSelection().focusOffset;
	} else {
	    path = "." + window.getSelection().anchorOffset;
	}
    }
    while(node.parentElement){
	if(node.id && (node.id != "")){
	    path = '"' + node.id + '"' + path;
	    return path;
	} else {
	    var sibs = Array.from(node.parentElement.childNodes);
	    for(var ix in sibs){
		if(sibs[ix] == node){
		    var sibix = parseInt(ix) + 1;
		    /* Nodes are indexed from 1 in XPointer/XPath */
		    path = "/" + sibix  + path;
		    break;
		}
	    }
	}
	if(node.parentElement != rootNode){
	    node = node.parentElement;
	} else {
	    break;
	}
    }
    
    return path.substr(1);
}

Alphjs.getPoints = function(r,rootNode){

    // Return a range() scheme XPointer for the passed range.
    var resp = [];
    rootNode = rootNode || document.documentElement;
    var node = r.startContainer;
    var path = "";
    if(node.nodeType == 3){
	path = "." + r.startOffset;
    }
    while(node.parentElement){
	if(node.id && (node.id != "")){
	    path = '"' + node.id + '"' + path;
	    break;
	} else {
	    var sibs = Array.from(node.parentElement.childNodes);
	    for(var ix in sibs){
		if(sibs[ix] == node){
		    var sibix = parseInt(ix) + 1;
		    /* Nodes are indexed from 1 in XPointer/XPath */
		    path = "/" + sibix  + path;
		    break;
		}
	    }
	}
	if(node.parentElement != rootNode){
	    node = node.parentElement;
	} else {
	    break;
	}
    }
    resp.push(
	path.startsWith("/") ? path.substr(1) : path
    );
    

    node = r.endContainer;
    path = "";
    if(node.nodeType == 3){
	path = "." + r.endOffset;
    }
    while(node.parentElement){
	if(node.id && (node.id != "")){
	    path = '"' + node.id + '"' + path;
	    break;
	} else {
	    var sibs = Array.from(node.parentElement.childNodes);
	    for(var ix in sibs){
		if(sibs[ix] == node){
		    var sibix = parseInt(ix) + 1;
		    /* Nodes are indexed from 1 in XPointer/XPath */
		    path = "/" + sibix  + path;
		    break;
		}
	    }
	}
	if(node.parentElement != rootNode){
	    node = node.parentElement;
	} else {
	    break;
	}
    }
    resp.push(
	path.startsWith("/") ? path.substr(1) : path
    );
    
    return resp;
}


Alphjs.objectToHTML = function(o){
    
    /* Do a pretty-print of an object as an HTML list. */
    var ul = document.createElement("ul");
    ul.setAttribute("class","alph-description");
    if(!o) return ul;

    var keys = Object.keys(o);
    if(keys){
	for (var ix in keys){
	    var li = document.createElement("li");
	    ul.appendChild(li);
	    var lititle = document.createElement("span");
	    lititle.setAttribute("class","key");
	    if(keys[ix].startsWith("http://") ||
	       keys[ix].startsWith("https://")){
		var a = lititle.appendChild(document.createElement("a"));
		a.href = keys[ix];
		a.textContent = keys[ix];		
	    } else {
		lititle.textContent = keys[ix];
	    }
	    li.appendChild(lititle);
	    if(typeof o[keys[ix]] == "object"){
		li.appendChild(Alphjs.objectToHTML(o[keys[ix]]));
	    } else {
		var livalue = document.createElement("span");
		livalue.setAttribute("class","value");
		if(keys[ix] == "mediaCache"){
		    livalue.textContent = ": " + o[keys[ix]].substring(0,100) +
			" ... (" + o[keys[ix]].length + " chars)";
		} else if(o[keys[ix]].toString().startsWith("http://") ||
		   o[keys[ix]].toString().startsWith("https://")){
		    var a = livalue.appendChild(document.createElement("a"));
		    a.href = o[keys[ix]];
		    a.textContent = o[keys[ix]];
		} else {   
		    livalue.textContent = ": " + o[keys[ix]];
		}
		li.appendChild(livalue);
	    }
	}
    }
    return ul;
}

Alphjs.paste = function(frag){
    /*
      I THINK I have fixed this so that the hard work is done
      entirely by Alphjs.split(). */
    
    var sibs = Alphjs.split();
    if(sibs[1]){
	sibs[1].parentElement.insertBefore(frag,sibs[1]);
    }else if(sibs[0]){
	sibs[0].parentElement.appendChild(frag);
    } else {
	console.log("Paste failed: no valid sibling nodes given!");
    }

    /* That's it! */
}

Alphjs.paste_old = function(frag){
    /*
       BROKEN
    
       Insert the passed code fragment into the document, making sure
       to update the xextent and xorigin of any Alph spans we're
       pasting into.

       If called without passing a fragment, this will simply split
       the span at the caret. */
    

    var anchorNode = window.getSelection().anchorNode;
    var anchorOffset = window.getSelection().anchorOffset;

    if(anchorNode.nodeType == Node.TEXT_NODE){
	/* First, see if we're at the very end of the text node. If we
	   are, then we don't need to call split(), just move the
	   point past the end of the text node's container. */
	if(anchorOffset == anchorNode.length){}
	    
    }else if(anchorNode.nodeType == Node.ELEMENT_NODE){
	
	/* If we're not in a Text node, then we're in-between
	   elements. That's great.  We can just insert the fragment,
	   then. */
	
	anchorNode.insertBefore(frag,anchorNode.childNodes[anchorOffset]);
    } else if(anchorNode.nodeType == Node.TEXT_NODE){ // Text node
	var splitSpans = Alphjs.split();
	splitSpans[1].parentElement.insertBefore(frag,splitSpans[1]);
    }
    return frag;
}


Alphjs.cut = function(){
    var splitNodes = Alphjs.split();
    var r = document.createRange();
    r.setStartAfter(splitNodes[0]);
    r.setEndAfter(splitNodes[2]);
    r.deleteContents();
    return splitNodes;
}

Alphjs.surround = function(tag,element){
    // Wrap the selection in an HTML element
    var r = document.createRange();
    var sel = window.getSelection();
    if(!element){
	if (sel.isCollapsed){
	    var span = sel.anchorNode.parentElement;
	    r.selectNode(span);
	} else {
	    var splitNodes = Alphjs.split();
	    r.setStartAfter(splitNodes[0]);
	    r.setEndAfter(splitNodes[2]);
	}
    } else {
	r.selectNode(element);
    }
    var tag = tag || prompt("Element");
    var el = document.createElement(tag);
    r.surroundContents(el);
    return el;
}


Alphjs.split = function(withParent){
	
    /* 
       THIS THING IS A MESS

       Split the DOM at the selection point, being careful to split
       <x-text>s properly, and return references to the nodes on either 
       half of the split. */

    var returnSpans = [];
    var sel = window.getSelection();
    var range = sel.getRangeAt(0);
    var startElement = range.startContainer.parentElement;
    var startOffset = range.startOffset;
    

    /* Okay, so... first, see if we're at the very beginning or end of
       a text node. If we are, we don't really want to do a split, we
       just want to return references to the text node or its container and
       the adjacent node of that. Right? */

    if(range.startContainer.nodeType == Node.TEXT_NODE){
	if(startOffset == 0){
	    if(startElement.tagName.toLowerCase() == "x-text"){
		
		/* <x-text>s never contain multiple text nodes. Return
		   the x-text and its previousSibling. */
		
		return [startElement.previousSibling,
			startElement];
	    } else {
		return [range.startContainer.previousSibling,
			range.startContainer];
	    }
	} else if(startOffset == range.startContainer.length){
	    if(startElement.tagName.toLowerCase() == "x-text"){
		return [startElement,
			startElement.nextSibling ];
	    } else {
		return [range.startContainer,
			range.startContainer.nextSibling];
	    }
	}
    } else {
	
	/* Not inside of a Text node. As long as we're not inside of
	   an <x-text> element, we can just return references to our
	   surrounding elements since we're already at a split-point. */
	
	if(sel.anchorNode.tagName.toLowerCase() != "x-text"){
	    if(sel.anchorOffset > 0){
		return [sel.anchorNode.childNodes[sel.anchorOffset - 1],
			sel.anchorNode.childNodes[sel.anchorOffset]];
	    } else {
		return [null,sel.anchorNode.childNodes[sel.anchorOffset]];
	    }
	} else {
	    return [sel.anchorNode.previousSibling,sel.anchorNode];
	}
	
    }
    
    var startXsrc = startElement.getAttribute("src");
    var startXorigin = parseInt(startElement.getAttribute("origin"));
    var startXextent = parseInt(startElement.getAttribute("extent"));
    var endElement = range.endContainer.parentElement;
    var endOffset = range.endOffset;
    var endXsrc = endElement.getAttribute("src");
    var endXorigin = parseInt(endElement.getAttribute("origin"));
    var endXextent = parseInt(endElement.getAttribute("extent"));
    var collapsed = sel.isCollapsed;
    
    // Somewhat tricky. There's no handy method in the Web API for simply
    // splitting a tag, and Range.insertNode() will not automatically split
    // <span> tags. Maybe we need a custom tag that inherits from <p>?.
    
    // For now, we re-spec the span with new xorigin/xextent properties,
    // insert a new span after it, then insert the selection between the two
    // and then send them all to fillSpan() to have their contents refreshed.
    
    startElement.setAttribute("extent",startXorigin + startOffset);
    var newEnd = Alphjs.newSpan(startXsrc,startXorigin + startOffset, startXextent);
    
    newEnd.textContent = startElement.textContent.substr(startOffset);
    startElement.textContent = startElement.textContent.substr(0,startOffset);
    
    if(startElement.nextElementSibling){
	startElement.parentElement.insertBefore(newEnd,startElement.nextElementSibling);
    } else {
	startElement.parentElement.appendChild(newEnd);
    }
    returnSpans[0] = startElement;
    returnSpans[1] = newEnd;

    if(!collapsed){
	if (startElement === endElement){
	    // These need to be updated if we're doing a chop in the middle
	    // of a span.
	    endElement = newEnd;
	    endOffset = endOffset - startOffset;
	    endXorigin = startXorigin + startOffset;
	}

	// Do another split at the selection endpoint.
	endElement.setAttribute("extent",endXorigin + endOffset);
	var farEnd = Alphjs.newSpan(endXsrc,endXorigin+endOffset,endXextent);

	farEnd.textContent = endElement.textContent.substr(endOffset);
	endElement.textContent = endElement.textContent.substr(0,endOffset);

	if(farEnd.textContent.length > 0){
	    // Don't append farEnd to the document if it's an empty span. 
	    if(endElement.nextElementSibling){
		endElement.parentElement.insertBefore(farEnd,endElement.nextElementSibling);
	    } else {
		endElement.parentElement.appendChild(farEnd);
	    }
	    returnSpans[3] = farEnd;
	}

	returnSpans[2] = endElement;

    }

    /* If withParent is true, also split the element containing the X-TEXT. */
    if(withParent){
	var container = returnSpans[0].parentElement;
	var containerClone = container.cloneNode(false);
	var containerParent = container.parentElement;
	if(containerParent && containerParent.tagName != "HTML"){
	    container.insertAdjacentElement('afterend',containerClone);
	    var containerChildren = Array.from(container.children);
	    var a = false;
	    for(var ix in containerChildren){
		if (containerChildren[ix] == returnSpans[1]){
		    a = true;
		}
		if (a) {
		    containerClone.appendChild(containerChildren[ix]);
		}
	    }
	}
	// Place the selection between the new container elements.
	var range = new Range();
	range.setStartAfter(container);
	var sel = window.getSelection();
	sel.removeAllRanges();
	sel.addRange(range);
    }
    
    return returnSpans;
}



Alphjs.compareSpans = function(span,target){

    /* *** Only works for text spans ATM.

       *** AND, this is written to work with simple arrays, of 
       [ source, origin, offset ] instead of AlphSpan objects, which
       I would prefer, but I need to rework the findMatching...()
       functions in Docuplextron.js.
    
    */

    if(span[0] != target[0]) return false; // Different sources
    
    if(span[0].startsWith("#")) return false; // Ignore DOM sources

    /* It would be nice to audit the pipeline, so to speak, such that
       all calls to this function would be passing Numbers instead of
       Strings, thus obviating these parseInt() calls. */
    
    span[1] = parseInt(span[1]);
    span[2] = parseInt(span[2]);
    target[1] = parseInt(target[1]);
    target[2] = parseInt(target[2]);
    
    // Sort 'em
    if(span[1] < target[1]){
	if (span[2] < target[1] || span[1] >= target[2]) return false;
	var lOrigin = span[1];
	var lExtent = span[2];
	var rOrigin = target[1];
	var rExtent = target[2];
    } else {
	var rOrigin = span[1];
	var rExtent = span[2];
	var lOrigin = target[1];
	var lExtent = target[2];
    }

    if(lExtent <= rOrigin){ 	    // Left span lies outside of right span
	return false;
    } else if (lExtent < rExtent){  // Left span partially overlaps right span
	return [ span[0], rOrigin, rOrigin + (lExtent - rOrigin)];
    } else if (lExtent >= rExtent){ // Left span encloses right span
	return [ span[0], rOrigin, rExtent];
    } else {
	return false;
    }
}


Alphjs.prototype.resizeSpan = function(dir,amt,span){
    
    /* Trim/expand an X-TEXT element. */
    
    if(!parseInt(amt)) return;
    
    if(!span){
	var span = window.getSelection().anchorNode.parentElement;
	if(span.tagName != "X-TEXT") return;
    }
    
    if(dir == "extent"){
	var newExtent = parseInt(span.getAttribute("extent")) + amt;
	if (newExtent > this.sources[span.getAttribute("src")].length){
	    newExtent = this.sources[span.getAttribute("src")].length;
	}
	span.setAttribute("extent",newExtent);
    }else if (dir == "origin"){
	var newOrigin = parseInt(span.getAttribute("origin")) - amt;
	if (newOrigin < 0) newOrigin = 0;
	span.setAttribute("origin",newOrigin);
    } else {
	return;
    }
    this.fillSpan(span);
}


Alphjs.prototype.selectionChange = function(){
    var s = window.getSelection();
    this.selection = new AlphSelection();
    var tSpans = "";
    for (var ix = 0; ix < s.rangeCount; ix++){
	var range = s.getRangeAt(ix);
	// For these, startContainer is a text Node inside of an HTML element,
	// so we have to get the parentElement for the containing <x-text> or w/e.
	var startElement = range.startContainer.parentElement;
	var startOffset = range.startOffset;
	var endElement = range.endContainer.parentElement;
	var endOffset = range.endOffset;
	if (startElement === endElement){
	    // Range is confined to a single element -- EASY!
	    if (startElement.hasAttribute("src")) {
		this.selection.append( new AlphSpan(
		    startElement.getAttribute("src"),
		    parseInt(startElement.getAttribute("origin")) + startOffset,
		    parseInt(startElement.getAttribute("origin")) + endOffset));
	    } else {
		this.selection.append(AlphSpan.fromRange(range));
	    }
	} else {
	    
	    /* Now... how do we decide whether to do an HTML selection, or a
	       xanalogical selection? */

	    /* If there aren't any <x-text> elements inside the selection,
	       let's just assume that this is a non-alphic HTML document. */

	    var rContents = range.cloneContents();
	    var xtexts = rContents.querySelectorAll('x-text');

	    if(xtexts.length > 0){
	    
		// Get an HTMLCollection of all the elements contained in this Range
		var rNodes = rContents.querySelectorAll('[src]');
	    
		// Create transclusion spec for starting span
		if (startElement.hasAttribute("src")){
		    this.selection.append(new AlphSpan(
			startElement.getAttribute("src"),
			parseInt(startElement.getAttribute("origin")) + startOffset,
			parseInt(startElement.getAttribute("extent"))));
		}
		// Specs for middle spans.
		// Note that we start this loop at the *second* item in the
		// HTMLCollection, and we stop at the *next-to-last* item:
		for (var iy = 1; iy < (rNodes.length-1); iy++){
		    var spa = rNodes.item(iy);
		    if (spa.hasAttribute("src")){
			this.selection.append(new AlphSpan(
			    spa.getAttribute("src"),
			    spa.getAttribute("origin"),
			    spa.getAttribute("extent")));
		    }
		}
		// Spec for closing span
		if (endElement.hasAttribute("src")){
		    this.selection.append( new AlphSpan(
			endElement.getAttribute("src"),
			endElement.getAttribute("origin"),
			parseInt(endElement.getAttribute("origin")) + endOffset));
		}
	    } else {
		this.selection.append(AlphSpan.fromRange(range));		
	    }
	}
    }
    
    // If we're in an iframe, send the selection up.
    if(parent != window){
	parent.postMessage({"op":"selectionChange",
			    "sel":JSON.stringify(this.selection)},"*");
    } 

    this.box.content.textContent = alph.selection.asString();

    // This shouldn't be in here. It should be over in Docuplextron.js.
    if(document.getElementById("linkEditorTarget")){
	document.getElementById("linkEditorTarget").value = alph.selection.asString();
    }
}


Alphjs.prototype.popFooter = function(alphSpan){
    
    /* Pop-up the Alph info-box at the bottom of the page and populate
       it with information about the passed media span. This is called
       by mousemove events when passing over transclusible media. */

    this.box.show();
    
    var xsrc = alphSpan.src;

    // Get (or create) the metadata object for the span 
    if(xsrc.startsWith("#")){
	var sourceMeta = {"name":{"@language":"en","@value":"[scratchpad text]"},
			  "author":[{"name":"n/a"}]};
	sourceMeta.length = document.getElementById(xsrc.substr(1)).textContent.length;
    } else if(xsrc.startsWith("~") && (!this.sources[xsrc])){
	// Hmmm. How to handle this in the age of noodles?
	if(this.lamian){
	    var noodle = this.lamian.get(xsrc);
	    var sourceMeta = {"name":{"@language":"en","@value":noodle.title},
			      "author":[{"name":""}],
			      "alph:textLength":noodle.text.length};
	} else {
	    var sourceMeta = {"name":{"@language":"en","@value":"[noodle]"},
			      "author":[{"name":""}]};
	    sourceMeta.length = 0;
	}
    } else if(!this.sources[xsrc]) {
	var sourceMeta = {};
	var sourceTitle = "";
	var sourceTitleLang = this.lang;
	var sourceAuthors = "";
    } else {
	var sourceMeta = this.sources[xsrc];
	var sourceTitle = ""; //"[no title]";
	var sourceTitleLang = this.lang;
	var sourceAuthors = ""; //"[no author(s)]";
    }
    
    if (sourceMeta["name"]){
	var name = sourceMeta["name"];
	if(typeof(name) == "string"){
	    sourceTitle = name;
	} else if(Object.keys(name)[0] == "0"){
	    // Array. Multiple names...?
	    for(var ix in name){
		if(typeof(name[ix]) == "string"){
		    // Just take the first one
		    sourceTitle = name[ix];
		    break;
		} else if(this.lang.startsWith(name[ix]["@language"].substr(0,2))) {
		    // Let's just assume @lang, @value objects
		    sourceTitle = name[ix]["@value"];
		    break;
		}
	    }
	} else if(name["@value"]){
	    sourceTitle = name["@value"];
	}
    }
    
    if (sourceMeta["author"]){
	var a = sourceMeta["author"];
	sourceAuthors = "";
	for (var ix = 0; ix < a.length; ix++){
	    if (ix > 0) {
		sourceAuthors += ", ";
	    }
	    sourceAuthors += a[ix]["name"];
	}
	if (sourceAuthors != "") sourceAuthors += ", ";
    }
    
    this.box.content.textContent = sourceAuthors;
    this.box.content.appendChild(document.createElement("cite")).textContent = sourceTitle + " —";
    this.box.content.appendChild(document.createTextNode(alphSpan.toURL()));
}



Alphjs.prototype.getXSources = function(){

    /* Update sources with all of the transclusion media URLs
       used in the document, and fetch their ?describe metadata. 

       NEEDS CLEANUP

    */
    
    var spans = document.querySelectorAll('[src]');
    if(spans.length > 0){
	for (var ix in spans){
	    var xsrc = spans.item(ix).getAttribute("src");
	    if(!this.sources[xsrc]){
		this.getDescription(xsrc,true);
	    }
	}
    }
}


Alphjs.prototype.makeSourceInfo = function(src,element){
    
    /* Pretty-print the media metadata and put it inside of the passed element.
    
       The 'src' value passed should be a string containing a URL to some media
       resource. */

    if(src.startsWith("~") && (this.lamian != null)){
	var xsrc = this.lamian.get(src);
    } else {
	var xsrc = this.sources[src];
    }
    element.appendChild( Alphjs.objectToHTML(xsrc) );
}



Alphjs.prototype.queryLinks = function(alphSpan,element){
    
    /*  Query the ?links interface of an Alphic source, HTMLize the
        response and put it into the passed element. */

    if(!this.sources[alphSpan.src].alphic){
	// Non-alphic source -- no links!
	element.textContent = "This media source does not support the ?link interface.";
	return;
    }

    var url = (alphSpan.origin) ?
	alphSpan.src + Alphjs.LINK_QUERY + "=" + alphSpan.origin + "-" + alphSpan.extent :
	alphSpan.src + Alphjs.LINK_QUERY;
    
    var xhr = new XMLHttpRequest();
    xhr.onload = function(el){
	if(this.status == 200){
	    try {
		var links = JSON.parse(this.responseText);
		element.appendChild( Alphjs.objectToHTML(links) );
	    } catch(e) {
		element.textContent = this.responseText;
	    }
	} else {
	    element.textContent = "HTTP " + this.status + " --\n" + this.responseText;
	}
    }.bind(xhr,element);
    xhr.open("GET",url);
    xhr.send();
}


Alphjs.prototype.getDescription = function(src,async,force){
    
    /* Pull in the ALD metadata for a media source, or, if meta isn't
       available, cache the media and make a metadata object for it in
       alph.sources.

       TODO: Suppord Linked Data
       TODO: Extend this to support host-meta/LRDD lookups?

       src --> String (a URL)
       async --> Boolean
    */

    if(src.startsWith("moz") || src.startsWith("chrome")){
	// WebExtension resource
	return;
    }
    else if(src.startsWith("~")){
	// Noodle
	return;
    }
    else if(src.startsWith("#")){

	/* Document-local (in-browser) resource. No JSON to fetch, so
	   create a basic metadata object to store in alph.sources. */

	var sourceMeta = {"name":{"@language":"en","@value":"doctext"},
			  "author":[{"name":"n/a"}]};
	sourceMeta["alph:textLength"] = document.getElementById(src.substr(1)).textContent.length;
	this.sources[src] = sourceMeta;
	return sourceMeta;
    } else {
	if(!force){
	    if(this.sources[src]){
	    
		/* Some throttling, here. If we already have some metadata
		   and it's been checked within the last 100 seconds,
		   don't check it again. */
	    
		var metaAge = Date.now() - this.sources[src].lastChecked;
		if(metaAge < 100000){
		    return this.sources[src];
		}
	    }
	}
	
	var r = new XMLHttpRequest();
	// Probe for metadata
	if(!async){
	    // Asynchronous
	    r.onload = function(xhr,src){
		this.processMetaProbe(xhr,src);
	    }.bind(this,r,src);
	    r.open("HEAD",src);
	    r.send();
	    return r;
	} else {
	    // Synchronous
	    r.open("HEAD",src,false);
	    try{
		r.send();
	    } catch(e){
		console.log("YERGH.",src,r);
	    }
	    return this.processMetaProbe(r,src,true);
	}

    }
}

Alphjs.prototype.processMetaProbe = function(xhr,url,async){
    
    /* Process a HEAD response. See if the media supports Linked Data
       or ...other metadata sources? */
    
    if(xhr.status == 200){
	var accept = "*";

	/* Get the media type of the resource */
	var respType = xhr.getResponseHeader("Content-Type");
	if(!respType) respType = "text/plain";

	var links = xhr.getResponseHeader("Link");
	if(links){
	    links = links.split(", ");
	    for (var ix in links){
		if(links[ix].includes("http://www.w3.org/ns/ldp#Resource") &&
		   links[ix].includes('rel="type"')){
		    // LDP Resource. Send a request for its JSON
		    accept = "application/ld+json";
		}
	    }
	}

	/* If the resource is an LDPR, then we'll end-up sending a
	 * request for its Linked Data representation, here. If it
	 * isn't an LDPR, we'll just be GETting the resource itself in
	 * the hopes that we'll be able to extract some kind of
	 * metadata from it. */
	
	var r = new XMLHttpRequest();
	if(!async){
	    // Asynchronous
	    r.onload = function(xhr,url,type){
		this.processMetaResponse(xhr,url,type);
	    }.bind(this,r,url,respType);
	    r.open("GET",url);
	    r.setRequestHeader("Accept",accept);
	    r.send();
	    return r;
	} else {
	    // Synchronous
	    r.open("GET",url,false);
	    r.setRequestHeader("Accept",accept);	    
	    r.send();
	    return this.processMetaResponse(r,url,respType);
	}
    } else {
	this.sources[url] = {
	    id:url,
	    metaAge:0,
	    alphic:false,
	    httpError: xhr.status,
	    note:"processMetaProbe() failed with " + xhr.status
	}
	return this.sources[url];
	//return xhr.status;
    }
}

Alphjs.prototype.processMetaResponse = function(xhr,url,type){
    
    /* 
       xhr --> XMLHttpRequest that's already been sent
       url --> String, a URL 
    */
    
    var response = false;
    if(xhr.status == 200){
	
	/* What media type did we get this time? */
	
	var respType = xhr.getResponseHeader("Content-Type");
	if(!respType) {
	    // No Content-Type in response. (textfiles.com does this).
	    // Treat as plain-text.
	    respType = "text/plain";
	}
	
	if (respType.startsWith("application/ld+json")){

	    /* Woohoo! Linked Data! */
	    
	    response = JSON.parse(xhr.responseText);
	    response.alphic = true;
	    
	} else {
	    response = {"alphic":false};
	    
	    /* I should do something here, to try to cobble-together
	     * metadata or find metadata sources from HTTP response
	     * headers, or HTML <head> elements. */

	    /* Cache textual media */
	    
	    if(respType.startsWith("text/")){

		/* This is important! 

		   We need to read this length from the text that was
		   sent from the server. If we were to put this text
		   into an element and then try to read its length,
		   we'd get an incorrect number. The responseText and
		   the text in the mediaCache retains CR/LF newlines,
		   but when that text goes into a DOM element, the
		   newlines are converted. */
		
		response.mediaCache = xhr.responseText;
		response["alph:textLength"] = response.mediaCache.length;		
	    }
	}
	
	this.sources[url] = response;
	this.sources[url].lastChecked = Date.now();

	this.sources[url].mediaType = type;
	
	this.fulfilSource(url); 	// This should maybe not be here.

	
	/* If we're in an iframe, send-up the metadata to our parent. */
	
	if(parent != window){
	    var sourceUpdate = {};
	    sourceUpdate[url] = response;
	    parent.postMessage({"op":"sourcesResponse",
				"sources":JSON.stringify(sourceUpdate)},"*");
	}

	this.sources[url].id = url;
	return this.sources[url];
    } else {
	// HTTP error status.
	this.sources[url] = {
	    id:url,
	    metaAge:0,
	    alphic:false,
	    httpError: xhr.status,
	    note:"processMetaResponse() failed with " + xhr.getAllResponseHeaders()
	}
	return this.sources[url];

	//return xhr.status;
    }
}

Alphjs.prototype.fulfilSource = function(xsrc,element){
    
    /* Retrieve all media spans for a particular text source, and put them
       in the document. xsrc should be a URL string, a key name in
       alph.sources; called with an element will confine the action that
       element's children.

       TEXT-ONLY, ATM
    */
    
    if (!element) element = document;

    var spans = element.querySelectorAll("x-text[src='" + xsrc  + "']");
    
    if(xsrc.startsWith("#")){

	// DOM source
	try{
	    var srcText = document.getElementById(xsrc.substr(1)).textContent;
	} catch(e){
	    console.log("Could not locate element with ID ", xsrc);
	    return;
	}
	for(var iy = 0; iy < spans.length; iy++){
	    var span = spans.item(iy);
	    span.textContent = srcText.substring(
		parseInt(span.getAttribute("origin")),
		parseInt(span.getAttribute("extent")));
	}
    } else if(xsrc.startsWith("~")){
	if(!this.lamian) return;
	var srcNoodle = this.lamian.get(xsrc);
	if (srcNoodle) {
	    for(var iy = 0; iy < spans.length; iy++){
		var span = spans.item(iy);
		span.textContent = srcNoodle.text.substring(
		    parseInt(span.getAttribute("origin")),
		    parseInt(span.getAttribute("extent")));
	    }
	}
    } else if(this.sources[xsrc].mediaCache){
	
	/* Non-xanalogical network source, text should have been
	   cached by getDescription() */
	
	var srcText = this.sources[xsrc].mediaCache;
	for(var iy = 0; iy < spans.length; iy++){
	    var span = spans.item(iy);
	    span.textContent = srcText.substring(
		parseInt(span.getAttribute("origin")),
		parseInt(span.getAttribute("extent")));
	}
    } else {
	/* Network source.

	   This is a relatively efficient way to pull-in all text
	   transclusions from their originating servers. I concatenate
	   all of the transclusion ranges for each URL into a single
	   query, then chop the returned string into the right
	   substrings for each span.
	*/

	var tspec = ""; // ?fragment query string
	for (var iy = 0; iy < spans.length; iy++){
	    tspec += spans.item(iy).getAttribute("origin") + "-" +
		spans.item(iy).getAttribute("extent") + "`";
	}
	
	// No query string? Nothing to fetch, then!
	if (tspec == "") return;
	
	if (tspec.endsWith("`")) tspec = tspec.substring(0,tspec.length -1);
	var url = xsrc + Alphjs.TRANSCLUDE_QUERY + tspec;
	
	var r = new XMLHttpRequest();
	r.onload = function(spans){
	    var response = this.responseText;
	    var lastOffset = 0;
	    for(var iz = 0; iz < spans.length; iz++){
		// Fill the spans with substrings from the response text,
		// and whittle it down as you go.
		var span = spans.item(iz);
		var origin = parseInt(span.getAttribute("origin"));
		var extent = parseInt(span.getAttribute("extent"));
		var length = extent - origin;
		span.textContent = response.substr(lastOffset,length);
		lastOffset += length;
	    }
	}.bind(r,spans);
	r.open("GET",url);
	r.send();
    }
}

Alphjs.prototype.fetchAndFill = function(element){
    
    /* Fulfil all of the text transclusions in an element (or the 
       entire document. */
    
    if(!element) element = document;
    var srcKeys = Object.keys(this.sources);
    for (var ix = 0; ix < srcKeys.length; ix ++){
	this.fulfilSource(srcKeys[ix],element);
    }
}

Alphjs.prototype.fillSpan = function(span){

    /* Inefficient method of fetching the remote media for a single element.  */

    var xsrc = span.getAttribute("src");
    
    // First, make sure the origin and extent are the right way 'round
    var origin = Math.min(parseInt(span.getAttribute("origin")),
			  parseInt(span.getAttribute("extent")));
    var extent = Math.max(parseInt(span.getAttribute("origin")),
			  parseInt(span.getAttribute("extent")));
    span.setAttribute("origin",origin);
    span.setAttribute("extent",extent);

    /* If the caret/selection is inside this span, save its offset
       so that we can restore it later, because replacing the span's
       textContent will move the Selection's anchorNode */
    
    var offset;
    if(window.getSelection().anchorNode == span.childNodes[0]){
	offset = window.getSelection().getRangeAt(0).startOffset;
    }

    if(xsrc.startsWith("#")){
	// DOM source
	span.textContent = document.getElementById(xsrc.substr(1)).textContent.substring(origin,extent);
	if(offset){
	    Alphjs.restoreCaretPosition(span,offset);
	}
    } else if(this.sources[xsrc].mediaCache){
	// Non-Alphic source
	span.textContent = this.sources[xsrc].mediaCache.substring(origin,extent);
	if(offset){
	    Alphjs.restoreCaretPosition(span,offset);
	}
    }else {
	// Alphic network source
	var r = new XMLHttpRequest();
	r.onload = function(span,offset){
	    span.textContent = this.responseText;
	    if(offset){
		Alphjs.restoreCaretPosition(span,offset);
	    }
	}.bind(r,span,offset);
	r.open("GET",span.getAttribute("src") + Alphjs.TRANSCLUDE_QUERY +
	       span.getAttribute("origin") + "-" + span.getAttribute("extent"));
	r.send()
    }
}



function AlphBox(){
    
    /* This is an info-popup at the bottom of the screen; it shows
       information about the source media that the pointer passes over,
       and the media spans of the current selection. */
    
    this.timeout;
    
    this.container = document.createElement("div");
    this.container.id = "alph-footer";
    this.container.style.display = "none";
    
    this.logo = document.createElement("img");
    this.logo.id = "alph-logo";
    // this.logo.src = "icons/alph.svg";
    this.logo.src = Alphjs.logoURL;
    this.logo.width = "22";
    this.logo.onmousemove = function(e){
	// Mouse-over of the logo will change the information in
	// the box unless we block it!
	e.stopPropagation();
	e.preventDefault();
    }
    this.container.appendChild(this.logo);
    
    this.content = document.createElement("div");
    this.content.id = "alph-infotext";
    this.container.appendChild(this.content);

    // Replace any pre-existing AlphBox DIV if one exists for some reason.
    if(document.getElementById("alph-footer")){
	document.getElementById("alph-footer").remove();
    }
    document.body.appendChild(this.container);
    
    this.show = function(){
	this.container.style.display = "block";
	document.body.appendChild(this.container);
	// ↓ Should this be here? Or elsewhere?
	if (this.timeout){
	    clearTimeout(this.timeout);
	}
	this.timeout = setTimeout(this.autoHide,5000);
    }.bind(this);
    
    this.hide = function(){
	this.container.style.display = "none";
    }.bind(this);
    
    this.autoHide = function(){
	/*
	console.log("Autohide called with ", this);
	var r = this.container.getBoundingClientRect();
	if(alph.lastY < r.top || alph.lastY > r.bottom){
	*/
	this.hide();
	/*
	} else {

	    this.timeout = setTimeout(this.autoHide,5000);
	}*/
    }.bind(this);
}

function AlphSelection(){
    
    /* This only works for text at the moment. 

       This is a transclusion-based selection object. That is, instead
       of storing a span of characters or pixels or what-have-you, it
       stores an EDL of media fragments. */
    
    this.spans = [];
    
    this.asString = function(){
	var selString = "";
	var lastSrc = "";
	for (var ix = 0; ix < this.spans.length; ix++){
	    var range = this.spans[ix];
	    if(range.src == lastSrc){
		selString += "`" + range.origin + "-" + range.extent;
	    } else {
		lastSrc = range.src;
		selString += "\n" + range.toURL();
	    }
	}
	return selString.trim();
    }.bind(this);
    
    this.append = function(range){
	this.spans.push(range);
    }.bind(this);
    
    this.toFragment = function(){
	var frag = document.createDocumentFragment();
	for(var ix = 0; ix < this.spans.length; ix++){
	    var span = this.spans[ix];
	    var el = Alphjs.newSpan(span.src,span.origin,span.extent);
	    el.textContent = Array(span.extent - span.origin).fill("_").join("");
	    frag.appendChild(el);
	}
	return frag;
    }.bind(this);

    this.toHTML = function(){
	var codestring = "";
	for(var ix = 0; ix < this.spans.length; ix++){
	    var span = this.spans[ix];
	    codestring += '<x-text src="' + span.src + '" origin="' +
		span.origin + '" extent="' + span.extent + '">' +
		Array(span.extent - span.origin).fill("😐").join("") +
		'</x-text>';
	}
	return codestring;
    }.bind(this);

    this.fromMessage = function(obj){
	var newSpans = obj["spans"];
	for(var ix in newSpans){
	    this.append(new AlphSpan( newSpans[ix]["src"],
				      newSpans[ix]["origin"],
				      newSpans[ix]["extent"]) );
	}
	return this;
    }.bind(this);
}


function AlphSpan(src,origin,extent,name){
    /* This needs a lot of work, to support audiovisual and XML sources. */
    this.src = src;
    this.origin = origin; 
    this.extent = extent;
    this.name = name; 
    this.toURL = function(){
	if(!origin){
	    // Document pointer. No offset/origin.
	    return this.src;
	} else if(parseFloat(this.origin) == this.origin){
	    // Simple numeric origin/offset format
	    return this.src + "#" + this.origin + "-" + this.extent;
	} else if(this.origin.match(/[\d\.,]+/) == this.origin){
	    // n-dimensional numeric origin/offset
	    return this.src + "#" + this.origin + "-" + this.extent;
	} else if (this.origin.startsWith("point(")){
	    //Xpointer range
	    return this.src + "#range(" +
		this.origin.match(/point\((.+)\)/)[1] + "," +
		this.extent.match(/point\((.+)\)/)[1] + ")";
	}
    }.bind(this);
    this.toHTML = function(){
	return '<x-text src="' + this.src + '" origin="' + this.origin + '" extent="' +
	    this.extent + '">😐</xtext>';
    };
    this.toDOMFragment = function(){
	var xtext = document.createElement("x-text");
	xtext.setAttribute("src",this.src);
	xtext.setAttribute("origin",this.origin);
	xtext.setAttribute("extent",this.extent);
	xtext.textContent = "😐";
	return xtext;
    }
}

/***/

AlphSpan.fromURL = function(url){
    
    /* This could turn into a whole thing, but... let's just get some
       essentials taken care of. */
    
    if(url.includes(Alphjs.TRANSCLUDE_QUERY)){
	/* *** Old-style. Deprecate? ***
	   This is a URL like:
	      http://host/path/file?fragment=123,456 
	*/
	
	url = url.split(Alphjs.TRANSCLUDE_QUERY);
	var spec = url[1].split("-");
	return new AlphSpan(url[0],spec[0],spec[1]);
    } else if(!url.includes("#")){
	return new AlphSpan(url);
    } else if(url.includes("#element(")){
	var span = new AlphSpan(url.split("#")[0]);
	/* ... */
    } else if(url.includes("#range(")){
	var span = new AlphSpan(url.split("#")[0]);
	/* ... */
    } else {
	var span = new AlphSpan(url.split("#")[0]);
	var fragment = url.split("#")[1];

	/* Assume Alph shorthand fragment? */

	fragment = fragment.split("-");
	span.origin = fragment[0];
	span.extent = fragment[1];
    }
    return span;
}

/***/

AlphSpan.fromXtext = function(xtext){
    return new AlphSpan(xtext.getAttribute("src"),
			xtext.getAttribute("origin"),
			xtext.getAttribute("extent"));
}

/***/

AlphSpan.fromRange = function(r){
    var src = window.location.toString().split("#")[0];
    var srcEl = Alphjs.getAncestorByAttribute(r.startContainer,"content-source");
    if(srcEl){
	src = srcEl.getAttribute("content-source").split("#")[0];
    } else {
	srcEl = document.documentElement;
    }
    var pointers = Alphjs.getPoints(r,srcEl);
    var span = new AlphSpan(src,
			    "point(" + pointers[0] + ")",
			    "point(" + pointers[1] + ")");
    
    return span;
}

Alphjs.getAncestorByAttribute = function(el,attr,val){
    // Find the ancestor of el that has the attribute attr which
    // matches value val
    
    try{
	if(!el.nodeType){
	    // Not a DOM node
	    return false;
	} else if(el.nodeType == Node.TEXT_NODE){
	    var p = el.parentElement;
	} else if(el.nodeType == Node.ELEMENT_NODE){
	    var p = el;
	} else {
	    return false;
	}
    } catch(e){ debugger; }
    
    while(p){
	if(p.hasAttribute(attr)){
	    if(!val){
		return p;
	    } else {
		if(p.getAttribute("attr").match(val)){
		    return p;
		}
	    }
	}
	p = p.parentElement;
    }
    
    if(!p){
	return false
    } else {
	return p;
    }

}

Alphjs.urlToFragments = function(url){
    
    /* Parse a concatenated fragment query string into
       an array of AlphSpan objects 

       Shouldn't I be using this in Docuplextron.js's pushAllLinks()? 
    */
    
    var spanObjects = [];
    var xsrc = url.split("?")[0];
    var frags = url.split("fragment=")[1];
    frags = frags.split("&")[0];
    frags = frags.split("`");
    for(var ix=0;ix<frags.length;ix++){
	var splitFrag = frags[ix].split("-");
	spanObjects.push( new AlphSpan( xsrc, splitFrag[0], splitFrag[1]) );
    }
    return spanObjects;
}

/***/

Alphjs.joinContiguousSpans = function(element){
    
    /* Find <x-text> elements with contiguous content spans and merge them
       into a single element. 

       This recurses in a really inefficient way. */

    var element = element || document;
    
    var spans = Array.from(element.querySelectorAll("x-text"));
    for (var ix in spans){
	var span = spans[ix];
	var nextSpan = span.nextSibling;
	if(nextSpan){
	    if(nextSpan.tagName == "X-TEXT"){
		if(
		    ( span.getAttribute("src") == nextSpan.getAttribute("src") ) &&
			( span.getAttribute("extent") == nextSpan.getAttribute("origin") )
		){
		    span.setAttribute("extent", nextSpan.getAttribute("extent") );
		    span.textContent += nextSpan.textContent;
		    nextSpan.remove();
		    return Alphjs.joinContiguousSpans();
		}
	    }		
	}
    }
}

/***/

Alphjs.cullEmptyNodes = function(element){
    
    /* If I get the split/cut/past-into functions working properly,
       this may be obsolete, but for now I need a function to nuke all
       of the empty span tags that those operations sometimes
       produce. */

    var element = element || document.body;
    var empties = element.querySelectorAll("x-text:empty,span:empty,p:empty");
    for (var ix in empties){
	try{
	    empties.item(ix).remove();
	} catch(e){
	    //console.log("Unable to kill this empty span:",empties.item(ix));
	}
    }
    // Also, nuke whitespace
    Alphjs.cullWhitespaceNodes(element);
}

/***/

Alphjs.cullWhitespaceNodes = function(element){
    
    /* When dealing with plain-text files, there is a lot of presentational
       white space to strip away. It's a pain in the ass to do manually, so
       this lets us just do a split on either side of any unwanted white space
       and then have this function round-up all the crud and delete it. */

    var element = element || document.body;
    
    var empties = element.querySelectorAll("x-text");
    for (var ix in empties){
	if(empties.item(ix).textContent.match(/^\s+$/)){
	    try{
		empties.item(ix).remove();
	    } catch(e){
		//console.log("Unable to kill this whitespace span:",empties.item(ix));
	    }
	}
    }
}

/***/

Alphjs.cullOrphanNodes = function(target){
    
    /* Careful with this one! */
    
    var target = target || document.body;
    
    var bangers = Array.from(target.querySelectorAll("[src^=\"!\"]"));
    while(bangers.length > 0){
	var b = bangers.pop();
	b.remove();
    }
}



