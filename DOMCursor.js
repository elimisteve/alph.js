function DOMCursor(){
    this.target;
    this.enabled = true;

    this.div = document.createElement("div");
    this.div.id = "alphDOMCursor";
    this.div.style.display = "none";
    
    this.label = document.createElement("span");
    this.label.id = "alphDOMCursorInfotext";

    document.body.appendChild(this.div);
    document.body.appendChild(this.label);


    this.boundsCheck = function(el){

	/* Replace this function with some kind of bounds-check, to
	   keep certain elements from being selected. */
	
	return true;
    }
    
    this.toggle = function(){
	this.enabled = (this.enabled) ? false : true;
	if(!this.enabled){
	    this.target = null;
	    this.div.style.display = "none";
	    this.label.style.display = "none";
	} else {
	    this.div.style.display = "block";
	    this.label.style.display = "inline-block";
	}
    };

    this.updatePosition = function(){
	if(this.target && this.enabled){
	    if(this.target.nodeType == Node.ELEMENT_NODE){
		var rectSource = this.target;
	    } else if(this.target.nodeType == Node.TEXT_NODE){
		// What a headache these text nodes are.
		var rectSource = document.createRange();
		rectSource.selectNode(this.target);
	    } else {
		this.target = null;
		return;
	    }

	    if(!rectSource.getClientRects()[0]){
		this.target = null;
		return;
	    }
	    
	    this.div.style.display = "block";
	    this.label.style.display = "inline-block";

	    var r = rectSource.getBoundingClientRect();

	    this.div.style.width = r.width + "px";
	    this.div.style.height = r.height + "px";
	    this.div.style.top = r.top + "px";
	    this.div.style.left = r.left + "px";
	    
	    this.label.style.top = Math.max(
		this.div.getBoundingClientRect().top -
		    this.label.getBoundingClientRect().height,
		0
	    ) + "px";
	    this.label.style.left = Math.max(
		rectSource.getClientRects()[0].left,0
	    ) + "px";

	    var clientRects = rectSource.getClientRects();

	    /* If this is a huge text element with a ton of rects, just
	       use the bounding rect. */
	    
	    /*
	    if(clientRects.length > 40){
		clientRects = [this.target.getBoundingClientRect()];
	    }
	    */
	    
	    /* We want to re-use existing divs instead of creating new ones,
	       but if we have excess, trim 'em off. */
	    
	    while(this.div.children.length > clientRects.length){
		this.div.removeChild(this.div.children[0]);
	    }
	    var availableRects = this.div.children.length;
	    var usedRects = 0;
	    
	    for (var ix in clientRects){
		var cRect = clientRects[ix];

		// Skip rects that are off-screen
		if(!cRect.top ||
		   cRect.top > window.innerHeight ||
		   cRect.bottom < 1 ||
		   cRect.left > window.innerWidth ||
		   cRect.right < 1) continue;
		// Re-use existing divs
		if(usedRects < availableRects){
		    var d = this.div.children[usedRects];
		    usedRects++;
		} else {
		    // Create new ones as needed.
		    var d = document.createElement("div");
		    this.div.appendChild(d);
		}
		d.style.top = cRect.top + "px";
		d.style.left = cRect.left + "px";
		d.style.width = cRect.width + "px";
		d.style.height = cRect.height + "px";
	    }
	} else {
	    this.label.style.display = "none";
	    this.div.style.display = "none";
	}

    }
    
    /***/
    
    this.select = function(el){

	if(!this.boundsCheck(el)){ 

	    /* Should I also make this.target = null or something? Or leave
	       the current selection in place? */
	    
	    this.target = null;
	    return;
	}

	this.target = el;
	
	this.label.textContent = "";
	
	if(!this.target)return;
	
	if(this.target.parentElement &&
	   this.target.parentElement.tagName != "X-FLOATER") {
	    this.label.textContent = this.target.parentElement.tagName + "◀";
	}
	
	this.label.appendChild(document.createElement("strong")).textContent =  (this.target.tagName || "#") + ( this.target.id ? "#" + this.target.id : "" );
	
	if(this.target.firstElementChild){
	    this.label.appendChild(document.createTextNode("▶" +
		this.target.firstElementChild.tagName));
	}
	this.updatePosition();
    }
    
    /***/
    
    this.move = function(dir){
	
	/* The postMessage() calls in here should probably be moved out into the
	   Docuplextron code.*/
	
	var ae = this.target;
	
	if(!ae) return;
	
	switch(dir){
	case "up":
	    // Move active element up the tree
	    ae.parentElement.insertBefore(ae,ae.previousSibling);
	    parent.postMessage({"op":"findMyMatchingContent"},"*");
	    break;
	case "shiftUp":
	    // Move active element up INTO bottom of adjacent container, or
	    // up OUT of top of current container.
	    var ps = ae.previousSibling;
	    var pa = ae.parentElement;
	    if(!ps) {
		if(pa &&
		   this.boundsCheck(pa) &&
		   pa != document.body){
		    var oldParent = ae.parentElement;
		    oldParent.parentElement.insertBefore(ae,ae.parentElement);
		}
	    } else {
		if(ps.nodeType == Node.ELEMENT_NODE &&
		   ps.tagName != "X-TEXT"){
		    ps.appendChild(ae);
		}
	    }
	    parent.postMessage({"op":"findMyMatchingContent"},"*");
	    break;
	case "down":
	    // Move active element down the tree
	    if(ae.nextSibling){
		ae.parentElement.insertBefore(ae.nextSibling,ae);
	    } else {
		ae.parentElement.insertBefore(ae,ae.parentElement.firstChild);
	    }
	    parent.postMessage({"op":"findMyMatchingContent"},"*");
	    break;
	case "shiftDown":
	    // Move active element down INTO top of adjacent container, or
	    // down OUT of the bottom of the current container.
	    var ns = ae.nextSibling;
	    var pa = ae.parentElement;
	    if (!ns) {
		if(pa &&
		   this.boundsCheck(pa) &&
		   pa != document.body){
		    pa.parentNode.insertBefore(ae,pa);
		    pa.parentNode.insertBefore(pa,ae);
		}
	    } else {
		// Move into adjacent node, unless it is <x-text> or,
		// obviously, not an element.
		if(ns.nodeType == Node.ELEMENT_NODE &&
		   ns.tagName != "X-TEXT"){
		    ns.insertBefore(ae,ns.firstChild);
		}
	    }
	    parent.postMessage({"op":"findMyMatchingContent"},"*");
	    break;
	case "left":
	    // Selactivate the active element's previous sibling.
	    if(ae.previousSibling){
		this.select(ae.previousSibling);
	    } else {
		this.select(ae.parentElement.lastChild);
	    }
	    window.getSelection().getRangeAt(0).setStartBefore(ae);
	    window.getSelection().collapseToStart();
	    break;
	case "shiftLeft":
	    // Selactivate the active element's parent element.
	    this.select(ae.parentElement);
	    break;
	case "right":
	    // Selactivate the next sibling element.
	    if(ae.nextSibling){
		this.select(ae.nextSibling);
	    } else {
		this.select(ae.parentElement.firstChild);
	    }
	    window.getSelection().collapse(ae,0);
	    break;
	case "shiftRight":
	    // Selactivate the active element's first element child.
	    this.select(ae.firstChild);
	    break;
	}
	this.updatePosition();
	return this.target;
    }
}

DOMCursor.prototype.kill = function(){
    
    // Select an item that will become the new DOMCursor target
    var prevElement;
    
    if(this.target.previousElementSibling){
	prevElement = this.target.previousElementSibling;
    } else if(this.target.nextElementSibling){
	prevElement = this.target.nextElementSibling;
    } else if(this.target.parentElement){
	prevElement = this.target.parentElement;
    } else {
	prevElement = null;
    }
		
    this.target.remove();
    this.select(prevElement);

    return prevElement;
}
