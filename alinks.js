/* Alph Links

   Our links, to a large extent, match the Web Annotation Data Model,
   and we should be able to interoperate with Web Annotations without
   much fuss. Links have an id, a type, a body, and a target, plus 
   any additional properties that you might find in the Web Annotation
   spec. 

   Internally though, we model bodies and targets a bit differently.
   Unlike Web Annotations, we ALWAYS assume bodies have an "items"
   property, and are of one of the proposed "sets" types (List, Composite,
   or Independents). Each item is an AlphSpan object. For links to whole
   documents, origin and extent are null.

   So, for example, in Web Annotations, the simplest body/target is
   a URL, like either of these:

   "body" : "http://some.site/some/text"

   "body" : {
     "id" : "http://some.site/some/text",
     "type" : "Text",
     "format" : "text/plain"
   }

   Instead, we use this:

   "body" : {
     "items" : [
       {
         "src" : "http://some.site/some/text",
	 "origin" : null,
	 "extent" : null
       }
     ]
   }

   We don't worry about type or format in the link structure, because
   we store that information in Alph.sources when we do metadata
   discovery on the resource.

   A TextualBody annotation in Web Annotations looks like this:

   "body": { 
     "type" : "TextualBody",
     "value" : "Some text..."
   }

   But we do that with a data URI, like this:

   "body" : {
     "items" : [
       {
         "src" : "data:text/plain,Some text...",
	 "origin" : null,
	 "extent" : null
       }
   }

*/


function Alinks(){
    
}

function Alink (id,type,body,target){

    this.id = id || "";
    this.type = type || "";
    this.body = body || null;
    this.target = target || null;

}

Alinks.asAnnotation = function(l){

    var anno = Object.assign({},l);
    anno["@context"] = "http://www.w3.org/ns/anno.jsonld";

    /* Now we have to go through and make all of the targets and bodies
       comply with the Web Annotations Data Model
       ...
    */
    
    return this;
}

function AlinkBody (type,items){
    this.type = type || "List";
    this.items = items || [];
}

Alink.fromLink = function(hlink){
    var link = new Alink();
    link.type = "Weblink";
    link.id = hlink.ownerDocument.location.toString() + (
	(hlink.id != "") ?
	    ("#" + hlink.id) :
	    ("#xpath:" + Alphjs.getXPath(hlink))
    );
    link.body = new AlinkBody(
	null,
	[
	    new AlphSpan(hlink.ownerDocument.location.toString())
	]);
    link.target = new AlinkBody(
	null,
	[
	    new AlphSpan(hlink.href)
	]);
    if (hlink.rel) link.rel = hlink.rel;
    if (hlink.title) link.title = hlink.title;
    return link;
}

Alinks.bodyFromAnchor = function(a){
    // Get an id for the anchor element itself
    var anchorId = "(unresolved)";
    
    // Is the anchor inside of a floater?
    var f = Docuplextron.getParentItem(a);
    if(f){
	// Is the floater a container for an imported DOM fragment?
	if(f.id.startsWith("[")){
	    var basePath = f.id.substr(1,f.id.indexOf("]"));
	    basePath = basePath.split("#")[0];

	    if(a.id != "") {
		// If a has an id, we're all set.
		anchorId = basePath + "#" + a.id;
	    } else if(f.id.includes("#")){
		/* If a doesn't have an id, but the floater it's in has a
		   fragment, use the fragment as the id() base of an XPath.

		   *** This isn't perfect. It assumes that any fragment identifier
		   in the floater's id will be an id/name. What if the fragment
		   is an xpath: ? */
		anchorId = f.id.substr(1,f.id.indexOf("#")) +
		    "xpath:id(\"" + f.id.substring(f.id.indexOf("#")+1,f.id.length -1) + "\")" +
		    Alphjs.getXPath(a,f).substr(1);
	    } else {
		/* The anchor has no id, and the floater's id does not have a
		   fragment, then we assume the floater contains an entire 
		   document, and we get an XPath for the anchor using the
		   floater as the document root. */
		anchorId = basePath + "#xpath:" + Alphjs.getXPath(a,f);
	    }		     
	} else {
	    /* Floater contains a local document fragment. */
	    var basePath = a.ownerDocument.location.toString().split("#")[0];
	    if(a.id != "") {
		// If a has an id, we're all set.
		anchorId = basePath + "#" + a.id;
	    } else {
		anchorId = basePath +
		    "#xpath:id(\"" + f.id + "\")" + Alphjs.getXPath(a,f).substr(1);
	    }
	}

    } else {
	/* We are probably not in the docuplextron */
	
	var anchorId = a.ownerDocument.location.toString().split("#")[0] + (
	    (a.id != "") ?
		("#" + a.id) :
		("#xpath:" + Alphjs.getXPath(a))
	);
    }
    
    alink = new AlinkBody();

    /* Now, we want to try to make a xanalogical link body if we can. So, 
       get the media fragments within the anchor, if there are any, and 
       put them together as a "List" body structure. */
    
    var mediaElements = Array.from(a.querySelectorAll("[src]"));
    if(mediaElements.length > 0){
	alink.type = "List";
	alink.items = [];
	for (var ix in mediaElements){
	    var span = new AlphSpan(
		mediaElements[ix].getAttribute("src"), null , null );
	    
	    if(mediaElements[ix].tagName == "X-TEXT"){
		span.origin = mediaElements[ix].getAttribute("origin");
		span.extent = mediaElements[ix].getAttribute("extent");
	    } else {

		/* Don't I have a clever function yet that dereferences URLs
		   into AlphSpans? That would be good here. */
		
	    }
	    
	    alink.items.push(span);	    
	}
    } else {
	//alink.items = [ "data:text/plain," + a.textContent ];
	alink.items = [ { "src" : anchorId, "origin" : null, "extent" : null } ];
    }
    
    return alink;
}

Alinks.targetFromAnchor = function(a){
    
    /* Do something here where we dereference concatenated fragment URLs
       into a "List" type target structure. */

    alink = new AlinkBody(null,[ { "src" : a.href, "origin" : null, "extent" : null } ] );
    return alink;
}
