/*

  ALPH.JS
  The Alph Project <http://alph.io/>
  ©2016,2017 Adam C. Moore (LÆMEUR) <adam@laemeur.com>

  This software is a prototype multi-document interface for
  xanalogical Web media as implemented by the Alph specifications. It
  includes support for the Xanadu Project's <http://xanadu.com>
  current xanadoc and xanalink formats.

  This is free software and is released under the terms of the current
  GNU General Public License <https://www.gnu.org/copyleft/gpl.html>

  Source code and revision history are available on GitLab:
  <https://gitlab.com/alph-project/alph.js>

*/

var alph = new Alphjs();
var dptron = new Docuplextron();

/*






   POSTMESSAGE I/O __________________________________________________






*/

function handleMessage(m){
    
    var msg = m.data;
    switch(msg["op"]){
    case "keyDn" :
	dptron.keys[msg["which"]] = true;
	break;
    case "keyUp" :
	dptron.keys[msg["which"]] = false;
	break;
    case "isAlphMyParent" :
	// Sent by iframes to see if they should run iframeInit() or not.
	m.source.postMessage({"op":"iframeInit"},"*");
	break;
    case "iframeInit" :
	// Sent from the parent context to tell the iframe that Alph is listening
	iframeInit();
	break;
    case "floaterContextMenu":
	// Sent by iframes to get an Alph context menu at the top level
	dptron.activeMenu = dptron.contextMenuOn(
	    Docuplextron.getParentItem(Docuplextron.frameOf(m.source)));
	break;
    case "spanContextMenu":
	// Sent by iframes to get an Alph context menu for a particular media
	// fragment at the top level.
	msg.returnAddress = Docuplextron.frameOf(m.source);
	dptron.activeMenu = dptron.contextMenuOn(msg);
	break;
    case "updateCursorPosition":
	// Sent up from iframes to let the parent context know where the cursor is

	var f = Docuplextron.frameOf(m.source);
	var fl = Docuplextron.getParentItem(f);
	dptron.lastX = f.getBoundingClientRect().left + (fl.getAttribute("data-scale") * msg["x"]);
	dptron.lastY = f.getBoundingClientRect().top + (fl.getAttribute("data-scale") * msg["y"]);
	break;
    case "iAmTheContext":
	var issuingFrame = Docuplextron.frameOf(m.source);
	if(issuingFrame != dptron.currentContext){
	    dptron.currentContext = issuingFrame;
	    dptron.reportSizeToParent();
	    dptron.currentSpan = new AlphSpan();
	    dptron.rebuildTranspointers();
	}
	break;
    case "iAmActive":
	var issuingFrame = Docuplextron.frameOf(m.source);
	if(issuingFrame != dptron.currentContext){
	    dptron.currentContext = issuingFrame;
	    Dptron.reportSizeToParent();
	    dptron.activate(Docuplextron.getParentItem(dptron.currentContext));
	    dptron.currentSpan = new AlphSpan();
	    dptron.rebuildTranspointers();
	}
	break;
    case "requestLinkRects":
	m.source.postMessage({"op":"receiveLinkRects",
			      "span":msg["src"] + "~" + msg["origin"] + "~" + msg["extent"],
			      "matches": dptron.getMatchingContentRects(msg["type"],
								 msg["src"],
								 msg["origin"],
								 msg["extent"],
								 msg["id"]
								)},
			     "*");
	break;
    case "requestRects" :
	// Send the matching content rects for the passed span
	m.source.postMessage({"op":"receiveRects",
			      "id":msg["id"],
			      "span":msg["src"] + "~" + msg["origin"] + "~" + msg["extent"],
			      "matches": dptron.getMatchingContentRects(msg["type"],
								 msg["src"],
								 msg["origin"],
								 msg["extent"],
								 msg["id"]
								)},
			     "*");
	break;
    case "receiveRects" :
	dptron.receiveRects(msg, m.source);
	break;
    case "reportSizeToParent":
	dptron.reportSizeToParent();
	break;
    case "sizeReport":
	var iframe = Docuplextron.frameOf(m.source);
	console.log(msg);
	if(iframe){
	    if(iframe.tagName == "IFRAME"){
		var floater = Docuplextron.getParentItem(iframe);
		iframe.style.width = "100%";
		iframe.style.height = msg["height"];
		floater.style.width = msg["width"];
	    }
	    else {
		iframe.style.width = msg["width"];
		iframe.style.height = msg["height"];
	    }
	}
	break;
    case "pickMeUp":
	// This message is rec'd by the parent window from an iframe, asking to
	// pick-up the iframe's floater.
	dptron.floaterManager.pickUp(Docuplextron.getParentItem(Docuplextron.frameOf(m.source)));
	break;
    case "dropAll":
	// If we're the topmost window, repeat this to all iframes
	//console.log("DropAll rec'd", m.source);
	if(parent == window){
	    dptron.floaterManager.drop();
	    Docuplextron.broadcast({"op":"dropAll"});
	} else {
	    dptron.mouseBypass = false;
	}
	break;
    case "inhandIsParent":
	dptron.inhand = "parent";
	break;
    case "moveMe":
	dptron.floaterManager.inhand.move( msg["x"], msg["y"] );
	break;
    case "zMoveMe":
	if(msg["shifted"]){
	    dptron.floaterManager.zoomWorkspace(
		msg["deltaX"],msg["deltaY"],dptron.lastX,dptron.lastY
	    );
	} else {
	    Docuplextron.getParentItem(Docuplextron.frameOf(m.source)).zMove(
		msg["deltaX"], msg["deltaY"], dptron.lastX,dptron.lastY,true
	    );
	}
	break;
    case "resetZoom":
	Docuplextron.getParentItem(Docuplextron.frameOf(m.source)).resetZoom();
	break;
    case "panWorkspace":
	dptron.floaterManager.panWorkspace(msg["x"],msg["y"]);
	break;
    case "zoomWorkspace":
	dptron.floaterManager.zoomWorkspace(
	    msg["deltaX"], msg["deltaY"], dptron.lastX, dptron.lastY
	);
	break;
    case "wheel":
	dptron.wheelMessage(Docuplextron.frameOf(m.source),msg);
	break;
    case "scrollMe":
	break;
    case "changeMyWidth":
	Docuplextron.getParentItem(Docuplextron.frameOf(m.source)).changeWidth(msg["x"]);
	break;
    case "popFooter":
	// If we don't have metadata for the span we've been passed,
	// request it from the source frame.
	if(!alph.sources[msg["src"]]){
	    m.source.postMessage({"op":"sourcesRequest"},"*");
	}
	
	var sentSpan = new AlphSpan(msg["src"],msg["origin"],msg["extent"]);
	if(!dptron.currentSpan){
	    dptron.currentSpan = new AlphSpan();
	}
	
	if(dptron.currentSpan.toURL() != sentSpan.toURL()){
	    dptron.currentSpan = new AlphSpan(msg["src"],msg["origin"],msg["extent"]);
	    alph.popFooter(dptron.currentSpan);
	    if(dptron.transpointerMode == 2){
		//console.log("currentSpan changed to ",dptron.currentSpan);
		dptron.rebuildTranspointers();
	    }
	} else {
	    alph.box.show();
	}
	break;
    case "selectionChange":
	alph.selection = new AlphSelection().fromMessage(JSON.parse(msg["sel"]));
	alph.box.content.textContent = alph.selection.asString();
	break;
    case "killringPush":
	
	/* We have received a serialized document fragment to be pushed to our
	   local killring. */

	/* We parse the selection as 'application/xml' because if we use 'text/html',
	   the parser generates the whole <html><head></head><body>...</body></html>
	   structure around the fragment. We don't need that. */
	
	var parser = new DOMParser();
	var xmlDoc = parser.parseFromString(msg["sel"],"text/html");
	var nodes = Array.from(xmlDoc.body.childNodes);

	var frag = document.createDocumentFragment();
	console.log("killringPush msg rec'd. Produced this fragment: ",frag);
	for(var ix in nodes){
	    frag.appendChild(nodes[ix]);
	}
	
	dptron.killring.push(frag);
	//dptron.killring.push((new AlphSelection().fromMessage(JSON.parse(msg["sel"]))).toFragment());
	
	// If we're the top-level window, and this message came from another
	// window, broadcast it down to the iframes.
	if((parent == window) && (m.source != window)){
	    Docuplextron.broadcast(msg);
	}
	break;
    case "popout":
	dptron.popout();
	break;
    case "sourcesRequest":
	// Our parent window has asked for our media sources. Send them.
	parent.postMessage({"op":"sourcesResponse",
			    "sources":JSON.stringify(alph.sources)},"*");
	break;
    case "sourcesResponse":
	// An iframe has sent us its media sources
	var sources = JSON.parse(msg["sources"]);
	var keys = Object.keys(sources);
	// A more sophisticated merge might be better, but
	// for now just overwrite any existing sources.
	for (var ix in keys){
	    alph.sources[keys[ix]] = sources[keys[ix]];
	    //console.log("Metadata updated for: " + keys[ix]);
	}
	break;
    case "postEDL":
	dptron.postEDL();
	break;
    case "storeEDL":
	dptron.storeEDL(m.source,msg["edl"],msg["id"]);
	break;
    case "loadExternal":
	if(msg["url"].startsWith("ld+")){
	    // This needs to be done better later, but for now, if the
	    // URL is prefixed with "ld+", send an "Accept: application/ld+json"
	    // header with the request.
	    dptron.loadExternal(msg["url"].substr(3),"ld");
	} else {
	    dptron.loadExternal(msg["url"],msg["as"]);
	}
	break;
    case "toggleDOMCursor":
	dptron.DOMCursor.toggle();
	break;
    case "alph.fetchAndFill":
	alph.fetchAndFill();
	break;
    case "findAllMatchingContent":
	dptron.findAllMatchingContent();
	break;
    case "findMyMatchingContent":
	dptron.findSomeMatchingContent(Docuplextron.frameOf(m.source));
	break;
    case "linkEdit":
	linkEdit();
	break;
    case "changeTpointerMode":
	if(msg.dir == "up"){
	    dptron.transpointerMode = (dptron.transpointerMode < dptron.TRANSPOINTER_MODES) ?
		dptron.transpointerMode + 1 : 0;
	} else {
	    dptron.transpointerMode = (dptron.transpointerMode > 0 ) ?
		dptron.transpointerMode - 1 :
		dptron.TRANSPOINTER_MODES;
	}
	dptron.rebuildTranspointers();
	if(document.getElementById("dptron-linkview-button")){
	    var viewModeBtn = document.getElementById("dptron-linkview-button");
	    viewModeBtn.src = [Docuplextron.urls["linkmodeAll"],
			       Docuplextron.urls["linkmodeContext"],
			       Docuplextron.urls["linkmodeSpan"],
			       Docuplextron.urls["linkmodeNone"] ][dptron.transpointerMode];
	}
	//console.log(dptron.transpointerMode);
	break;
    case "exportHTMLToParent":
	//console.log("Export request rec'd");
	parent.postMessage({"op":"importHTMLFromContext","html":dptron.exportContextHTML(document.body)},"*");
	break;
    case "exportHTMLAndPost":
	parent.postMessage({"op":"importHTMLAndPost",
			    "method":"PUT",
			    "data":dptron.exportContextHTML(document.body),
			    "bin":true,
			    "url":false},"*");
	break;		    
    case "updateOnServer":
	//console.log("Rec'd request to update " + window.location + " on the server");
	parent.postMessage({"op":"postHtmlToUrl",
			    "html":dptron.exportContextHTML(document.body),
			    "url":window.location.toString()},"*");
	break;
    case "importHTMLFromContext":
	//console.log("Rec'd imported HTML from IFRAME");
	var f = dptron.floaterManager.create();
	f.style.width = "720px";
	var pre = document.createElement("pre");
	pre.style.whiteSpace = "pre-wrap";
	pre.textContent = msg.html;
	f.appendChild(pre);
	break;
    case "importHTMLAndPost":
	dptron.postMedia(msg.method,msg.data,msg.bin);
	break;
    case "postHtmlToUrl":
	// This does the job of exportContextToURL for context iframes. Very ugly.
	//console.log("Rec'd request to post some HTML to " + msg.url);
	var xhr = new XMLHttpRequest();
	var form = new FormData();
	form.append("media",msg.html);
	xhr.onload = function(xhr,url,context){
	    if(xhr.status == 200){
		context.id = url;
		if(window.location != url){
		    dptron.loadExternal(url);
		} else {
		}
	    } else {
		alert(xhr.responseText);
	    }
	}.bind(this,xhr,msg.url,Docuplextron.frameOf(m.source));
	xhr.open("POST",msg.url);
	xhr.send(form);
	break;
    case "cursorOnscreen":
	dptron.cursorOnscreen(msg.top,
			      msg.bottom,
			      msg.left,
			      msg.right,
			      Docuplextron.frameOf(m.source));
	break;
    }
}

/* */

Docuplextron.prototype.cursorOnscreen = function(top,bottom,left,right,frame){
    
    /* This should be in Floaters.js, shouldn't it?*/
    
    top = parseFloat(top);
    bottom = parseFloat(bottom);
    left = parseFloat(left);
    right = parseFloat(right);
    
    var deltaX = 0;
    var deltaY = 0;
    var upperBound = (window.innerHeight - this.floaterManager.topBoundary) * 0.05;
    var lowerBound = window.innerHeight * 0.95;
    var innerBound = window.innerWidth * 0.05;
    var outerBound = window.innerWidth * 0.95;
    
    if(bottom > lowerBound){
	deltaY = lowerBound - bottom;
    } else if(top < upperBound){
	deltaY = upperBound - top;
    }

    if(right > outerBound){
	deltaX = outerBound - right;
    } else if(left < innerBound){
	deltaX = innerBound - left;
    }

    if(deltaX != 0 || deltaY != 0){
	this.floaterManager.smoothPanWorkspace(deltaX*1.05,deltaY*1.05);
    }
}

/* 





   CONTROLS

 
   First up,
   KEYBOARD HANDLING




*/

function keyup(evt){

    parent.postMessage({
	"op":"keyUp","which":evt.which
    },"*");
    //dptron.keys[evt.which] = false;
    
    if(evt.which == 18) {
	evt.preventDefault();
	dptron.altKey = false;
    }
    
    if(dptron.activePalette){
	dptron.activePalette.command({"code":evt.which,
				      "alt":evt.altKey,
				      "shift":evt.shiftKey,
				      "ctrl":evt.ctrlKey});
	dptron.DOMCursor.updatePosition();
    }

    /* If the target is an editable <x-text>, update its extent. If
       it's a noodle, update the noodleStore. */
    
    if( evt.target.tagName == "X-TEXT" && evt.target.contentEditable ){
	if ( evt.target.getAttribute("src").startsWith("~") ){
	    var noodleID = evt.target.getAttribute("src").substring(1);
	    dptron.noodleManager.set( noodleID, evt.target.textContent );
	    
	    /* And then! Update the contents of any other x-texts 
	       sourcing this noodle.*/
	    
	    var otherSpans = Array.from(
		document.querySelectorAll('x-text[src="~' + noodleID + '"]'));
	    for(var ix in otherSpans){
		if(otherSpans[ix] === evt.target) continue;
		otherSpans[ix].textContent = evt.target.textContent;
		otherSpans[ix].setAttribute("origin",0);
		otherSpans[ix].setAttribute("extent",evt.target.textContent.length);
	    }
	}
	evt.target.setAttribute("extent",evt.target.textContent.length);
    }

    dptron.activeElement = dptron.DOMCursor.target || null;

    var selFocus = window.getSelection().focusNode;
    if(selFocus.nodeType == Node.TEXT_NODE &&
       evt.which != 18 &&
       evt.which != 17 &&
       evt.which != 16){
	var cRange = new Range();
	cRange.setStart(selFocus, window.getSelection().focusOffset);
	cRange.collapse(true);
	var cRect = cRange.getBoundingClientRect();
	//console.log(cRect);
	parent.postMessage({"op":"cursorOnscreen",
			    "top":cRect.top,
			    "bottom":cRect.bottom,
			    "left":cRect.left,
			    "right":cRect.right},
			   "*");
    }
}

/***/

function keydn(evt){

    /* Ignore key repeats when modifiers are held 
    if(evt.altKey || evt.ctrlKey || evt.metaKey){
	if(dptron.keys[evt.which] == true) return;
    }*/

    /* Record the key's down state */
    parent.postMessage({
	"op":"keyDn","which":evt.which
    },"*");    
    
    /* If a command palette is open, ignore keydowns. */
    if(dptron.activePalette) {
	evt.preventDefault();
	if(dptron.activePalette.palName.startsWith("Color") && !evt.repeat){
	    if(dptron.activePalette.palName == "Color1"){
		dptron.makePalette("Color2");
	    } else {
		dptron.makePalette("Color1");
	    }
	} 
	return;
    }

    /* If the event target is inside of a floater, make that floater
       our activeItem. */
    
    var parentItem = Docuplextron.getParentItem(evt.target);  
    if(parentItem){
	//dptron.activate(parentItem);
    }

    /* Okay, here's the big keymap. What a mess. */
    
    switch (evt.which){
    case 8: // [Bksp],[Mac Delete]
	
	/* Same as case 46! Remember to update both */
	
	if(evt.altKey){
	    evt.preventDefault();
	    for(var ix in dptron.activeItems){
		dptron.actionTarget = dptron.activeItems[ix];
		if(dptron.actionTarget.classList.contains("noodleBox")){
		    dptron.command("close and delete");
		} else {
		    dptron.command("close");
		}
	    }
	}
	break;
    case 13: // [Enter]
	if(evt.ctrlKey){
	    evt.preventDefault();
	    dptron.commitSpan();
	} else if(evt.altKey){

	//} else if(evt.shiftKey){

	} else {
	    Alphjs.insertNewline(evt);
	}
	break;
    case 18: // [Alt]
	dptron.altKey = true;
	break;
    case 32: // [Space]
	//evt.preventDefault();
	break;
    case 38: // [Up]
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){ 
		dptron.DOMCursor.move("shiftUp");
	    } else { 
		dptron.DOMCursor.move("up");
	    }
	} else if(evt.shiftKey){
	    alph.selectionChange();
	}
	break;
    case 40: // [Down]
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		dptron.DOMCursor.move("shiftDown");
	    } else {
		dptron.DOMCursor.move("down");
	    }
	} else if(evt.shiftKey){
	    alph.selectionChange();
	}
	break;
    case 39: // [Right]
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		dptron.DOMCursor.move("shiftRight");
	    } else {
		dptron.DOMCursor.move("right");
	    }
	} else if(evt.shiftKey){
	    alph.selectionChange();
	}
	break;
    case 37: // [Left]
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		dptron.DOMCursor.move("shiftLeft");
	    } else {
		dptron.DOMCursor.move("left");
	    }
	} else if(evt.shiftKey){
	    alph.selectionChange();
	}
	break;
    case 46: // [Del]
	/* Same as case 8! Remember to update both */
	
	if(evt.altKey){
	    evt.preventDefault();
	    for(var ix in dptron.activeItems){
		dptron.actionTarget = dptron.activeItems[ix];
		if(dptron.actionTarget.classList.contains("noodleBox")){
		    dptron.command("close and delete");
		} else {
		    dptron.command("close");
		}
	    }
	}
	break;
    case 48: // 0 (zero)
	if(parent == window){
	    if(dptron.floaterManager.inhand){
		evt.preventDefault();
		evt.stopPropagation();
		dptron.floaterManager.inhand.resetZoom();
	    }
	} else {
	    if(dptron.mouseBypass){
		evt.preventDefault();
		evt.stopPropagation();
		parent.postMessage({"op":"resetZoom"},"*");
	    }
	}
	break;
	
    case 49: // 1 - DOMpath to console.
	//console.log(getDomPath(alph.activeElement));
	break;
	
    case 173: // [-] Font size decrease
	if(evt.ctrlKey || evt.altKey){
	    // There needs to be a message send here, to increse font size in
	    // subordinate frames as well.
	    for(var ix in dptron.activeItems){
		var activeItem = dptron.activeItems[ix];
		if(evt.shiftKey){
		    var oldScale = parseFloat(activeItem.getAttribute("data-scale"));
		    var newScale = oldScale * 0.952;
		    activeItem.setAttribute("data-scale",newScale);
		    activeItem.style.transform = "scale(" + newScale + ")";
		} else {
		    var oldSize = window.getComputedStyle(activeItem).fontSize;
		    activeItem.style.fontSize = (parseInt(oldSize) - 1) + "px";
		}
	    }
	    // Note that this blocks the default enlarge/reduce operations
	    // of the browser -- that's intentional.
	    evt.preventDefault();
	}
	break;

    case 61: // [+] Font size increase
	if(evt.ctrlKey || evt.altKey){
	    // There needs to be a message send here, to increse font size in
	    // subordinate frames as well.
	    for(var ix in dptron.activeItems){
		var activeItem = dptron.activeItems[ix];
		if(evt.shiftKey){
		    var oldScale = parseFloat(activeItem.getAttribute("data-scale"));
		    var newScale = oldScale * 1.05;
		    activeItem.setAttribute("data-scale",newScale);
		    activeItem.style.transform = "scale(" + newScale + ")"; 
		} else {
		    var oldSize = window.getComputedStyle(activeItem).fontSize;
		    activeItem.style.fontSize = (parseInt(oldSize) + 1) + "px";
		}
	    }
	    evt.preventDefault();
	}
	break;

    case 65: // [A]lph palette
	if(evt.altKey){
	    evt.preventDefault();
	    dptron.makePalette("A");
	}
	break;
    case 66: // Alph [B]ar
	if(evt.altKey){
	    evt.preventDefault();
	    if(dptron.bar.style.top == "0px"){
		dptron.bar.style.top = "-50px";
		dptron.bar.querySelector("#dptronSideFlap").style.top = "0px";
		dptron.floaterManager.topBoundary = 0;
	    } else {
		dptron.bar.style.top = "0px";
		dptron.bar.querySelector("#dptronSideFlap").style.top = window.getComputedStyle(dptron.bar).height;
		dptron.floaterManager.topBoundary = parseInt(window.getComputedStyle(dptron.bar).height);
	    }    
	}	
	break;
    case 67: // [C]opy xanalogically
	/*
	  if(evt.ctrlKey){
	    evt.preventDefault();
	    if(dptron.dptronMode == "teditor" || window.getSelection().isCollapsed){
		dptron.makePalette("Color1");
	    }
	}
	*/
	if(evt.altKey){
	    evt.preventDefault();
	    
	    if(window.getSelection().isCollapsed){
		
		/* Nothing highlighted -- do we have anything selected
		   with the DOMCursor? */
		
		if(dptron.DOMCursor.target){
		    var copySource = document.createDocumentFragment();
		    copySource.appendChild(dptron.DOMCursor.target.cloneNode(true));
		    console.log("copySource from DOMCursor:",copySource);
		} else {
		    // Nothing to copy!
		    console.log("Nothing selected!")
		    return;
		}
		
	    } else {
		var copySource = alph.selection.toFragment();
		console.log("copySource from AlphSelection:",copySource);
	    }

	    /* Convert the document fragment to a string so we can pass it to
	       the killrings of all parent/child windows with postMessage(). */
	    
	    var fragmentString = "";
	    var sourceChildren = Array.from(copySource.childNodes);
	    for(var ix in sourceChildren){
		fragmentString += sourceChildren[ix].outerHTML;
	    }
	    
	    if(parent == window){
		
		/* Push the document fragment onto the kill-ring (the
		   kill-ring is just an array of document
		   fragments. )*/

		dptron.killring.push(copySource);

		/* If we're the top-level window, post that copy down to the
		   lower iframes. */
		console.log("Broadcasting killringPush message with: ",fragmentString);	
		Docuplextron.broadcast({"op":"killringPush",
					//"sel":JSON.stringify(alph.selection)});
					"sel": fragmentString});
	    } else {
		// If we're an iframe, post that copy up to the top-level window.
		console.log("Posting up killringPush message with: ",fragmentString);
		parent.postMessage({"op":"killringPush",
				    //"sel":JSON.stringify(alph.selection)},"*");
				    "sel":fragmentString},"*");
	    }
	}
	break;	
    case 68: // [D]elete, [Shift]+[D]e-parent
	if(evt.altKey){
	    if(evt.shiftKey){
		// !!! Wat. Fix this, Adam.
		Docuplextron.deParent(window.getSelection().anchorNode.parentElement.parentElement);
	    } else {
		evt.preventDefault();
		// DUPLICATE noodle (put this in a function, Adam)
		for(var ix in dptron.activeItems){
		    var floater = dptron.activeItems[ix];
		    if(floater.classList.contains("noodleBox")){
			// Only for noodleBoxen at the moment.
			var oldLimbic = dptron.noodleManager.get(floater.querySelector("x-text").getAttribute("src"));
			if (!oldLimbic) return;
			var newdle = dptron.newdleBox(null,oldLimbic.text);
			var newLimbic = dptron.noodleManager.get(newdle.querySelector("x-text").getAttribute("src"));
			newdle.tab.text.textContent = newLimbic.title = oldLimbic.title + "(copy)";
			newdle.toTop();
		    }
		}
		/* What was going on here?
		  dptron.DOMCursor.select(Alphjs.cut()[0]);
		  dptron.postEDL();
		*/
	    }
	}
	break;
    case 69: // [E]dit attributes
	if(evt.altKey && dptron.DOMCursor.enabled && dptron.DOMCursor.target){
	    evt.preventDefault();
	    attributeMunglor(dptron.DOMCursor.target);
	}
	break;
    case 70: // [F]it || [F]use
	if(evt.altKey){
	    evt.preventDefault();
	    if(parent != window){
		dptron.reportSizeToParent();
	    } else {
		dptron.merge();
	    }
	    /*
	    } else if(evt.shiftKey){
		evt.preventDefault();
		alph.fetchAndFill();
	    } else {
		evt.preventDefault();
		dptron.reportSizeToParent();
	    }*/
	} else if(evt.ctrlKey){
	    // Reserved for FRONT-TO-BACK op w/ wheel.
	    evt.preventDefault();
	    evt.stopPropagation();
	}
	break;
    case 72: // [H]TML surround
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.ctrlKey){
		var HTML = window.getSelection().toString();
		document.execCommand("insertHTML", false, HTML);
	    } else {
		// HTML element palette. This is dumb. Rewrite it.
		//dptron.stateSaves.push(dptron.mainDiv.innerHTML);
		var munglor = dptron.floaterManager.create(null,null,dptron.lastX,dptron.lastY);
		munglor.stickyToggle();
		munglor.classList.add("styleMunglor");
		munglor.style.width = "180px";
		var elements = ["b","i","u","s","em","dfn","sup","sub","ol","ul","li","blockquote","p","nav","section","article","header","footer","aside","h1","h2","h3","h4","h5","h6"];
		for(var ix in elements){
		    var button = document.createElement("input");
		    button.type = "button";
		    button.value = elements[ix];
		    button.onclick = function(m){
			Alphjs.surround(this.value);
			dptron.postEDL();
			parent.postMessage({"op":"findMyMatchingContent"},"*");
			m.remove();
		    }.bind(button,munglor);
		    munglor.appendChild(button);
		}
	    }
	}
	break;
    case 73: // [I]nsert Editable span
	if(evt.altKey){
	    evt.preventDefault();
	    //dptron.stateSaves.push(dptron.mainDiv.innerHTML);
	    if(evt.shiftKey){
		Alphjs.split(true);
		dptron.insertEditable();
		//dptron.insertEditable("sibling");
	    } else {
		dptron.insertEditable();
	    }
	}
	break;
    case 74: // [J]oin
	if(evt.altKey){
	    evt.preventDefault();
	    Alphjs.joinContiguousSpans();
	    dptron.postEDL();
	}
	break;
    case 75: // [K]ill
	if(evt.altKey){
	    evt.preventDefault();
	    //dptron.stateSaves.push(dptron.mainDiv.innerHTML);
	    dptron.DOMCursor.kill();
	    Alphjs.joinContiguousSpans();
	    dptron.postEDL();
	    parent.postMessage({"op":"findMyMatchingContent"},"*");
	}
	break;
    case 76: // [L]ink
	if(evt.altKey){
	    parent.postMessage({"op":"linkEdit"},"*");
	    //linkEdit();
	}
	break;
    case 77: // [M]ungle style
	if(evt.altKey){
	    if(dptron.DOMCursor.target){
		return newStyleMunglor(dptron.DOMCursor.target);
	    }
	};
	break;
    case 78: // [N]ew Composition
	if(evt.altKey){
	    evt.preventDefault();
	    evt.stopPropagation();
	    if(dptron.dptronMode == "teditor" || evt.shiftKey){
		dptron.newdleBox(null,null,true).toTop();
	    } else {
		dptron.newComposition().toTop();
	    }
	}
	break;
    case 79: // [O]pen for editing
	if(evt.altKey){
	    if(evt.target.tagName == "X-TEXT" && evt.target.getAttribute("src").startsWith("~")){
		if (evt.target.contentEditable){
		    evt.target.contentEditable = false;
		}else{
		    evt.target.contentEditable =  true;
		    evt.target.focus();
		}
	    } else {
		for(var ix in dptron.activeItems){
		    if(dptron.activeItems[ix].classList.contains("noodleBox")){
			var xt = dptron.activeItems[ix].querySelector("x-text");
			xt.contentEditable = true;
			xt.focus();
		    }
		}
	    }
	}
	break;
    case 80: // [P]op-out
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		dptron.domPopout();
	    } else {
		// Alph pop-out. Creates a new floater containing the current Alph selection.
		dptron.popout();
	    }
	}
	break;
    case 81: // [Q] rebuild transpointers
	if(evt.altKey){
	    evt.preventDefault();	    
	    dptron.rebuildTranspointers();
	}
	break;
    case 82: // [R]efresh
	if(evt.altKey){
	    evt.preventDefault();
	    overlayLoop();
	    dptron.updateAllRects();
	}
	break;
    case 83 : // [S]plit
	if(evt.altKey){
	    evt.preventDefault();
	    //dptron.stateSaves.push(dptron.mainDiv.innerHTML);
	    //Alphjs.split();
	    dptron.DOMCursor.select(dptron.split(evt.shiftKey));
	    dptron.postEDL();
	} else if(evt.ctrlKey){
	    // Reserved for SIZE op w/ wheel
	    evt.preventDefault();
	    evt.stopPropagation();
	}
	break;
    case 84:  // [T]ranspointer view mode
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		parent.postMessage({"op":"changeTpointerMode","dir":"down"},"*");
	    } else {
		parent.postMessage({"op":"changeTpointerMode","dir":"up"},"*");
	    }
	}
	break;	
    case 85:  // [U]ndo
	if(evt.altKey){
	    evt.preventDefault();
	    dptron.undo();
	}
	break;
    case 86: // [V] paste xanalogically
	if(evt.altKey){
	    evt.preventDefault();
	    //dptron.stateSaves.push(dptron.mainDiv.innerHTML);
	    if(dptron.killring.length > 0){
		console.log("Calling Alphjs.paste() at: ", window.getSelection().getRangeAt(0));
		var frag = Alphjs.paste(dptron.killring[dptron.killring.length -1]);
		alph.fetchAndFill(frag);
		Docuplextron.taintId(
		    Docuplextron.getParentItem(window.getSelection().anchorNode));
		Alphjs.cullEmptyNodes();
		dptron.postEDL();
		parent.postMessage({"op":"findMyMatchingContent"},"*");
	    }
	}
	break;
    case 87: // [W]ebuild EDLs
	dptron.rebuildEdlStore();
	break;
    case 88: // e[X]cise
	if(evt.altKey){
	    if(dptron.dptronMode != false){
		evt.preventDefault();
		if(dptron.dptronMode == "teditor"){
		    // HYPERAMA "excise"
		    dptron.popout(true);
		} else if(evt.shiftKey){	    
		    dptron.domPopout();
		} else {
		    dptron.popout();
		}
	    }
	}
	break;
    case 90: // [Z] Delete empty nodes
	if(evt.altKey){
	    evt.preventDefault();
	    //dptron.stateSaves.push(dptron.mainDiv.innerHTML);
	    Alphjs.cullEmptyNodes();
	}
	break;
    case 219: // [[] Grow/shrink span origin
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		alph.resizeSpan("origin",-1);
	    } else {
		alph.resizeSpan("origin",1);
	    }
	    dptron.postEDL();
	    parent.postMessage({"op":"findMyMatchingContent"},"*");
	}
	break;
    case 221: // []] Grow/shrink span extent
	if(evt.altKey){
	    evt.preventDefault();
	    if(evt.shiftKey){
		alph.resizeSpan("extent",-1);
	    } else {
		alph.resizeSpan("extent",1);
	    }
	    dptron.postEDL();
	    parent.postMessage({"op":"findMyMatchingContent"},"*");
	}
	break;
    }
	
}

/*





   And now...
   MOUSE HANDLING




*/

function mousemv(evt){

    /* If we're in a subordinate context and we're in mouse-bypass mode...
       send up floater movement messages? This looks broken. Shouldn't I 
       just be posting an updateCursorPosition message, and shouldn't that
       handler be determining what action to take? */
    
    if(parent != window){
	if (dptron.mouseBypass){
	    if(evt.ctrlKey){
		parent.postMessage({"op":"zMoveMe",
				    "deltaX":evt.movementX,
				    "deltaY":evt.movementY,
				    "clientX":evt.clientX,
				    "clientY":evt.clientY},"*");
		parent.postMessage({"op":"moveMe",
				    "x":evt.movementX,
				    "y":evt.movementY},"*");
	    } else if (evt.shiftKey && !evt.altKey){
		parent.postMessage({"op":"changeMyWidth",
				    "x":evt.movementX,
				    "y":evt.movementY},"*");
	    } else {
		parent.postMessage({"op":"moveMe",
				    "x":evt.movementX,
				    "y":evt.movementY},"*");
	    }
	    evt.preventDefault();
	    evt.stopPropagation();
	}
    }
    
    /* Same thing as that last stanza: do some floater movements if
       we've got a floater in-hand. */
    
    if(parent == window){
	if(dptron.floaterManager.inhand != null){

	/* Much as I would love to unify this all to the postMessage()
	   interface, if we're not in an iframe we have to call zMove,
	   changeWidth, or dragElement directly, because we're working
	   with a DOM subtree, not with an iframe, and we can't pass a
	   reference to the subtree's floater div through
	   postMessage(). Although... there's probably a way to unify
	   this, so that we can use the postMessage() interface to
	   these things by passing the floater's id or
	   ...something. */
	    evt.preventDefault();
	    evt.stopPropagation();
	    
	    if(dptron.floaterManager.inhand == "all"){
		dptron.floaterManager.panWorkspace(evt.movementX,evt.movementY);
	    } else if(evt.ctrlKey){
		if(dptron.activeItems.includes(dptron.floaterManager.inhand)){

		    for(var ix in dptron.activeItems){
			dptron.activeItems[ix].zMove(
			    evt.movementX,evt.movementY,dptron.lastX,dptron.lastY,true );
			dptron.activeItems[ix].move(evt.movementX,evt.movementY);
		    }
		} else {
		    dptron.floaterManager.inhand.zMove(
			evt.movementX, evt.movementY,dptron.lastX,dptron.lastY,true );
		    dptron.floaterManager.inhand.move(evt.movementX, evt.movementY);
		} 
		  
		
	    } else if(evt.shiftKey && !evt.altKey){
		dptron.floaterManager.inhand.changeWidth(evt.movementX);
	    } else {
		if(!dptron.floaterManager.inhand.classList.contains("alphActiveItem")){
		    dptron.floaterManager.inhand.move(evt.movementX,evt.movementY);
		} else {
		    for(var ix in dptron.activeItems){
			dptron.activeItems[ix].move(evt.movementX,evt.movementY);
		    }
		}
	    }
	}
    }
    if(evt.ctrlKey && evt.shiftKey){
    /*
    if(evt.altKey && evt.shiftKey){
	
	if(evt.ctrlKey){
	    if(parent != window){
		parent.postMessage({"op":"zoomWorkspace",
				    "deltaX":evt.movementX,
				    "deltaY":evt.movementY},"*");
	    } else {
		dptron.floaterManager.zoomWorkspace(
		    evt.movementX, evt.movementY, dptron.lastX, dptron.lastY
		);
	    }
	} else {
	    // Pan the workspace */
	    if(parent != window){
		parent.postMessage({"op":"panWorkspace",
				    "x":evt.movementX,
				    "y":evt.movementY},"*");
	    } else {
		dptron.floaterManager.panWorkspace(evt.movementX,evt.movementY);
	    }
	//}
    }

    /* If we're in an IFRAME, or a top-level context that has not gone
       into floater-mode, post a message to our parent that we are the
       active context. */

    if(!dptron.dptronMode){
	parent.postMessage({"op":"iAmTheContext",
			    "id":false
			   },"*");	
    } else {
	var contextFloater = Docuplextron.getParentItem(evt.target);
	if(contextFloater){
	    dptron.currentContext = Docuplextron.getParentItem(evt.target).id;
	} else if(evt.target == dptron.mainDiv){
	    dptron.currentContext = null;
	}
    }
    
    /* If there is currently a selection and we are dragging the mouse,
       update the Alph selection and keep the Alphbox visible. */
    
    if (!window.getSelection().isCollapsed){
	if (evt.buttons > 0){
	    alph.selectionChange();
	} else {
	    alph.box.show();
	}
    } else {
	/* If there is not a selection, only pop-up the Alphbox when we
	   mouse over transclusible media. */
	if (!evt.target.namespaceURI){
	    //
	} else if (evt.target.hasAttribute("src")){
	    var xsrc = evt.target.getAttribute("src");
	    if(xsrc.includes(Alphjs.TRANSCLUDE_QUERY)){
		var alphSpan = AlphSpan.fromURL(xsrc);
	    } else if(evt.target.tagName == "X-TEXT") {
		var alphSpan = AlphSpan.fromXtext(evt.target);
	    } else {
		var alphSpan = new AlphSpan(xsrc,null,null);
	    }

	    parent.postMessage({"op":"popFooter",
				"src":alphSpan.src,
				"origin":alphSpan.origin,
				"extent":alphSpan.extent},"*");
	} else if(Docuplextron.getParentItem(evt.target) ||
		  !dptron.dptronMode){
	    var srcEl = Alphjs.getAncestorByAttribute(
		evt.target, "content-source");
	    /* This gets us an <x-floater>, which is what we'll pass to 
	       getElementPointer() */
	    if(srcEl){
		var src = srcEl.getAttribute("content-source").split("#")[0] + "#" +
		    Alphjs.getElementPointer(evt.target,srcEl);
	    } else {
		var src = window.location.toString().split("#")[0] + "#" +
		    Alphjs.getElementPointer(evt.target);
	    }
	    parent.postMessage({"op":"popFooter",
				"src":src,
				"origin":null,
				"extent":null},
			       "*"
			      );
	
	}
    }

    dptron.lastX = evt.clientX;
    dptron.lastY = evt.clientY;
    
    if(parent != window){
	parent.postMessage({"op":"updateCursorPosition",
			    "x": evt.clientX,
			    "y": evt.clientY },"*");
    }
}

/***/

function mouseup(e){
    //dptron.movement = 0;

    //console.log("Points? ", Alphjs.getPoints(window.getSelection().getRangeAt(0)));
    //console.log("Element: ", Alphjs.getElementPointer(e.target));
    
    if(e.button == 1){
	e.preventDefault();
	e.stopPropagation();
    }
    
    if(parent == window){
	if(dptron.floaterManager.inhand){
	    e.preventDefault();
	    e.stopPropagation();
	}
	dptron.floaterManager.drop();
	Docuplextron.broadcast({"op":"dropAll"});
    } else {
	parent.postMessage({"op":"dropAll"},"*");
	dptron.mouseBypass = false;
    }
    
    dptron.killPalette();

    var cmdString;
    if(e.target instanceof HTMLElement &&
       e.target.classList.contains("alph-contextmenuitem")){
	cmdString = e.target.textContent;
    }
    
    if(dptron.activeMenu){
	dptron.activeMenu.remove();
	dptron.activeMenu = null;
	if (cmdString) dptron.command(cmdString);
    }

    dptron.activeElement = dptron.DOMCursor.target || null;
}

/***/

function contextmenu(e){
    if(e.ctrlKey){
	if(parent != window){
	    parent.postMessage({"op":"floaterContextMenu"},"*");
	} else {
	    if (Docuplextron.getParentItem(e.target)){
		dptron.activeMenu = dptron.contextMenuOn(
		    Docuplextron.getParentItem(e.target));
	    }
	}
	e.preventDefault();
	e.stopPropagation();
    } else if(e.target.hasAttribute("src")){
	if(parent == window){
	    e.preventDefault();
	    e.stopPropagation();
	    if(e.target.getAttribute("src").startsWith("~")){
		// Noodle ...
		var floater = Docuplextron.getParentItem(e.target);
		if(floater.classList.contains("noodleBox")){
		    // Noodle in a noodleBox...
		    dptron.activeMenu = dptron.contextMenuOn(e.target,["source info"]);
		} else {
		    // Noodle in a composition...
		    dptron.activeMenu = dptron.contextMenuOn(e.target,["source info"]);
		}
	    } else {
		dptron.activeMenu = dptron.contextMenuOn(e.target);
	    }
	} else {
	    e.preventDefault();
	    e.stopPropagation();
	    dptron.actionTarget = e.target;
	    parent.postMessage({"op":"spanContextMenu",
				"tagName":e.target.tagName || "X-TEXT",
				"src":e.target.getAttribute("src"),
				"origin":e.target.getAttribute("origin") || 0,
				"extent":e.target.getAttribute("extent") || 1
			       },"*"
			      );
	}
    } else if(e.target.tagName == "X-FLOATER"){
	e.preventDefault();
	e.stopPropagation();
	dptron.activeMenu = dptron.contextMenuOn(e.target);
    }
}

function titleTabContextMenu(floater,evt){
    if(floater.classList.contains("noodleBox")){
	dptron.activeMenu = dptron.contextMenuOn(floater,
						 [ "title",
						   "clear colours",
						   "select colour group",
						   "export as plain-text",
						   "progenitors",
						   "close and delete",
						   "close"]);				
    }else{
	dptron.activeMenu = dptron.contextMenuOn(floater);
    }
}

function titleTabMouseDown(floater,evt){
    dptron.movement = 0;
    dptron.floaterManager.pickUp(floater);
}

function titleTabMouseUp(floater,evt){
    if(dptron.movement < 15){
	dptron.activate(floater,evt.shiftKey);
    } 
}
/***/

function mousedn(e){
    dptron.movement = 0;

    // Testing...
    //var targetXPath = Alphjs.getXPath(e.target);
    
    
    var parentFloater = Docuplextron.getParentItem(e.target);
    
    if(e.button == 0){
	if(e.target == dptron.mainDiv){
	    e.preventDefault();
	    dptron.floaterManager.pickUp("all");
	} else if(parentFloater){
	    if(e.shiftKey){
		e.preventDefault();
		e.stopPropagation();
	    }
	    if(e.target.tagName == "X-FLOATER" ||
	       e.ctrlKey){
		dptron.floaterManager.pickUp(e.target);
	    }
	} else if(parent != window){
	    parent.postMessage({"op":"iAmActive"},"*");
	    if(e.altKey){
		e.preventDefault();
		e.stopPropagation();
		parent.postMessage({"op":"pickMeUp"},"*");
		dptron.mouseBypass = true;
	    }
	}
    } else if(e.button == 1){ // Middle button
	if(parent != window){
	    e.preventDefault();
	    e.stopPropagation();
	    parent.postMessage({"op":"pickMeUp"},"*");
	    dptron.mouseBypass = true;
	} else if(parentFloater){
	    e.preventDefault();
	    e.stopPropagation();
	    dptron.floaterManager.pickUp(parentFloater);
	} 
    } else if(e.button == 2){
	// Nothing here. See contextmenu()
    }
}

/***/

function click(e){

    var topLevel = (parent == window);
    var parentItem = Docuplextron.getParentItem(e.target);

    if(e.button == 0){
	if(e.target == dptron.mainDiv){
	    dptron.activate(e.target);
	} else if(dptron.movement < 15){
	    if(e.target.classList.contains("titleTab")){
		//dptron.activate(e.target.floater,e.shiftKey);
	    } else if(parentItem){
		dptron.activate(parentItem,e.shiftKey);
	    }
	}
	dptron.DOMCursor.select(e.target);
    } else if(e.button == 1){
	e.preventDefault();
	e.stopPropagation();
    }
    
    dptron.postEDL(); // This should be removed?

    /* If a link is clicked while we're in multi-document mode or 
       when we're a subordinate frame in multi-document mode, 
       open it in a floater. Ctrl+click, however, should work
       as normal.*/

    var anchor = Docuplextron.getParentItem(e.target,"A");
    
    if(anchor && ( e.button == 0 ) && !anchor.download){
	if(topLevel){
	    if(dptron.dptronMode && !e.ctrlKey &&
	       dptron.floaterManager.layer.contains(e.target)){
		e.preventDefault();
		e.stopPropagation();
		dptron.loadExternal(anchor.href);
	    }
	} else if(!e.ctrlKey){
	    e.preventDefault();
	    e.stopPropagation();
	    dptron.loadExternal(anchor.href);
	}
    }
}

/***/

function wheel(e){
    if(parent != window){
	e.preventDefault();
	e.stopPropagation();
	parent.postMessage({"op":"wheel",
			    "deltaX":e.deltaX,
			    "deltaY":e.deltaY,
			    "altKey":e.altKey,
			    "shiftKey":e.shiftKey,
			    "crtlKey":e.ctrlKey,
			    "clientX":e.clientX,
			    "clientY":e.clientY},"*");
    } else {//if(Docuplextron.getParentItem(e.target)){
	if(e.target == dptron.mainDiv ||
	   dptron.floaterDiv.contains(e.target)){
	    e.preventDefault();
	    dptron.wheelMessage(e.target,{"deltaX":e.deltaX,
					  "deltaY":e.deltaY,
					  "altKey":e.altKey,
					  "shiftKey":e.shiftKey,
					  "crtlKey":e.ctrlKey,
					  "clientX":e.clientX,
					  "clientY":e.clientY});
	}
    }
}

/***/

Docuplextron.prototype.wheelMessage = function(target,e){

    /* Handler for wheel events coming in through postMessage() */

    /* This is mishandling mouse messages coming up from IFRAMEs. The
       clientX and clientY need to be adjusted to match the coordinate
       space of the workspace. */

    var p = Docuplextron.getParentItem(target);    
    var clientX = e.clientX;
    var clientY = e.clientY;
    
    if(target.tagName == "IFRAME"){
	var clientRect = target.getBoundingClientRect();
	var scale = parseFloat(p.style.transform.match(/scale\((.+)\)/)[1]);
	clientX = (e.clientX * scale) + clientRect.left;
	clientY = (e.clientY * scale) + clientRect.top;
    }
    
    if(dptron.keys[17]){ // Ctrl
	if(dptron.keys[16]){ // Shift
	    // Ctrl+Shift+wheel anywhere 
	    dptron.floaterManager.zoomWorkspace(e.deltaX,e.deltaY,clientX, clientY);
	} else if(p){
	    if(p.classList.contains("alphActiveItem")){
		// Ctrl+wheel on activated floater
		for(var ix in dptron.activeItems){
		    p = dptron.activeItems[ix];
		    if(p) p.zMove(e.deltaX,e.deltaY,clientX,clientY,true);
		}
	    } else {
		// Ctrl+wheel over a floater
		p.zMove(e.deltaX,e.deltaY,clientX,clientY,true);
	    }
	}
    } else {
	p = Docuplextron.getParentItem(target);
	if(p){
	    if(p.classList.contains("alphActiveItem")){
		// Wheel over an activated floater
		for(var ix in dptron.activeItems){
		    p = dptron.activeItems[ix];
		    if(p) p.move(0,e.deltaY);
		}
	    } else {
		// Wheel over any floater
		p.move(0,e.deltaY);
	    }
	} else {
	    // Wheel anywhere else
	    dptron.floaterManager.panWorkspace(e.deltaX,e.deltaY);
	}
    }
	
/*    
    if(dptron.mainDiv.classList.contains("alphActiveItem")){
	// Workspace background is selected...
	if(dptron.activePalette &&
		dptron.activePalette.palName.startsWith("Color")){
	    // Color-change input mode.
	    var direction = (e.deltaY > 0) ? 1 : -1;
	    if(this.dptronMode == "teditor"){
		// No gradients in Hyperama. Simple.
		var bgColor = dptron.mainDiv.style.backgroundColor;
		var newBG = Docuplextron.colorSpin(bgColor,direction);
		dptron.mainDiv.style.backgroundImage = "none";
		dptron.mainDiv.style.backgroundColor = "#" + newBG;
	    } else {
		var bgColorDef = dptron.mainDiv.style.background;
		var bgColors = bgColorDef.match(/rgb\(.*?\)/g);
		if(dptron.activePalette.palName == "Color2"){
		    dptron.mainDiv.style.background = "linear-gradient(0deg," +
			"#" + Docuplextron.colorSpin(bgColors[0],direction) +
			", " + bgColors[1] + " )";
		} else {
		    dptron.mainDiv.style.background = "linear-gradient(0deg," +
			bgColors[0] + ", " +
			"#" + Docuplextron.colorSpin(bgColors[1],direction) + ") ";
		}
	    }
	//} else if(dptron.keys[17] && dptron.keys[83]){
	} else if(dptron.keys[17]){
	    //  Control+S --> scale workspace.
	    dptron.floaterManager.zoomWorkspace(e.deltaX, e.deltaY, e.clientX, e.clientY);
	}  else {
	    dptron.floaterManager.panWorkspace(e.deltaX,e.deltaY);
	}
	
    } else if (dptron.activeItems.length > 0){
	var col;
	for (var ix in dptron.activeItems){
	    if(p=Docuplextron.getParentItem(dptron.activeItems[ix])){
		if(dptron.activePalette && dptron.activePalette.palName.startsWith("Color")){
		    var direction = (e.deltaY > 0) ? 1 : -1;
		    if(dptron.activeItems.length == 1 &&
		       document.activeElement.tagName == "X-TEXT"){
			if(dptron.activePalette.palName == "Color2"){
			    var styleCol = p.style.color ||
				window.getComputedStyle(p).color;
			    p.style.color = "#" +
				Docuplextron.colorSpin(styleCol,direction);
			} else {
			    var styleCol = p.style.backgroundColor ||
				window.getComputedStyle(p).backgroundColor;
			    p.style.backgroundColor = "#" +
				Docuplextron.colorSpin(styleCol,direction);
			}
		    } else {
			if(!col){
			    var styleCol = p.style.borderColor ||
				window.getComputedStyle(p).borderColor;
			    col = Docuplextron.colorSpin(styleCol,direction);
			}
			p.style.borderColor = "#" + col
		    }
		} else if(dptron.keys[17]){
		    if(dptron.keys[83]) // Control+S
		       { 
			p.scale(e.deltaX,e.deltaY,e.clientX,e.clientY);
		    };
		    if (dptron.keys[70]){ // Control+F
			p.zMove(e.deltaX,e.deltaY,e.clientX,e.clientY);
		    };
		} else {
		    p.move(0,e.deltaY);
		}
	    }
	}
    }
    */
}



/*




 
   COMMAND PALETTES
   This all needs to be reworked, modularized, etc.





*/

dptron.makePalette = function(x){
    
    /* Okay, so a better way to do something like this is to create
       a div with a bunch of spans in it, where each span has an event
       listener for keydowns and keyups, listening for specific keys,
       so that they can give some visual feedback of what's been
       pressed, yadda, yadda -- but this is a quick and dirty command
       palette mechanism that works. */

    // Nuke any existing palette if there is one
    
    if(dptron.activePalette){
	dptron.activePalette.remove();
    }

    // Make the <div>, place it, put it in the DOM
    
    dptron.activePalette = document.createElement("div");
    dptron.activePalette.id = "dptron-palette";
    dptron.activePalette.pending = 1;
    dptron.activePalette.style.cssText = "font-weight: normal; padding: 0.2em;";
    dptron.activePalette.style.top = (parseInt(window.getComputedStyle(dptron.bar).top) < 0) ?
	"0px" :
	(parseInt(window.getComputedStyle(dptron.bar).top) +
	 parseInt(window.getComputedStyle(dptron.bar).height)) + "px";
    document.body.appendChild(dptron.activePalette);
    dptron.activePalette.isPending = function(){
	if(dptron.activePalette.pending > 0){
	    dptron.activePalette.pending --;
	    return true;
	} else {
	    return false;
	}
    }
    
    // Which palette?
    switch(x){
    case "Color1":
	dptron.activePalette.palName = "Color1";
	if(dptron.mainDiv.classList.contains("alphActiveItem")){
	    Docuplextron.makePaletteItems(["Use wheel to select workspace primary background color; press |C| again to select secondary background color."],dptron.activePalette);
	} else if(document.activeElement.tagName == "X-TEXT"){
	    Docuplextron.makePaletteItems(["Use wheel to select noodle background color; press |C| again to select text color."],dptron.activePalette);
	} else if(dptron.activeItems.length == 1){
	    Docuplextron.makePaletteItems(["Use wheel to select noodle border/tab color"],dptron.activePalette);
	} else if(dptron.activeItems.length > 1){
	    Docuplextron.makePaletteItems(["Use wheel to select border/tab color of selected noodles"],dptron.activePalette);
	}
	dptron.activePalette.command = function(keypress){
	    if(dptron.activePalette.isPending()){return};
	    // Do the command...
	    switch (String.fromCharCode(keypress.code)){
	    case "C": // [C]leanup simplePointers
		dptron.makePalette("Color2");
		return; // Return before destroying palette! 
		break;
	    }
	    dptron.killPalette();
	}	
	break;
    case "Color2":
	dptron.activePalette.palName = "Color2";
	if(dptron.mainDiv.classList.contains("alphActiveItem")){
	    Docuplextron.makePaletteItems(["Use wheel to select workspace secondary background color; press |C| again to select primary background color."],dptron.activePalette);
	} else if(document.activeElement.tagName == "X-TEXT"){
	    Docuplextron.makePaletteItems(["Use wheel to select noodle text color; press |C| again to select background color."],dptron.activePalette);
	} else if(dptron.activeItems.length == 1){
	    Docuplextron.makePaletteItems(["Use wheel to select noodle border/tab color"],dptron.activePalette);
	} else if(dptron.activeItems.length > 1){
	    Docuplextron.makePaletteItems(["Use wheel to select border/tab color of selected noodles"],dptron.activePalette);
	}
	dptron.activePalette.command = function(keypress){
	    if(dptron.activePalette.isPending()){return};
	    // Do the command...
	    switch (String.fromCharCode(keypress.code)){
	    case "C": // [C]leanup simplePointers
		dptron.makePalette("Color1");
		return; // Return before destroying palette! 
		break;
	    }
	    dptron.killPalette();
	}	
	break;
    case "A":
	dptron.activePalette.palName = "Alph";	
	Docuplextron.makePaletteItems(["|A|lph-mode",
				       "DOM|C|ursor",
				       "|D|ebug…",
				       "|N|ew-Item",
				       "|H|TML…",
				       "|R|earrange…",
				      "|S|ave-session"],dptron.activePalette);
	dptron.activePalette.command = function(keypress){
	    if(dptron.activePalette.isPending()){return};
	    // Do the command...
	    switch (String.fromCharCode(keypress.code)){
	    case "A": // Alph mode (takeover)
		dptron.takeover();
		break;
	    case "B":
		dl = document.createElement("a");
		dl.href = "data:text/plain;charset=utf-8," + encodeURI(JSON.stringify(localStorage));
		dl.download = "limbic.txt";
		dl.target = "new";
		dl.textContent = "";
		dl.onclick = function(){ this.remove(); }.bind(dl);
		dptron.bar.appendChild(dl);
		dl.click();
		break;
	    case "C": // DOMcursor
		// *** This needs to be fixed.
		var t = document.getElementById("alph-editoverlaytoggle");
		t.checked = (t.checked) ? false : true;
		dptron.DOMCursor.toggle();
		break;
	    case "D": // [D]EBUG
		dptron.subPalette("DEBUG")
		return; // Return before destroying palette! 
		break;
	    case "H": // [H]TML surround
		dptron.subPalette("HTML")
		return; // Return before destroying palette! 
		break;
	    case "N": // [N]ew composition
		dptron.newComposition();
		break;
	    case "P": // TESTING...
		buildLinkpointerSpans();
		break;
	    case "R": // arRange...
		dptron.subPalette("ARRANGE")
		return; // Return before destroying palette! 
		break;
	    case "S":
		dptron.sessionSave();
		break;
	    }
	    dptron.killPalette();
	}
	break;
    case "DEBUG":
	dptron.activePalette.palName = "DEBUG";
	Docuplextron.makePaletteItems(["|D|ump Tables",
				       "|L|ink Terminals",
				       "|N|oodle Dump",
				       "|O|verlayLoop",
				       "|S|ession Export"],dptron.activePalette);	
	dptron.activePalette.command = function(keypress){
	    if(dptron.activePalette.isPending()){return};
	    // Do the command...
	    switch (String.fromCharCode(keypress.code)){
	    case "A": // b[A]rf?
		dptron.command("barf");
		break;
	    case "B": // [B]uild link bridges
		buildLinkBridges();
		break;
	    case "C": // [C]leanup simplePointers
		cleanupSimplePointers();
		break;
	    case "D": // [D]ump tables to console (debuggin')
		console.log("simplePointers: ", dptron.simplePointers, "DOMCursor:",dptron.DOMCursor, "contexts:",dptron.contexts,"edlStore:",dptron.edlStore,"transpointerSpans:",dptron.transpointerSpans,dptron.linkStore);
		break;
	    case "L": // Light-up [L]ink terminals
		//buildLinkpointerSpans();
		dptron.paintLinkTerminals();
		break;
	    case "N":
		var f = dptron.floaterManager.create(null,null, dptron.lastX, dptron.lastY);
		f.setAttribute("title","Alph Text Export");
		var p = document.createElement("pre");
		p.appendChild(
		    document.createTextNode(dptron.noodleManager.textDump())
		);
		p.style.whiteSpace = "pre-wrap";
		f.insertBefore(p, f.querySelector("svg"));
		break;
	    case "O":
		dptron.overlayLoop();
		break;
	    case "S":
		dptron.exportSession();
		break;
	    case "Z":
		dptron.floaterManager.compressZSpace();
		break;
	    }
	    dptron.killPalette();
	}
	break;
    case "ARRANGE":
	dptron.activePalette.palName = "ARRANGE";
	dptron.activePalette.textContent = '[L]ineUp';
	dptron.activePalette.command = function(keypress){
	    if(dptron.activePalette.isPending()){return};
	    // Do the command...
	    switch (String.fromCharCode(keypress.code)){
	    case "L": // Line-up
		dptron.floaterManager.lineUp();
		break;
	    }
	    dptron.killPalette();
	}
	break;
    case "HTML":
	dptron.activePalette.palName = "HTML";
	dptron.activePalette.textContent = 'HTML: [H]yperlink [B] [C]ode [I] [K]bd [U] [E]m [D]fn bi[G] s[M]all s[T]rong -- H[1-6] -- [P] [S]ection [A]rticle block[Q]uote [ ]pre -- [O]l [N](ul) [L]i ';
	dptron.activePalette.keysToElements = {
	    "1":"h1",
	    "2":"h2",
	    "3":"h3",
	    "4":"h4",
	    "5":"h5",
	    "6":"h6",
	    " ":"pre",
	    "A":"article",
	    "B":"b",
	    "C":"code",
	    "D":"dfn",
	    "E":"em",
	    "G":"big",
	    "H":"hyperlink",
	    "I":"i",
	    "K":"kbd",
	    "L":"li",
	    "M":"small",
	    "N":"ul",
	    "O":"ol",
	    "P":"p",
	    "Q":"blockquote",
	    "S":"section",
	    "T":"strong",
	    "U":"u"
	    }
	dptron.activePalette.command = function(keypress){
	    var key = String.fromCharCode(keypress.code);
	    if(dptron.activePalette.isPending()){return};
	    var tag = dptron.activePalette.keysToElements[key]; 
	    if(tag){
		if(tag == "hyperlink"){
		    oldLinkEdit();
		} else {
		    dptron.DOMCursor.select(
			Alphjs.surround(
			    tag,dptron.DOMCursor.target)
		    );
		}
	    }
	    dptron.killPalette();
	}
	break;

    }
    return dptron.activePalette;
}

/***/

dptron.subPalette = function(palette){
    var p = dptron.makePalette(palette);
    p.pending--;
}

/***/

dptron.killPalette = function(){
    if(dptron.activePalette){
	dptron.activePalette.remove();
	dptron.activePalette = null;
    }
}

/*





  INIT





 */

function commonInit(){

    /* Init routine common to top-level contexts and iframes */

    window.addEventListener('mousemove',mousemv);
    window.addEventListener('keydown',keydn);
    window.addEventListener('keyup',keyup);
    window.addEventListener('mousedown',mousedn);
    window.addEventListener('mouseup',mouseup);
    window.addEventListener('wheel',wheel);
    window.addEventListener('click',click);
    window.addEventListener('contextmenu',contextmenu);
    
    alph.getXSources();
    dptron.postEDL();
    dptron.pushAllLinks();

    overlayLoop();
}

/***/

function iframeInit(){

    /* Init stuff for IFRAME contexts */

    dptron.initMain();
    dptron.initBar("iframe");

    dptron.noodleManager = new Lamian();
    alph.lamian = dptron.noodleManager;
    dptron.DOMCursor.toggle();
    
    commonInit();
}

/***/

function init(){
    var metaTags = Array.from(document.getElementsByTagName("meta"));
    var autoplex = false;
    var expiry;
    for(var ix in metaTags){
	if(metaTags[ix].hasAttribute("docuplextron")){
	    autoplex = metaTags[ix].getAttribute("docuplextron");
	} else if(metaTags[ix].hasAttribute("dptronexpiry")){
	    expiry = parseInt(metaTags[ix].getAttribute("dptronexpiry"));
	}
    }

    if(parent == window &&
       typeof(chrome) != "undefined" &&
       typeof(chrome.extension) != "undefined" &&
       document.contentType == "text/html" &&
       Array.from(document.getElementsByTagName("X-TEXT")).length == 0){

	/* If we are running as a WebExtension, if this is the parent
	   context (not an IFRAME), and the document is an HTML
	   document, and it has no <x-text> elements, don't do
	   anything. */

	return;
	
    } else if(document.contentType == "text/plain"){

	/* If we're loading a plain text document, wrap the text in an <x-text>, 
	   and let the Alph tools load -- even if it's not an Alphic source, 
	   we can treat it as a transclusible source. */

	var plaintext = document.body.querySelector("pre").childNodes[0];

	var xtext = document.createElement("x-text");

	/* This *could* be a transclusion that we're looking at, so
	   dereference the URL into an AlphSpan if we need to. */
	
	var srcUrl = window.location.toString().split("?")[0].split("#")[0];

	if(window.location.toString().includes("fragment=")){
	    var aSpan = AlphSpan.fromURL(window.location.toString());
	} else {
	    alph.getDescription(srcUrl,true);
	    var aSpan = new AlphSpan(srcUrl,0,alph.sources[srcUrl]["alph:textLength"]);
	}
	
	xtext.setAttribute("src", aSpan.src);
	xtext.setAttribute("origin",aSpan.origin);
	xtext.setAttribute("extent",aSpan.extent);
	xtext.appendChild(plaintext);
	document.body.querySelector("pre").appendChild(xtext);
    }

    window.addEventListener('message',handleMessage);

    if(parent != window){

	if(typeof(chrome) == "undefined" ||
	   typeof(chrome.extension) == "undefined"){

	    /* If we are running as a page script in an iframe/embed, do nothing. */
	    
	    return;
	} else {
	    /* We are in an <embed> or <iframe>. Ask our parent to trigger an init. */
	
	    dptron.reportSizeToParent();
	    parent.postMessage({"op":"isAlphMyParent"},"*");
	}
    } else {
	
	dptron.initMain(autoplex);
	dptron.initFloaterDiv();
	dptron.floaterManager = new FloaterManager(dptron.floaterDiv);

	dptron.noodleManager = new Lamian();
	alph.lamian = dptron.noodleManager;
	
	dptron.initOverlay();
	dptron.initBar(autoplex);

	dptron.initButtons();
	dptron.initScratchpad();
	dptron.initBuffer();

	// TESTING AREA ====

	// =================
	
	commonInit();

	if (autoplex != false){
	    dptron.takeover(autoplex,expiry);
	}
    }
}

function overlayLoop(){
    
    // Don't do any overlay stuff if we're a subordinate window. 
    if(parent != window) return;

    dptron.movement++;
    
    requestAnimationFrame(overlayLoop);

    dptron.loopCounter++;
    if(dptron.loopCounter % 600 == 500){
	var asToggle = document.getElementById("dptronSessionAutosaveToggle");
	if(dptron.dptronMode != false &&
	   dptron.autoSaveSafe &&
	   asToggle &&
	   asToggle.checked){
	    dptron.sessionSave();
	}
	
	// Clean-up simplepointers -- REMOVE THIS?
	//dptron.simplePointers = dptron.simplePointers.filter(function(p){
	//    return (p.dead == false);
	//});
    }

    // We don't need 60 FPS.
    dptron.frameskip++;
    dptron.frameskip = dptron.frameskip % 2;
    if(dptron.frameskip != 1){
	return;
    }    
    
    dptron.redraw();
    if(dptron.currentContext){
	if(dptron.currentContext.tagName == "IFRAME"){
	    dptron.floaterManager.updateTabs([Docuplextron.getParentItem(dptron.currentContext)]);
	}else{
	    dptron.floaterManager.updateTabs([dptron.currentContext]);
	}
    } else {
	dptron.floaterManager.updateTabs([dptron.currentContext]);
    }
}

init();
